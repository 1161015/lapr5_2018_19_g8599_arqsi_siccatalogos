﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using SiC.Models;
using SiC.Utils;
using Xunit;
using Xunit.Abstractions;
using SiC.ViewModels;

namespace SiC.Tests
{
    public class ProdutoTest
    {
        private readonly ITestOutputHelper output;
        public ProdutoTest(ITestOutputHelper output)
        {
            this.output = output;
        }


        [Fact]
        public void TestarAdicionarProdutos()
        {
            DimensaoContinua altura1 = new DimensaoContinua();
            altura1.dimensaoMin = 30;
            altura1.dimensaoMax = 60;
            DimensaoDiscreta largura1 = new DimensaoDiscreta();
            largura1.dimensao = 50;
            DimensaoDiscreta largura2 = new DimensaoDiscreta();
            largura2.dimensao = 70;
            DimensaoContinua profundidade1 = new DimensaoContinua();
            profundidade1.dimensaoMin = 40;
            profundidade1.dimensaoMax = 60;

            DimensaoContinua altura2 = new DimensaoContinua();
            altura2.dimensaoMin = 20;
            altura2.dimensaoMax = 50;
            DimensaoDiscreta largura3 = new DimensaoDiscreta();
            largura3.dimensao = 40;
            DimensaoDiscreta largura4 = new DimensaoDiscreta();
            largura4.dimensao = 60;
            DimensaoContinua profundidade2 = new DimensaoContinua();
            profundidade2.dimensaoMin = 30;
            profundidade2.dimensaoMax = 50;

            ICollection<Dimensao> alturas = new List<Dimensao>();
            ICollection<Dimensao> larguras = new List<Dimensao>();
            ICollection<Dimensao> profundidades = new List<Dimensao>();

            alturas.Add(altura1);
            alturas.Add(altura2);
            larguras.Add(largura1);
            larguras.Add(largura2);
            larguras.Add(largura3);
            larguras.Add(largura4);
            profundidades.Add(profundidade1);
            profundidades.Add(profundidade2);

            Dimensoes dimensoes = new Dimensoes();
            dimensoes.Altura = alturas;
            dimensoes.Largura = larguras;
            dimensoes.Profundidade = profundidades;

            Produto pai = new Produto(null, null, null, null, dimensoes, null, null);

            

            altura1 = new DimensaoContinua();
            altura1.dimensaoMin = 350;
            altura1.dimensaoMax = 600;
            largura1 = new DimensaoDiscreta();
            largura1.dimensao = 50;
            largura2 = new DimensaoDiscreta();
            largura2.dimensao = 70;
            profundidade1 = new DimensaoContinua();
            profundidade1.dimensaoMin = 400;
            profundidade1.dimensaoMax = 600;

            altura2 = new DimensaoContinua();
            altura2.dimensaoMin = 400;
            altura2.dimensaoMax = 500;
            largura3 = new DimensaoDiscreta();
            largura3.dimensao = 20;
            largura4 = new DimensaoDiscreta();
            largura4.dimensao = 60;
            profundidade2 = new DimensaoContinua();
            profundidade2.dimensaoMin = 300;
            profundidade2.dimensaoMax = 500;

            alturas = new List<Dimensao>();
            larguras = new List<Dimensao>();
            profundidades = new List<Dimensao>();

            alturas.Add(altura1);
            alturas.Add(altura2);
            larguras.Add(largura1);
            larguras.Add(largura2);
            larguras.Add(largura3);
            larguras.Add(largura4);
            profundidades.Add(profundidade1);
            profundidades.Add(profundidade2);

            dimensoes = new Dimensoes();
            dimensoes.Altura = alturas;
            dimensoes.Largura = larguras;
            dimensoes.Profundidade = profundidades;

            Produto filho = new Produto(null, null, null, null, dimensoes, null, null);

            RestricaoTamanho rt = new RestricaoTamanho();
            rt.percentagemAlturaMaxima = 90;
            rt.percentagemAlturaMinima = 60;
            rt.percentagemLarguraMaxima = 90;
            rt.percentagemLarguraMinima = 60;
            rt.percentagemProfundidadeMaxima = 90;
            rt.percentagemProfundidadeMinima = 60;

            output.WriteLine(""+pai.ConverteTamanhoMinimo(filho, 2, rt));
            output.WriteLine("" + pai.BuscarLimiteDimensao(pai.Dimensoes, 2, 1));

            List<Tuplo<Produto, RestricaoTamanho>> tuplos = new List<Tuplo<Produto, RestricaoTamanho>>();

            Tuplo<Produto, RestricaoTamanho> tuplo = new Tuplo<Produto, RestricaoTamanho>();
            tuplo.Chave = filho;
            tuplo.Valor = rt;

            tuplos.Add(tuplo);

            bool expected = false;
            bool result = pai.AdicionarProdutos(tuplos);
            Assert.True(result == expected);
        }

        [Fact]
        public void TestarAdicionarProdutosPaiSemFilhos()
        {
            DimensaoContinua altura1 = new DimensaoContinua();
            altura1.dimensaoMin = 30;
            altura1.dimensaoMax = 60;
            DimensaoDiscreta largura1 = new DimensaoDiscreta();
            largura1.dimensao = 50;
            DimensaoDiscreta largura2 = new DimensaoDiscreta();
            largura2.dimensao = 70;
            DimensaoContinua profundidade1 = new DimensaoContinua();
            profundidade1.dimensaoMin = 40;
            profundidade1.dimensaoMax = 60;

            DimensaoContinua altura2 = new DimensaoContinua();
            altura2.dimensaoMin = 20;
            altura2.dimensaoMax = 50;
            DimensaoDiscreta largura3 = new DimensaoDiscreta();
            largura3.dimensao = 40;
            DimensaoDiscreta largura4 = new DimensaoDiscreta();
            largura4.dimensao = 60;
            DimensaoContinua profundidade2 = new DimensaoContinua();
            profundidade2.dimensaoMin = 30;
            profundidade2.dimensaoMax = 50;

            ICollection<Dimensao> alturas = new List<Dimensao>();
            ICollection<Dimensao> larguras = new List<Dimensao>();
            ICollection<Dimensao> profundidades = new List<Dimensao>();

            alturas.Add(altura1);
            alturas.Add(altura2);
            larguras.Add(largura1);
            larguras.Add(largura2);
            larguras.Add(largura3);
            larguras.Add(largura4);
            profundidades.Add(profundidade1);
            profundidades.Add(profundidade2);

            Dimensoes dimensoes = new Dimensoes();
            dimensoes.Altura = alturas;
            dimensoes.Largura = larguras;
            dimensoes.Profundidade = profundidades;

            Produto pai = new Produto(null, null, null, null, dimensoes, null, null);

            List<Tuplo<Produto, RestricaoTamanho>> tuplos = new List<Tuplo<Produto, RestricaoTamanho>>();


            bool expected = true;
            bool result = pai.AdicionarProdutos(tuplos);
            Assert.True(result == expected);
        }

        [Fact]
        public void TestarCumpreTaxaOcupacaoSucesso()
        {
            DimensaoContinua altura1 = new DimensaoContinua();
            altura1.dimensaoMin = 30;
            altura1.dimensaoMax = 60;
            DimensaoDiscreta largura1 = new DimensaoDiscreta();
            largura1.dimensao = 50;
            DimensaoDiscreta largura2 = new DimensaoDiscreta();
            largura2.dimensao = 70;
            DimensaoContinua profundidade1 = new DimensaoContinua();
            profundidade1.dimensaoMin = 40;
            profundidade1.dimensaoMax = 60;

            DimensaoContinua altura2 = new DimensaoContinua();
            altura2.dimensaoMin = 20;
            altura2.dimensaoMax = 50;
            DimensaoDiscreta largura3 = new DimensaoDiscreta();
            largura3.dimensao = 40;
            DimensaoDiscreta largura4 = new DimensaoDiscreta();
            largura4.dimensao = 60;
            DimensaoContinua profundidade2 = new DimensaoContinua();
            profundidade2.dimensaoMin = 30;
            profundidade2.dimensaoMax = 50;

            ICollection<Dimensao> alturas = new List<Dimensao>();
            ICollection<Dimensao> larguras = new List<Dimensao>();
            ICollection<Dimensao> profundidades = new List<Dimensao>();

            alturas.Add(altura1);
            alturas.Add(altura2);
            larguras.Add(largura1);
            larguras.Add(largura2);
            larguras.Add(largura3);
            larguras.Add(largura4);
            profundidades.Add(profundidade1);
            profundidades.Add(profundidade2);

            Dimensoes dimensoes = new Dimensoes();
            dimensoes.Altura = alturas;
            dimensoes.Largura = larguras;
            dimensoes.Profundidade = profundidades;

            Produto pai = new Produto(null, null, null, null, dimensoes, null, null);

            altura1 = new DimensaoContinua();
            altura1.dimensaoMin = 30;
            altura1.dimensaoMax = 60;
            largura1 = new DimensaoDiscreta();
            largura1.dimensao = 50;
            largura2 = new DimensaoDiscreta();
            largura2.dimensao = 70;
            profundidade1 = new DimensaoContinua();
            profundidade1.dimensaoMin = 40;
            profundidade1.dimensaoMax = 60;

            altura2 = new DimensaoContinua();
            altura2.dimensaoMin = 20;
            altura2.dimensaoMax = 50;
            largura3 = new DimensaoDiscreta();
            largura3.dimensao = 20;
            largura4 = new DimensaoDiscreta();
            largura4.dimensao = 60;
            profundidade2 = new DimensaoContinua();
            profundidade2.dimensaoMin = 30;
            profundidade2.dimensaoMax = 50;

            alturas = new List<Dimensao>();
            larguras = new List<Dimensao>();
            profundidades = new List<Dimensao>();

            alturas.Add(altura1);
            alturas.Add(altura2);
            larguras.Add(largura1);
            larguras.Add(largura2);
            larguras.Add(largura3);
            larguras.Add(largura4);
            profundidades.Add(profundidade1);
            profundidades.Add(profundidade2);

            dimensoes = new Dimensoes();
            dimensoes.Altura = alturas;
            dimensoes.Largura = larguras;
            dimensoes.Profundidade = profundidades;

            Produto filho = new Produto(null, null, null, null, dimensoes, null, null);

            RestricaoTamanho rt = new RestricaoTamanho();
            rt.percentagemAlturaMaxima = 50;
            rt.percentagemAlturaMinima = 15;
            rt.percentagemLarguraMaxima = 50;
            rt.percentagemLarguraMinima = 15;
            rt.percentagemProfundidadeMaxima = 50;
            rt.percentagemProfundidadeMinima = 15;

            bool expected = true;
            bool result = pai.CumpreTaxaOcupacao(rt, filho);
            Assert.True(result == expected);
        }


        [Fact]
        public void TestarCumpreTaxaOcupacaoInsucesso()
        {
            DimensaoContinua altura1 = new DimensaoContinua();
            altura1.dimensaoMin = 30;
            altura1.dimensaoMax = 60;
            DimensaoDiscreta largura1 = new DimensaoDiscreta();
            largura1.dimensao = 50;
            DimensaoDiscreta largura2 = new DimensaoDiscreta();
            largura2.dimensao = 70;
            DimensaoContinua profundidade1 = new DimensaoContinua();
            profundidade1.dimensaoMin = 40;
            profundidade1.dimensaoMax = 60;

            DimensaoContinua altura2 = new DimensaoContinua();
            altura2.dimensaoMin = 20;
            altura2.dimensaoMax = 50;
            DimensaoDiscreta largura3 = new DimensaoDiscreta();
            largura3.dimensao = 40;
            DimensaoDiscreta largura4 = new DimensaoDiscreta();
            largura4.dimensao = 60;
            DimensaoContinua profundidade2 = new DimensaoContinua();
            profundidade2.dimensaoMin = 30;
            profundidade2.dimensaoMax = 50;

            ICollection<Dimensao> alturas = new List<Dimensao>();
            ICollection<Dimensao> larguras = new List<Dimensao>();
            ICollection<Dimensao> profundidades = new List<Dimensao>();

            alturas.Add(altura1);
            alturas.Add(altura2);
            larguras.Add(largura1);
            larguras.Add(largura2);
            larguras.Add(largura3);
            larguras.Add(largura4);
            profundidades.Add(profundidade1);
            profundidades.Add(profundidade2);

            Dimensoes dimensoes = new Dimensoes();
            dimensoes.Altura = alturas;
            dimensoes.Largura = larguras;
            dimensoes.Profundidade = profundidades;

            Produto pai = new Produto(null, null, null, null, dimensoes, null, null);

            altura1 = new DimensaoContinua();
            altura1.dimensaoMin = 30;
            altura1.dimensaoMax = 60;
            largura1 = new DimensaoDiscreta();
            largura1.dimensao = 50;
            largura2 = new DimensaoDiscreta();
            largura2.dimensao = 70;
            profundidade1 = new DimensaoContinua();
            profundidade1.dimensaoMin = 40;
            profundidade1.dimensaoMax = 60;

            altura2 = new DimensaoContinua();
            altura2.dimensaoMin = 20;
            altura2.dimensaoMax = 50;
            largura3 = new DimensaoDiscreta();
            largura3.dimensao = 20;
            largura4 = new DimensaoDiscreta();
            largura4.dimensao = 60;
            profundidade2 = new DimensaoContinua();
            profundidade2.dimensaoMin = 30;
            profundidade2.dimensaoMax = 50;

            alturas = new List<Dimensao>();
            larguras = new List<Dimensao>();
            profundidades = new List<Dimensao>();

            alturas.Add(altura1);
            alturas.Add(altura2);
            larguras.Add(largura1);
            larguras.Add(largura2);
            profundidades.Add(profundidade1);
            profundidades.Add(profundidade2);

            dimensoes = new Dimensoes();
            dimensoes.Altura = alturas;
            dimensoes.Largura = larguras;
            dimensoes.Profundidade = profundidades;

            Produto filho = new Produto(null, null, null, null, dimensoes, null, null);

            RestricaoTamanho rt = new RestricaoTamanho();
            rt.percentagemAlturaMaxima = 50;
            rt.percentagemAlturaMinima = 15;
            rt.percentagemLarguraMaxima = 50;
            rt.percentagemLarguraMinima = 15;
            rt.percentagemProfundidadeMaxima = 50;
            rt.percentagemProfundidadeMinima = 15;

            bool expected = false;
            bool result = pai.CumpreTaxaOcupacao(rt, filho);
            Assert.True(result == expected);
        }
        [Fact]
        public void TestarPodeTerFilhos()
        {
            Categoria categoriaPai = new Categoria()
            {
                Nome = new Nome()
                {
                    Conteudo = "armário"
                }
            };
            Categoria categoriaFilho1 = new Categoria()
            {
                Nome = new Nome()
                {
                    Conteudo = "Melhor armário de sempre"
                }
            };
            Categoria categoriaFilho2 = new Categoria()
            {
                Nome = new Nome()
                {
                    Conteudo = "armrio"
                }
            };
            Categoria categoriaFilho3 = new Categoria()
            {
                Nome = new Nome()
                {
                    Conteudo = "qualquer coisa"
                }
            };
            categoriaFilho3.CategoriaPai = categoriaFilho2;
            categoriaFilho2.CategoriaPai = categoriaFilho1;
            categoriaFilho1.CategoriaPai = categoriaPai;

            Produto p1 = new Produto() { Categoria = categoriaFilho3 };
            bool resultado = p1.PodeTerFilhos();
            bool resultadoEsperado = true;
            Assert.True(resultado == resultadoEsperado);
        }

        [Fact]
        public void TestFromDto()
        {

            
            Nome nome=new Nome("Produto");
            Nome nomeCategoria=  new Nome("Categoria");
            Categoria categoria= new Categoria(nomeCategoria,null);
            List<ProdutoMaterial> materiais= new List<ProdutoMaterial>();
            List<ProdutoParte> produtosAgregados=new List<ProdutoParte>();
            Produto produto=new Produto(nome,materiais,categoria,produtosAgregados);
            ICollection<Dimensao> altura= new List<Dimensao>();
            ICollection<Dimensao> largura=new List<Dimensao>();
            ICollection<Dimensao> profundidade= new List<Dimensao>();
            produto.Dimensoes = new Dimensoes();
            produto.Dimensoes.Altura=altura;
            produto.Dimensoes.Largura=largura;
            produto.Dimensoes.Profundidade=profundidade;
            produto.ProdutoId = 1;
            

            
            Nome nomeDto=new Nome("Produto");
            Nome nomeCategoriaDto= new Nome("Categoria");
            Categoria categoriaDto= new Categoria(nomeCategoriaDto,null);
            List<ProdutoMaterial> materiaisDto= new List<ProdutoMaterial>();
            List<ProdutoParte> produtosAgregadosDto=new List<ProdutoParte>();
            ProdutoDTO produtoDto= new ProdutoDTO(nomeDto,materiaisDto,categoriaDto,produtosAgregadosDto);
            ICollection<DimensaoDTO> alturaDto= new List<DimensaoDTO>();
            ICollection<DimensaoDTO> larguraDto=new List<DimensaoDTO>();
            ICollection<DimensaoDTO> profundidadeDto= new List<DimensaoDTO>();
            produtoDto.Dimensoes = new DimensoesDTO();
            produtoDto.Dimensoes.Altura=alturaDto;
            produtoDto.Dimensoes.Largura=larguraDto;
            produtoDto.Dimensoes.Profundidade=profundidadeDto;
            produtoDto.Id = 1;
            Produto produtoResultado=Produto.FromDTO(produtoDto);
       
            Assert.Equal(produto.Nome,produtoResultado.Nome);
            Assert.Equal(produto.Materiais, produtoResultado.Materiais);
            Assert.Equal(produto.ProdutosAgregados, produtoResultado.ProdutosAgregados);
            Assert.Equal(produto.Categoria, produtoResultado.Categoria);
        }

    }
}
