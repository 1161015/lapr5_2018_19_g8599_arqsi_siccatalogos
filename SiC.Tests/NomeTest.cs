﻿using System;
using System.Collections.Generic;
using System.Text;
using SiC.Models;
using Xunit;
using SiC.ViewModels;

namespace SiC.Tests
{
    public class NomeTest
    {
        private readonly Nome nome;

        public NomeTest()
        {
            nome = new Nome("NomeTeste");
        }

        [Fact]
        public void TestFromDTOConversion()
        {
            Nome expected = new Nome("teste");

            NomeDTO nomeDTO = new NomeDTO("teste");

            Nome result = Nome.fromDTO(nomeDTO);

            Assert.Equal(expected, result);
        }

        [Fact]
        public void TestToDTOConversion()
        {
            NomeDTO expected = new NomeDTO("teste");

            Nome nome = new Nome("teste");

            NomeDTO result = nome.toDTO();

            Assert.Equal(expected, result);
        }
    }
}
