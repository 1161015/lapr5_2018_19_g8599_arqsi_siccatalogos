﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using SiC.Models;
using SiC.ViewModels;

namespace SiC.Tests
{
    public class CategoriaTest
    {

        private readonly Categoria categoria;

        public CategoriaTest()
        {
            Nome nome = new Nome("Armario");
            categoria = new Categoria(nome, null);
        }

        [Fact]
        public void TestFromDTOConversion()
        {
            Nome nome = new Nome("teste");
            Categoria categoriaPai = null;
            Categoria expected = new Categoria(nome, categoriaPai);
            expected.Id=0;
            NomeDTO nomeDTO = new NomeDTO("teste");
            CategoriaDTO categoriaPaiDTO = null;
            CategoriaDTO categoriaDTO = new CategoriaDTO(nomeDTO, categoriaPaiDTO);

            Categoria result = Categoria.fromDTO(categoriaDTO);

            Assert.Equal(expected, result);
        }

        [Fact]
        public void TestToDTOConversion()
        {
            Nome nome = new Nome("teste");
            Categoria categoria = new Categoria(nome, null);

            NomeDTO nomeDTO = new NomeDTO("teste");
            CategoriaDTO expected = new CategoriaDTO(nomeDTO, null);

            CategoriaDTO result = categoria.toDTO();

            Assert.Equal(expected, result);
        }
    }
}
