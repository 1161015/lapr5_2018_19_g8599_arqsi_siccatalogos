using System;
using Xunit;
using SiC.Models;
using SiC.ViewModels;
using System.Collections.Generic;
using System.Diagnostics;

namespace SiC.Tests
{
    public class AcabamentoTest
    {

        
        public AcabamentoTest()
        {

        }

        [Fact]
        public void TestToDTOConversion()
        {
            NomeDTO nomeDTO = new NomeDTO("teste");
            AcabamentoDTO expected = new AcabamentoDTO(nomeDTO);

            Nome nome = new Nome("teste");
            Acabamento acabamento = new Acabamento(nome);

            AcabamentoDTO result = acabamento.toDTO();

            Assert.Equal(expected, result);
        }

        [Fact]
        public void TestFromDTOConversion()
        {
            
            Nome nome = new Nome("teste");
            Acabamento expected = new Acabamento(nome);
            Material m1 = new Material(new Nome("nome"), new List<MaterialAcabamento>());
            MaterialAcabamento ma1 = new MaterialAcabamento(m1, expected);
            expected.Materiais = new List<MaterialAcabamento>();
            expected.Materiais.Add(ma1);
            
            NomeDTO nomeDTO = new NomeDTO("teste");
            AcabamentoDTO acabamentoDTO = new AcabamentoDTO(nomeDTO);
            MaterialDTO md1 = new MaterialDTO(new NomeDTO("nome"), new List<MaterialAcabamentoDTO>());
            MaterialAcabamentoDTO mad1 = new MaterialAcabamentoDTO(md1, acabamentoDTO);
            acabamentoDTO.Materiais = new List<MaterialAcabamentoDTO>();
            acabamentoDTO.Materiais.Add(mad1);

            Acabamento result = Acabamento.fromDTO(acabamentoDTO);
            
            Assert.Equal(expected, result);
        }
    }
}
