﻿using System;
using System.Collections.Generic;
using System.Text;
using SiC.Models;
using Xunit;
using SiC.ViewModels;

namespace SiC.Tests
{
    public class DimensaoTest
    {

        public DimensaoTest()
        {         
        }

        [Fact]
        public void TestDimensaoContinuaNaoValida()
        {
            double dimMin = 100;
            double dimMax = 150;

            DimensaoContinua dim = new DimensaoContinua
            {
                dimensaoMin = (Double)dimMin,
                dimensaoMax = (Double)dimMax
            
            }; 

            bool resultado = dim.ValorValido(8);

            Assert.False(resultado);
        }

        [Fact]
        public void TestDimensaoContinuaValida()
        {
            double dimMin = 100;
            double dimMax = 150;

            DimensaoContinua dim = new DimensaoContinua
            {
                dimensaoMin = (Double)dimMin,
                dimensaoMax = (Double)dimMax

            };

            bool resultado = dim.ValorValido(110);

            Assert.True(resultado);
        }

        [Fact]
        public void TestDimensaoDiscretaNaoValida()
        {
            double dimensao = 100;
            DimensaoDiscreta dim = new DimensaoDiscreta
            {
                dimensao = (Double)dimensao
            };

            bool resultado = dim.ValorValido(20);

            Assert.False(resultado);
        }

        [Fact]
        public void TestDimensaoDiscretaValida()
        {
            double dimensao = 100;
            DimensaoDiscreta dim = new DimensaoDiscreta
            {
                dimensao = (Double)dimensao
            };

            bool resultado = dim.ValorValido(100);

            Assert.True(resultado);
        }

        [Fact]
        public void TestAlturaMaiorQueMaximo()
        {
            double dimensao = 230;

            bool resultado = Dimensao.ValidarAltura(dimensao, false);

            Assert.False(resultado);
        }

        [Fact]
        public void TestAlturaArmarioMenorQueMinimo()
        {
            double dimensao = 50;

            bool resultado = Dimensao.ValidarAltura(dimensao, true);

            Assert.False(resultado);
        }

        [Fact]
        public void TestAlturaMenorQueMinimo()
        {
            double dimensao = 38;

            bool resultado = Dimensao.ValidarAltura(dimensao, false);

            Assert.False(resultado);
        }

        [Fact]
        public void TestAlturaArmarioValida()
        {
            double dimensao = 100;

            bool resultado = Dimensao.ValidarAltura(dimensao, true);

            Assert.True(resultado);
        }

        [Fact]
        public void TestAlturaConstituinteValida()
        {
            double dimensao = 100;

            bool resultado = Dimensao.ValidarAltura(dimensao, true);

            Assert.True(resultado);
        }

        [Fact]
        public void TestLarguraMaiorQueMaximo()
        {
            double dimensao = 301;

            bool resultado = Dimensao.ValidarLargura(dimensao, false);

            Assert.False(resultado);
        }

        [Fact]
        public void TestLarguraArmarioMenorQueMinimo()
        {
            double dimensao = 50;

            bool resultado = Dimensao.ValidarLargura(dimensao, true);

            Assert.False(resultado);
        }

        [Fact]
        public void TestLarguraMenorQueMinimo()
        {
            double dimensao = 38;

            bool resultado = Dimensao.ValidarLargura(dimensao, false);

            Assert.False(resultado);
        }

        [Fact]
        public void TestLarguraArmarioValida()
        {
            double dimensao = 100;

            bool resultado = Dimensao.ValidarLargura(dimensao, true);

            Assert.True(resultado);
        }

        [Fact]
        public void TestLarguraConstituinteValida()
        {
            double dimensao = 100;

            bool resultado = Dimensao.ValidarLargura(dimensao, true);

            Assert.True(resultado);
        }

        [Fact]
        public void TestProfundidadeMaiorQueMaximo()
        {
            double dimensao = 201;

            bool resultado = Dimensao.ValidarProfundidade(dimensao, false);

            Assert.False(resultado);
        }

        [Fact]
        public void TestProfundidadeArmarioMenorQueMinimo()
        {
            double dimensao = 50;

            bool resultado = Dimensao.ValidarProfundidade(dimensao, true);

            Assert.False(resultado);
        }

        [Fact]
        public void TestProfundidadeMenorQueMinimo()
        {
            double dimensao = 48;

            bool resultado = Dimensao.ValidarProfundidade(dimensao, false);

            Assert.False(resultado);
        }

        [Fact]
        public void TestProfundidadeArmarioValida()
        {
            double dimensao = 100;

            bool resultado = Dimensao.ValidarProfundidade(dimensao, true);

            Assert.True(resultado);
        }

        [Fact]
        public void TestProfundidadeConstituinteValida()
        {
            double dimensao = 100;

            bool resultado = Dimensao.ValidarProfundidade(dimensao, true);

            Assert.True(resultado);
        }
    }
}
