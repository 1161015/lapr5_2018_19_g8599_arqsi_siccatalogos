﻿using System;
using System.Collections.Generic;
using System.Text;
using SiC.Models;
using Xunit;
using SiC.ViewModels;

namespace SiC.Tests
{
    public class MaterialTest
    {        
 
        public MaterialTest()
        {

        }

        [Fact]
        public void TestFromDTOConversion()
        {
            Nome nome = new Nome("teste");
            Material expected = new Material(nome);
            

            NomeDTO nomeDTO = new NomeDTO("teste");
            MaterialDTO materialDTO = new MaterialDTO(nomeDTO);

            Material result = Material.fromDTO(materialDTO);

            Assert.Equal(expected, result);
        }

        [Fact]
        public void TestToDTOConversion()
        {
            NomeDTO nomeDTO = new NomeDTO("teste");
            MaterialDTO expected = new MaterialDTO(nomeDTO);

            Nome nome = new Nome("teste");
            Acabamento acabamento = new Acabamento(nome);
            Material material = new Material(nome);

            MaterialDTO result = material.toDTO();

            Assert.Equal(expected, result);
        }
    }
}
