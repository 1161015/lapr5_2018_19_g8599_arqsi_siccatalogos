﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace SiC
{
    public class Program
    {

        private const string CHAVE_TIPO_REPOSITORIO = "Persistencia.TipoFabrica";
        private const string NOME_FICHEIRO_PROPRIEDADES = "SiC.properties";
        private static readonly Dictionary<string, string> Propriedades = new Dictionary<string, string>();
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();

        public static string TipoRepositorio()
        {
            if (!Propriedades.ContainsKey(CHAVE_TIPO_REPOSITORIO)) CarregarPropriedades(); // só deve de carregar uma vez
            return Propriedades[CHAVE_TIPO_REPOSITORIO];
        }

        public static void CarregarPropriedades()
        {
            using(StreamReader streamLeitura = new StreamReader(NOME_FICHEIRO_PROPRIEDADES))
            {
                string linhaAtual;
                while (!streamLeitura.EndOfStream)
                {
                    linhaAtual = streamLeitura.ReadLine();
                    if (linhaAtual.Equals(CHAVE_TIPO_REPOSITORIO))
                    {
                        string [] linhaSeparada = linhaAtual.Split("=");
                        Propriedades[CHAVE_TIPO_REPOSITORIO] = linhaSeparada[1]; 
                    }
                }
            }
        }
    }
}
