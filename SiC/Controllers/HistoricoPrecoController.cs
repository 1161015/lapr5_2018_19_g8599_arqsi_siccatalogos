using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC.Models;
using SiC.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Controllers
{
    [Authorize]
    [Route("api/historicopreco")]
    [ApiController]
    public class HistoricoPrecoController : ControllerBase
    {
        private readonly SicContext _context;

        private readonly HistoricoPrecoService servico;

        public HistoricoPrecoController(SicContext context)
        {
            _context = context;
            servico = new HistoricoPrecoService(context);
        }

        // GET: api/HistoricoPreco
        [HttpGet]
        public IEnumerable<HistoricoPreco> GetHistoricosPreco()
        {

            List<HistoricoPreco> listRes = servico.historicosPreenchidos();

            return listRes;
        }

        // GET: api/HistoricoPreco/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetHistoricoPreco([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var historicoPreco = _context.HistoricoPrecos.Where(hp => hp.Id == id).FirstOrDefault();

            if (historicoPreco == null)
            {
                return NotFound();
            }

            return Ok(historicoPreco);
        }

        // GET: api/HistoricoPreco
        [HttpGet("material/{id:int}")]
        public IEnumerable<HistoricoPreco> GetHistoricoPrecosMaterial([FromRoute] long id)
        {
            return servico.HistoricoMaterial(id);
        }
        [AllowAnonymous]
        [HttpGet("material/atual/{id:int}")]
        public HistoricoPreco GetPrecoAtual([FromRoute] long id)
        {
            List<HistoricoPreco> precos = servico.HistoricoMaterial(id).ToList();
            precos.Sort((x, y) => DateTime.Compare(y.Preco.DataInicio, x.Preco.DataInicio));
            DateTime dataAtual = DateTime.Now;
            foreach (HistoricoPreco hp in precos)
            {
                if (DateTime.Compare(dataAtual, hp.Preco.DataInicio) >= 0) return hp;
            }
            return null;
        }

        [HttpGet("acabamento/{idAcabamento:int}/{idMaterial:int}")]
        public IEnumerable<HistoricoPreco> GetHistoricoPrecos([FromRoute] long idAcabamento, [FromRoute] long idMaterial)
        {
            return servico.HistoricoAcabamentoMaterial(idAcabamento, idMaterial);
        }
        [AllowAnonymous]
        [HttpGet("acabamento/atual/{idAcabamento:int}/{idMaterial:int}")]
        public HistoricoPreco GetPrecoAtual([FromRoute] long idAcabamento, [FromRoute] long idMaterial)
        {
            List<HistoricoPreco> precos = servico.HistoricoAcabamentoMaterial(idAcabamento, idMaterial).ToList();
            precos.Sort((x, y) => DateTime.Compare(y.Preco.DataInicio, x.Preco.DataInicio));
            DateTime dataAtual = DateTime.Now;
            foreach (HistoricoPreco hp in precos)
            {
                if (DateTime.Compare(dataAtual, hp.Preco.DataInicio) >= 0) return hp;
            }
            return null;
        }
        [HttpGet("futuro/{comparador}")]
        public IEnumerable<HistoricoPreco> GetHistoricosFuturo([FromRoute] string comparador)
        {
            return servico.HistoricosFuturos(comparador);
        }

        // POST: api/HistoricoPreco
        [HttpPost]
        public async Task<IActionResult> PostHistoricoPreco([FromBody] HistoricoPreco historicoPreco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            long idHistorico = servico.JaExiste(historicoPreco);
            if (idHistorico != -1) // se existir
            {
                servico.AtualizarHistorico(idHistorico, historicoPreco);
                await _context.SaveChangesAsync();
                return NoContent();
            }
            else // se n�o pode criar um novo
            {
                _context.HistoricoPrecos.Add(historicoPreco);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetHistoricoPreco", new { id = historicoPreco.Id }, historicoPreco);
            }

        }

        // PUT: api/Historico/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutHistorico([FromRoute] long id, [FromBody] HistoricoPreco historico)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            servico.AtualizarHistorico(id, historico);


            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HistoricoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // DELETE: api/Historico/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteHistorico([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var historico = await _context.HistoricoPrecos.Include(hp => hp.Preco).Where(hp => hp.Id == id).FirstOrDefaultAsync();
            if (historico == null)
            {
                return NotFound();
            }
            servico.ApagarHistorico(historico);
            await _context.SaveChangesAsync();

            return Ok(historico);
        }

        private bool HistoricoExists(long id)
        {
            return _context.HistoricoPrecos.Any(e => e.Id == id);
        }

    }
}