using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC.Models;
using SiC.Services;

namespace SiC.Controllers
{
    [Authorize]
    [Route("api/colecao")]
    [ApiController]
    public class ColecaoController : ControllerBase
    {
        private readonly SicContext _context;
        private readonly ColecaoService servico;

        public ColecaoController(SicContext context)
        {
            _context = context;
            servico = new ColecaoService(context);
        }

        // GET: api/Colecao
        [AllowAnonymous]
        [HttpGet]
        public IEnumerable<Colecao> GetColecoes()
        {
            return servico.ColecoesPreenchidas();
        }
        [AllowAnonymous]
        [HttpGet("catalogo/{idCatalogo}")]
        public IEnumerable<Colecao> GetColecoesCatalogo(long idCatalogo)
        {
            return servico.ColecoesDeCatalogo(idCatalogo);
        }

        // GET: api/Colecao/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetColecao([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var colecao = servico.ColecaoPreenchida(id);

            if (colecao == null)
            {
                return NotFound();
            }

            return Ok(colecao);
        }

        // PUT: api/Colecao/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutColecao([FromRoute] long id, [FromBody] Colecao colecao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!servico.AtualizarColecao(id, colecao))
            {
                return new ContentResult() { Content = "A cole��o que pretende atualizar n�o existe", StatusCode = 404 };
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ColecaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Colecao
        [HttpPost]
        public async Task<IActionResult> PostColecao([FromBody] Colecao colecao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (!servico.PreencherColecao(colecao))
            {
                return new ContentResult() { Content = "N�o existe um cat�logo com esse id", StatusCode = 406 };
            }
            _context.Colecoes.Add(colecao);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetColecao", new { id = colecao.Id }, colecao);
        }

        // DELETE: api/Colecao/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteColecao([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var colecao = await _context.Colecoes.FindAsync(id);
            if (colecao == null)
            {
                return NotFound();
            }
            servico.ApagarColecao(colecao);
            _context.Colecoes.Remove(colecao);
            await _context.SaveChangesAsync();

            return Ok(colecao);
        }

        private bool ColecaoExists(long id)
        {
            return _context.Colecoes.Any(e => e.Id == id);
        }
    }
}