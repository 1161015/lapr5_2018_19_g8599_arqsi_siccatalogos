using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC.Models;
using SiC.Services;
using SiC.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Controllers
{
    [Authorize]
    [Route("api/restricao")]
    [ApiController]
    public class RestricaoController : ControllerBase
    {
        private readonly SicContext _context;

        private readonly RestricaoService servico;

        public RestricaoController(SicContext context)
        {
            _context = context;
            servico = new RestricaoService(context);
        }

        public IEnumerable<RestricaoDTO> GetRestricoes()
        {
            List<RestricaoDTO> retorno = new List<RestricaoDTO>();
            foreach (var restricaoAtual in _context.Restricao.Include(r => r.Filho).Include(r => r.Pai))
            {
                retorno.Add(restricaoAtual.ToDTO());
            }
            return retorno;
        }

        // GET: api/Restricao/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRestricao([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var restricao = _context.Restricao.Where(r => r.Id == id).FirstOrDefault();

            if (restricao == null)
            {
                return NotFound();
            }

            return Ok(restricao);
        }

        // GET: api/Restricao/5/7
        [HttpGet("{idPai:int}/{idFilho:int}")]
        public async Task<IActionResult> GetRestricoes([FromRoute] long idPai, [FromRoute] long idFilho)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var restricao = servico.RestricaoAgregacao(idPai, idFilho);

            if (restricao == null)
            {
                return NotFound();
            }

            return Ok(restricao);
        }

        // PUT: api/Restricao/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRestricao([FromRoute] long id, [FromBody] RestricaoDTO restricaoDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if(_context.Restricao.Where( r => r.Id == id).FirstOrDefault() == null)
            {
                return new ContentResult() { Content = "N�o existe nenhuma restri��o com esse id.", StatusCode = 404 };
            }
            var restricao = Restricao.FromDTO(restricaoDTO);
            if(!servico.AtualizarRestricao(id, restricao, restricaoDTO.IdPai, restricaoDTO.IdFilho))
            {
                return new ContentResult() { Content = "{ \"message\": \"J� existe uma restri��o desse tipo.\" }", StatusCode = 406, ContentType = "application/json" };
            }
            
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RestricaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Restricao
        [HttpPost]
        public async Task<IActionResult> PostRestricao([FromBody] RestricaoDTO restricaodto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var restricao = Restricao.FromDTO(restricaodto);

            restricao.Pai = _context.Produtos.Where(p => p.ProdutoId == restricaodto.IdPai).FirstOrDefault();
            restricao.Filho = _context.Produtos.Where(p => p.ProdutoId == restricaodto.IdFilho).FirstOrDefault();
            if (!servico.VerificaAgregacao(restricao))
            {
                return StatusCode(StatusCodes.Status406NotAcceptable);
            }
            if (restricao is RestricaoTamanho)
            {
                if (servico.DuplicadoTamanho(restricao))
                {
                    return new ContentResult() { Content = "J� existe uma restri��o para os produtos selecionados", StatusCode = 406 };
                }
            }
            if (restricao is RestricaoObrigatoriedade)
            {
                if (servico.DuplicadoObrigatoriedade(restricao))
                {
                    return new ContentResult() { Content = "J� existe uma restri��o para os produtos selecionados", StatusCode = 406 };
                }
            }
            if (restricao is RestricaoMaterial)
            {
                var restricaoM = (RestricaoMaterial)restricao;
                var materialFilho = _context.Materiais.Where(m => m.Id == restricaodto.idMaterialFilho).FirstOrDefault();
                var materialPai = _context.Materiais.Where(m => m.Id == restricaodto.idMaterialPai).FirstOrDefault();
                if (materialFilho == null || materialPai == null)
                {
                    return new ContentResult() { Content = "Material Inexistente", StatusCode = 406 };
                }
                if (servico.DuplicadoMaterial(restricao))
                {
                    return new ContentResult() { Content = "J� existe uma restri��o para os produtos selecionados juntamente com os materiais selecionados.", StatusCode = 406 };
                }
                restricao = restricaoM;
            }
            _context.Restricao.Add(restricao);
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetRestricao", new { id = restricao.Id }, restricao);
        }

        // DELETE: api/Restricao/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRestricao([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var restricao = await _context.Restricao.FindAsync(id);
            if (restricao == null)
            {
                return NotFound();
            }

            _context.Restricao.Remove(restricao);
            await _context.SaveChangesAsync();

            return Ok(restricao);
        }

        private bool RestricaoExists(long id)
        {
            return _context.Restricao.Any(e => e.Id == id);
        }
    }
}