using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using SiC.Models;
using SiC.Services;
using SiC.Utils;
using SiC.ViewModels;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SiC.Controllers
{
    [Authorize]
    [Route("api/produto")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
        private readonly SicContext _context;

        private readonly ProdutoService servico;
        private readonly string urlItens = "https://desolate-savannah-83248.herokuapp.com/item/podeApagar/produto/";
        private readonly string urlDimItens = "https://desolate-savannah-83248.herokuapp.com/item/podeEditar/produto/";


        public ProdutoController(SicContext context)
        {
            _context = context;
            servico = new ProdutoService(context);
        }

        // GET: api/Produto
        [AllowAnonymous]
        [HttpGet]
        public IEnumerable<Produto> GetProdutos()
        {
            List<Produto> listRes = servico.ProdutosPreenchidos();
            return listRes;
        }
        [AllowAnonymous]
        // GET: api/Produto/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProduto([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var produto = await _context.Produtos.FindAsync(id);

            if (produto == null)
            {
                return NotFound();
            }
            // PARA DESCOMENTAR
            servico.PreencherProduto(produto);
            return Ok(produto);
        }

        [HttpGet("nome={name}")]
        public IEnumerable<Produto> GetProdutosPorNome([FromRoute] string name)
        {
            return servico.ProdutoPorNome(name);
        }
        [AllowAnonymous]
        [Route("{idPai:long}/filhos/obrigatorios")]
        public IEnumerable<Produto> GetProdutosObrigatorios(long idPai)
        {
            return servico.ProdutosObrigatorios(idPai);
        }
        [AllowAnonymous]
        [Route("{id:int}/partes")]
        public IEnumerable<Produto> GetProdutosFilho(int id)
        {
            return servico.ProdutosFilho(id);
        }

        [Route("{id:int}/parteem")]
        public IEnumerable<Produto> GetProdutosPai(int id)
        {
            return servico.ProdutosPai(id);
        }

        [Route("{id:int}/{pOcupacao:double}/restricao")]
        public async Task<IActionResult> ValidaTaxaOcupacao(int id, double pOcupacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Produto p = servico.ProdutoPorId(id);
            //bool valido = servico.verificaTaxaOcupacao(p, pOcupacao);
            bool valido = true;
            if (!valido)
            {
                return StatusCode(StatusCodes.Status406NotAcceptable);
            }

            return Ok(valido);
        }
        [AllowAnonymous]
        [Route("verProdutos/catalogo/{idCatalogo:int}")]
        public IEnumerable<Produto> VerificarProdutoCatalogo(int idCatalogo)
        {
            List<Produto> listRes = servico.ProdutosDoCatalogo(idCatalogo);
            return listRes;
        }
        [AllowAnonymous]
        [Route("verProdutos/colecao/{idColecao:int}")]
        public IEnumerable<Produto> VerificarProdutoColecao(int idColecao)
        {
            List<Produto> listRes = servico.ProdutosDaColecao(idColecao);
            return listRes;
        }


        [Route("verificarItem/{id:int}/{altura:double}/{largura:double}/{profundidade:double}")]
        public async Task<IActionResult> ValidarDimensoesItem(int id, double altura, double largura, double profundidade)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Produto p = servico.ProdutoPorId(id);
            bool valido = servico.VerificarDimensoesItem(p, altura, largura, profundidade);

            if (!valido)
            {
                return StatusCode(StatusCodes.Status406NotAcceptable);
            }

            return Ok(valido);
        }

        [Route("verificarItem/{idProduto:int}/{idMaterial:int}/{idAcabamento:int}")]
        public async Task<IActionResult> ValidarMaterialAcabamentoItem(int idProduto, int idMaterial, int idAcabamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Produto p = servico.ProdutoPorId(idProduto);
            bool valido = servico.VerificarMaterialAcabamentoItem(p, idMaterial, idAcabamento);

            if (!valido)
            {
                return StatusCode(StatusCodes.Status406NotAcceptable);
            }

            return Ok(valido);
        }

        // PUT: api/Produto/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduto([FromRoute] long id, [FromBody] ProdutoDTO produtodto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Produto produto = Produto.FromDTO(produtodto);
            //if (!servico.CumpreTaxaOcupacao(produto))
            //{
            //    return StatusCode(StatusCodes.Status406NotAcceptable);
            //}
            produto.ProdutoId = id;
            if (produto.ProdutosAgregados != null)
            {
                if (produtodto.ProdutosAgregados.Count > 0)
                {
                    var servicoCategoria = new CategoriaService(_context);
                    // sendo pai
                    if (!servicoCategoria.PodeTerFilhos((long)produtodto.Categoria.Id))
                    {
                        string err = "{ \"message\":\"Apenas a categ�ria arm�rio ou as suas subcategorias podem conter itens filhos.\"}";
                        return new ContentResult() { Content = err, StatusCode = 406, ContentType = "application/json" };
                    }
                }
            }
            var clienteHttp = new HttpClient();

            var tokenHandler = new JwtSecurityTokenHandler();
            byte[] key = null;

            key = Encoding.ASCII.GetBytes("6cQ4vG4aeP6ATcJulsoiQBOUawU5VciM");


            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Expires = DateTime.UtcNow.AddSeconds(15),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            clienteHttp.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenHandler.WriteToken(token));
            var resposta = await clienteHttp.GetAsync(urlDimItens + id);
            List<Triplo<double, double, double>> podeEditar = null;
            if (resposta.IsSuccessStatusCode)
            {
                podeEditar = await resposta.Content.ReadAsAsync<List<Triplo<double, double, double>>>();
            }
            if (podeEditar != null)
            {
                if (podeEditar.Count > 0)
                {

                    bool state = servico.verificarDimensoes(podeEditar, produto);
                    if (!state)
                    {
                        string err = "{ \"message\":\"Imposs�vel de alterar pois existem itens cujas dimens�es ficariam inv�lidas.\"}";
                        return new ContentResult() { Content = err, StatusCode = 406, ContentType = "application/json" };
                    }
                }

                Produto pdt = _context.Produtos.Include(p => p.Categoria).Where(p => p.ProdutoId == id).FirstOrDefault();
                if (pdt != null)
                {
                    if (pdt.Categoria.Id != produtodto.Categoria.Id)
                    {

                        if (podeEditar.Count > 0)
                        {
                            string err = "{ \"message\":\"Itens referenciados com este produto, imposs�vel mudar categoria!\"}";
                            return new ContentResult() { Content = err, StatusCode = 406, ContentType = "application/json" };
                        }
                    }
                }



            }

            if (!servico.AdicionarFilhos(produto))
            {
                string err = "{ \"message\":\"Imposs�vel de adicionar filhos, verifique as restri��es e as dimens�es!\"}";
                return new ContentResult() { Content = err, StatusCode = 406, ContentType = "application/json" };
            }
            if (!servico.VerificarTamanho(produto))
            {
                string err = "{ \"message\":\"Verifique o tamanho do arm�rio e dos seus produtos filhos!\"}";
                return new ContentResult() { Content = err, StatusCode = 406, ContentType = "application/json" };
            }
            // sendo filho
            if (!servico.VerificaComoFilho(produto))
            {
                string err = "{ \"message\":\"Um dos produtos filhos cont�m dimens�es superiores �s do pai!\"}";
                return new ContentResult() { Content = err, StatusCode = 406, ContentType = "application/json" };
            }

            var p1 = _context.Produtos.Include(p => p.Dimensoes).Include(p => p.Colecao).ThenInclude(c => c.Catalogo).Include(p => p.ProdutoCatalogos).Include(p => p.Materiais).Include(p => p.ProdutosAgregados).Where(p => p.ProdutoId == id).First();
            servico.AtualizarProduto(p1, produto);
            _context.Entry(p1).State = EntityState.Modified;
            _context.Produtos.Update(p1);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProdutoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Produto
        [HttpPost]
        public async Task<IActionResult> PostProduto([FromBody] ProdutoDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var produto = Produto.FromDTO(dto);
            // sendo pai
            if (dto.ProdutosAgregados != null)
            {
                if (dto.ProdutosAgregados.Count > 0)
                {
                    var servicoCategoria = new CategoriaService(_context);
                    if (!servicoCategoria.PodeTerFilhos((long)dto.Categoria.Id))
                    {
                        string err = "{ \"message\":\"Apenas a categ�ria arm�rio ou as suas subcategorias podem conter itens filhos.\"}";
                        return new ContentResult() { Content = err, StatusCode = 406, ContentType = "application/json" };
                    }
                }
            }

            //if (!servico.CumpreTaxaOcupacao(produto))
            //{
            //    return StatusCode(StatusCodes.Status406NotAcceptable);
            //}

            if (!servico.AdicionarFilhos(produto))
            {
                string err = "{ \"message\":\"Imposs�vel de adicionar filhos, verifique as restri��es e as dimens�es!\"}";
                return new ContentResult() { Content = err, StatusCode = 406, ContentType = "application/json" };
            }

            if (!servico.AtualizarProduto(produto))
            {
                return StatusCode(StatusCodes.Status406NotAcceptable);
            }

            if (!servico.VerificarTamanho(produto))
            {
                string err = "{ \"message\":\"Verifique o tamanho do arm�rio e dos seus produtos filhos!\"}";
                return new ContentResult() { Content = err, StatusCode = 406, ContentType = "application/json" };
            }
            _context.Produtos.Add(produto);
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetProduto", new { id = produto.ProdutoId }, produto);
        }

        [HttpPost("obrigatoriedade")]
        public bool VerificarObrigatoriedade([FromBody] ItemDTO itemDTO)
        {
            return servico.VerificarObrigatoriedade(itemDTO);
        }

        [HttpPost("itensfilhos")]
        public bool VerificarItensFilhos([FromBody] ItemDTO itemDTO)
        {
            return servico.VerificarProdutosFilhosExistem(itemDTO);
        }

        [HttpPost("materialacabamento")]
        public bool VerificarMaterialAcabamentoObrigatoriedade([FromBody] ItemDTO itemDTO)
        {
            return servico.VerificarMaterialAcabamentoObrigatoriedade(itemDTO);
        }

        [HttpPost("taxapercentual")]
        public bool ValidarTaxaPercentual([FromBody] ItemDTO itemDTO)
        {
            return servico.VerificarTaxaPercentual(itemDTO);
        }

        // DELETE: api/Produto/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduto([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var produto = _context.Produtos.Include(p => p.Dimensoes).ThenInclude(d => d.Altura)
                .Include(p => p.Dimensoes).ThenInclude(d => d.Largura)
                .Include(p => p.Dimensoes).ThenInclude(d => d.Profundidade)
                .Include(p => p.Materiais).Include(p => p.ProdutosAgregados).Where(p => p.ProdutoId == id).FirstOrDefault();
            if (produto == null)
            {
                return NotFound();
            }
            var clienteHttp = new HttpClient();

            var tokenHandler = new JwtSecurityTokenHandler();
            byte[] key = null;

            key = Encoding.ASCII.GetBytes("6cQ4vG4aeP6ATcJulsoiQBOUawU5VciM");


            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Expires = DateTime.UtcNow.AddSeconds(15),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            clienteHttp.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenHandler.WriteToken(token));
            var resposta = await clienteHttp.GetAsync(urlItens + id);
            var podeApagar = true;
            if (resposta.IsSuccessStatusCode)
            {
                podeApagar = await resposta.Content.ReadAsAsync<bool>();
            }
            if (!podeApagar)
            {
                string err = "{ \"message\":\"Este produto j� � referenciado em itens, imposs�vel apagar.\"}";
                return new ContentResult() { Content = err, StatusCode = 406, ContentType = "application/json" };
            }
            servico.RemoverReferencias(produto);
            _context.Produtos.Remove(produto);
            await _context.SaveChangesAsync();

            return Ok(produto);
        }

        private bool ProdutoExists(long id)
        {
            return _context.Produtos.Any(e => e.ProdutoId == id);
        }
    }


}