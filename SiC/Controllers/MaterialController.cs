using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using SiC.Models;
using SiC.Services;


namespace SiC.Controllers
{
    [Authorize]
    [Route("api/material")]
    [ApiController]
    public class MaterialController : ControllerBase
    {
        private readonly SicContext _context;

        private readonly MaterialService servico;
        private readonly string urlItens = "https://desolate-savannah-83248.herokuapp.com/item/podeApagar/material/";

        public MaterialController(SicContext context)
        {
            _context = context;
            servico = new MaterialService(context);
        }

        // GET: api/Material
        [AllowAnonymous]
        [HttpGet]
        public IEnumerable<Material> GetMateriais()
        {

            List<Material> listRes = servico.MateriaisPreenchidos();
            return listRes;
        }
        [AllowAnonymous]
        [HttpGet("materiaisProduto/{idProduto:int}")]
        public IEnumerable<Material> GetMateriaisProduto(int idProduto)
        {
            var listaMateriaisProduto = servico.MateriaisProduto(idProduto);
            return listaMateriaisProduto;
        }
        [AllowAnonymous]
        // GET: api/Material/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMaterial([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var material = await _context.Materiais.FindAsync(id);
          
            if (material == null)
            {
                return NotFound();
            }
            servico.PreencherMaterial(material);
            return Ok(material);
        }

        // PUT: api/Material/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMaterial([FromRoute] long id, [FromBody] Material material)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            servico.AtualizarMaterial(id, material);


            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MaterialExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Material
        [HttpPost]
        public async Task<IActionResult> PostMaterial([FromBody] Material material)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Materiais.Add(material);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMaterial", new { id = material.Id }, material);
        }

        // DELETE: api/Material/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMaterial([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var material = await _context.Materiais.FindAsync(id);
            if (material == null)
            {
                return NotFound();
            }
            var clienteHttp = new HttpClient();

            var tokenHandler = new JwtSecurityTokenHandler();
            byte[] key = null;

            key = Encoding.ASCII.GetBytes("6cQ4vG4aeP6ATcJulsoiQBOUawU5VciM");


            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Expires = DateTime.UtcNow.AddSeconds(15),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            clienteHttp.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenHandler.WriteToken(token));
            var resposta = await clienteHttp.GetAsync(urlItens + id);
            var podeApagar = true;
            if (resposta.IsSuccessStatusCode)
            {
                podeApagar = await resposta.Content.ReadAsAsync<bool>();
            }
            if (!podeApagar)
            {
                string err = "{ \"message\":\"Este material j� � referenciado em itens, imposs�vel apagar.\"}";
                return new ContentResult() { Content = err, StatusCode = 406, ContentType = "application/json" };
            }
            if (!servico.VerificarMaterial(material))
            {
                 return new ContentResult() { Content = "{ \"message\": \"Material �nico num produto, imposs�vel eliminar este material.\" }", StatusCode = 404, ContentType= "application/json" };

            }
            servico.ApagarMaterial(material);
            await _context.SaveChangesAsync();

            return Ok(material);
        }

        private bool MaterialExists(long id)
        {
            return _context.Materiais.Any(e => e.Id == id);
        }
    }
}