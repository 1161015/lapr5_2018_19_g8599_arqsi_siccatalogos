using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using SiC.Models;
using SiC.Services;

namespace SiC.Controllers
{
    [Authorize]
    [Route("api/acabamento")]
    [ApiController]
    public class AcabamentoController : ControllerBase
    {
        private readonly SicContext _context;
        private readonly AcabamentoService servico;
        private readonly string urlItens = "https://desolate-savannah-83248.herokuapp.com/item/podeApagar/acabamento/";

        public AcabamentoController(SicContext context)
        {
            _context = context;
            servico = new AcabamentoService(context);
        }
        [AllowAnonymous]
        // GET: api/Acabamento
        [HttpGet]
        public IEnumerable<Acabamento> GetAcabamentos()
        {
            List<Acabamento> listRes = new List<Acabamento>();
            
               listRes = servico.AcabamentosPreenchidosAsync();
            

                
            return listRes;
        }
        [AllowAnonymous]
        [HttpGet("material/{idMaterial}")]
        public IEnumerable<Acabamento> GetAcabamentosDeMaterial(long idMaterial)
        {
            return servico.AcabamentosMaterial(idMaterial);
        }
        [AllowAnonymous]
        // GET: api/Acabamento/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAcabamento([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var acabamento = await _context.Acabamentos.FindAsync(id);

            if (acabamento == null)
            {
                return NotFound();
            }
            servico.PreencherAcabamento(acabamento);
            return Ok(acabamento);
        }

        // PUT: api/Acabamento/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAcabamento([FromRoute] long id, [FromBody] Acabamento acabamento)
        {
            
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            servico.AtualizarAcabamento(id, acabamento);
            



            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AcabamentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Acabamento
        [HttpPost]
        public async Task<IActionResult> PostAcabamento([FromBody] Acabamento acabamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Acabamentos.Add(acabamento);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAcabamento", new { id = acabamento.Id }, acabamento);
        }

        // DELETE: api/Acabamento/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAcabamento([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }



            var acabamento = await _context.Acabamentos.FindAsync(id);
            if (acabamento == null)
            {
                return NotFound();
            }
            var clienteHttp = new HttpClient();

            var tokenHandler = new JwtSecurityTokenHandler();
            byte[] key = null;

                key = Encoding.ASCII.GetBytes("6cQ4vG4aeP6ATcJulsoiQBOUawU5VciM");


            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Expires = DateTime.UtcNow.AddSeconds(15),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            clienteHttp.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenHandler.WriteToken(token));
            var resposta = await clienteHttp.GetAsync(urlItens + id);
            var podeApagar = true;
            if (resposta.IsSuccessStatusCode)
            {
                podeApagar = await resposta.Content.ReadAsAsync<bool>();
            }
            if (!podeApagar)
            {
                string err = "{ \"message\":\"Este acabamento j� � referenciado em itens, imposs�vel apagar.\"}";
                return new ContentResult() { Content = err, StatusCode = 406, ContentType = "application/json" };
            }
            if (!servico.VerificarAcabamento(acabamento))
            {
                
                return new ContentResult() { Content = "{ \"message\": \"Acabamento � �nico num material. Imposs�vel apagar.\" }", StatusCode = 404, ContentType = "application/json" };

            }

            servico.ApagarAcabamento(acabamento);

            await _context.SaveChangesAsync();

            return Ok(acabamento);
        }

        private bool AcabamentoExists(long id)
        {
            return _context.Acabamentos.Any(e => e.Id == id);
        }
    }
}