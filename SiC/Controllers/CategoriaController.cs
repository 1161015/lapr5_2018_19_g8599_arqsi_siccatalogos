using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC.Models;
using SiC.Services;

namespace SiC.Controllers
{
    //[Authorize]
    [Route("api/categoria")]
    [ApiController]
    public class CategoriaController : ControllerBase
    {
        private readonly SicContext _context;

        private readonly CategoriaService servico;

        public CategoriaController(SicContext context)
        {
            _context = context;
            servico = new CategoriaService(context);
        }
        [AllowAnonymous]
        // GET: api/Categoria
        [HttpGet]
        public IEnumerable<Categoria> GetCategorias()
        {
            return _context.Categorias.Include(c => c.CategoriaPai);
        }
        [AllowAnonymous]
        // GET: api/Categoria/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCategoria([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var categoria = await _context.Categorias.FindAsync(id);
            servico.preencherCategoria(categoria);
            if (categoria == null)
            {
                return NotFound();
            }

            return Ok(categoria);
        }
        [AllowAnonymous]
        [HttpGet("podeTerFilhos/{idCategoria:int}")]
        public bool VerificarPodeFilhos([FromRoute] long idCategoria)
        {
            return servico.PodeTerFilhos(idCategoria);
        }

        // PUT: api/Categoria/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCategoria([FromRoute] long id, [FromBody] Categoria categoria)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            servico.AtualizarCategoria(id,categoria);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoriaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Categoria
        [HttpPost]
        public async Task<IActionResult> PostCategoria([FromBody] Categoria categoria)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            servico.preencherCategoriaPost(categoria);
            _context.Categorias.Add(categoria);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCategoria", new { id = categoria.Id }, categoria);
        }

        // DELETE: api/Categoria/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategoria([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var categoria = await _context.Categorias.FindAsync(id);
            if (categoria == null)
            {
                return NotFound();
            }
            bool res = servico.VerificarCategoria(categoria);
            if (!res)
            {
                string err = "{ \"message\":\"Ainda existem produtos desta categoria - Impossível apagar!\"}";
                return new ContentResult() { Content = err, StatusCode = 406, ContentType = "application/json" };
            }
            servico.ApagarCategoria(categoria);
            _context.Categorias.Remove(categoria);
            await _context.SaveChangesAsync();

            return Ok(categoria);
        }

        private bool CategoriaExists(long id)
        {
            return _context.Categorias.Any(e => e.Id == id);
        }
    }
}