using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC.Models;
using SiC.Services;

namespace SiC.Controllers
{
    [Authorize]
    [Route("api/catalogo")]
    [ApiController]
    public class CatalogoController : ControllerBase
    {
        private readonly SicContext _context;
        private readonly CatalogoService servico;

        public CatalogoController(SicContext context)
        {
            _context = context;
            servico = new CatalogoService(context);
        }

        // GET: api/Catalogo
        [HttpGet]
        [AllowAnonymous]
        public IEnumerable<Catalogo> GetCatalogos()
        {
            return servico.CatalogosPreenchidos();
        }

        // GET: api/Catalogo/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCatalogo([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var catalogo = servico.CatalogoPreenchido(id);

            if (catalogo == null)
            {
                return NotFound();
            }

            return Ok(catalogo);
        }

        // PUT: api/Catalogo/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCatalogo([FromRoute] long id, [FromBody] Catalogo catalogo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!servico.AtualizarCatalogo(id, catalogo))
            {
                return new ContentResult() { Content = "Erro ao atualizar o cat�logo", StatusCode = 404 };
            }
           

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CatalogoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        // POST: api/Catalogo
        [HttpPost]
        public async Task<IActionResult> PostCatalogo([FromBody] Catalogo catalogo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Catalogos.Add(catalogo);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCatalogo", new { id = catalogo.CatalogoId }, catalogo);
        }

        [HttpGet("produto/{idProduto}")]
        public IEnumerable<Catalogo> GetColecoesCatalogo(long idProduto)
        {
            return servico.CatalogosProduto(idProduto);
        }

        // DELETE: api/Catalogo/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCatalogo([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var catalogo = await _context.Catalogos.FindAsync(id);
            if (catalogo == null)
            {
                return NotFound();
            }
            if (!servico.VerificarCatalogo(catalogo))
            {
                return new ContentResult() { Content = "{ \"message\": \"Catalogo �nico num produto, imposs�vel eliminar este catalogo.\" }", StatusCode = 404, ContentType = "application/json" };
            }
            servico.ApagarCatalogo(catalogo);
            _context.Catalogos.Remove(catalogo);
            await _context.SaveChangesAsync();

            return Ok(catalogo);
        }

        

        private bool CatalogoExists(long id)
        {
            return _context.Catalogos.Any(e => e.CatalogoId == id);
        }
    }
}