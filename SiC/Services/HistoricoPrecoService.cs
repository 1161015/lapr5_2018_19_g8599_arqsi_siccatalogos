﻿using Microsoft.EntityFrameworkCore;
using SiC.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SiC.Services
{
    public class HistoricoPrecoService
    {
        private readonly SicContext contexto;

        private HistoricoPrecoService()
        {

        }


        public HistoricoPrecoService(SicContext contexto)
        {
            this.contexto = contexto;
        }

        public List<HistoricoPreco> historicosPreenchidos()
        {
            List<HistoricoPreco> resultado = new List<HistoricoPreco>();
            foreach (HistoricoPreco hp in contexto.HistoricoPrecos)
            {
                resultado.Add(hp);
            }
            return resultado;
        }

        public long JaExiste(HistoricoPreco historicoPreco)
        {
            HistoricoPreco historicoDB = null;
            if (historicoPreco.Id_Acabamento == null)
            {
                historicoDB = contexto.HistoricoPrecos.Where(hp => hp.Id_Material == historicoPreco.Id_Material && hp.Id_Acabamento == null && hp.Preco.DataInicio.Equals(historicoPreco.Preco.DataInicio)).FirstOrDefault();
            }
            else
            {
                historicoDB = contexto.HistoricoPrecos.Where(hp => hp.Id_Material == historicoPreco.Id_Material && hp.Id_Acabamento == historicoPreco.Id_Acabamento && hp.Preco.DataInicio.Equals(historicoPreco.Preco.DataInicio)).FirstOrDefault();
            }
            if (historicoDB == null) return -1;
            return historicoDB.Id;
        }


        public IEnumerable<HistoricoPreco> HistoricosFuturos(string comparador)
        {
            var lista = contexto.HistoricoPrecos.Include(hp => hp.Preco).Where(hp => hp.Preco.DataInicio > DateTime.Now);
            var list = lista.ToList();
            if (comparador.Equals("data", StringComparison.InvariantCultureIgnoreCase))
            {
                list.Sort((x, y) => DateTime.Compare(x.Preco.DataInicio, y.Preco.DataInicio));
            }
            else if (comparador.Equals("material", StringComparison.InvariantCultureIgnoreCase))
            {
                list.Sort((x, y) => x.Id_Material.CompareTo(y.Id_Material));
            }

            return list;
        }
        public IEnumerable<HistoricoPreco> HistoricoMaterial(long idMaterial)
        {
            var lista = contexto.HistoricoPrecos.Include(h => h.Preco).Where(h => h.Id_Material == idMaterial && h.Id_Acabamento == null);
            List<HistoricoPreco> listaOrdenada = new List<HistoricoPreco>();
            listaOrdenada = lista.ToList();
            listaOrdenada.Sort((x, y) => DateTime.Compare(x.Preco.DataInicio, y.Preco.DataInicio));
            return listaOrdenada;
        }

        public IEnumerable<HistoricoPreco> HistoricoAcabamentoMaterial(long idAcabamento, long idMaterial)
        {
            var lista = contexto.HistoricoPrecos.Include(h => h.Preco).Where(h => h.Id_Acabamento == idAcabamento && h.Id_Material == idMaterial);
            List<HistoricoPreco> listaOrdenada = new List<HistoricoPreco>();
            listaOrdenada = lista.ToList();
            listaOrdenada.Sort((x, y) => DateTime.Compare(x.Preco.DataInicio, y.Preco.DataInicio));
            return listaOrdenada;
        }

        public void AtualizarHistorico(long id, HistoricoPreco historico)
        {

            var temp = contexto.HistoricoPrecos.Where<HistoricoPreco>(a => a.Id == id);
            HistoricoPreco aux = temp.First<HistoricoPreco>();
            if (historico.Id_Acabamento != null)
            {
                aux.Id_Acabamento = historico.Id_Acabamento;
            }
            aux.Id_Material = historico.Id_Material;
            aux.Preco.valor = historico.Preco.valor;
            aux.Preco.DataInicio = historico.Preco.DataInicio;
            contexto.Entry(aux).State = EntityState.Modified;
            contexto.HistoricoPrecos.Update(aux);

        }

        public void ApagarHistorico(HistoricoPreco historico)
        {
            contexto.Entry(historico).State = EntityState.Deleted;
            contexto.HistoricoPrecos.Remove(historico);
        }
    }
}
