﻿using Microsoft.EntityFrameworkCore;
using SiC.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SiC.Services
{
    public class CatalogoService
    {
        private readonly SicContext contexto;

        public CatalogoService(SicContext contexto)
        {
            this.contexto = contexto;
        }

        public IEnumerable<Catalogo> CatalogosPreenchidos()
        {
            var lista = new List<Catalogo>();

            foreach (var catalogo in contexto.Catalogos.Include(c => c.ProdutoCatalogos))
            {
                lista.Add(catalogo);
            }
            return lista;
        }

        public Catalogo CatalogoPreenchido(long idCatalogo)
        {
            return contexto.Catalogos.Where(c => c.CatalogoId == idCatalogo).Include(c => c.ProdutoCatalogos).FirstOrDefault();
        }

        public void ApagarCatalogo(Catalogo catalogo)
        {
            // apagar todas as coleções dentro do catálogo
            ColecaoService servicoColecao = new ColecaoService(contexto);
            var listaColecoes = contexto.Colecoes.Where(c => c.Catalogo.CatalogoId == catalogo.CatalogoId); // buscar todas as coleções que 
            //pertençam ao catálogo que vai ser eliminado
            foreach (var colecaoAtual in listaColecoes)
            {
                servicoColecao.ApagarColecao(colecaoAtual); // apagar referência da coleção nos produtos
                contexto.Colecoes.Remove(colecaoAtual); // só depois apagar a coleção
            }
        }

        public bool AtualizarCatalogo(long idCatalogo, Catalogo catalogo)
        {
            var catalogoDB = contexto.Catalogos.Where(cat => cat.CatalogoId == idCatalogo).FirstOrDefault();
            if (catalogoDB == null) return false;
            catalogoDB.Nome.Conteudo = catalogo.Nome.Conteudo;
            List<ProdutoCatalogo> produtosCatalogos = new List<ProdutoCatalogo>();
            bool state = false;
            var listaProdutoCatalogoApagar = contexto.ProdutoCatalogo.Include(pc => pc.Produto).ThenInclude(p => p.ProdutoCatalogos).Where(pc => pc.CatalogoId == idCatalogo);
            foreach(ProdutoCatalogo pc in listaProdutoCatalogoApagar)
            {
                foreach(ProdutoCatalogo pc2 in catalogo.ProdutoCatalogos)
                {
                    if (pc2.ProdutoId == pc.ProdutoId)
                    {
                        state = true;
                        break;
                    }
                }
                if (!state)
                {
                    if (pc.Produto.ProdutoCatalogos == null) return false;
                    if (pc.Produto.ProdutoCatalogos.Count == 1) return false;
                }
                state = false;
            }
            foreach (var pc in listaProdutoCatalogoApagar)
            {
                contexto.ProdutoCatalogo.Remove(pc);
            }
            if (catalogo.ProdutoCatalogos != null) { 
                foreach (var produtoCatalogo in catalogo.ProdutoCatalogos)
                {
                    produtoCatalogo.CatalogoId = catalogoDB.CatalogoId; // inserir a referência do catálogo
                    produtosCatalogos.Add(produtoCatalogo);
                }
            }
            catalogoDB.ProdutoCatalogos = produtosCatalogos;
            contexto.Entry(catalogoDB).State = EntityState.Modified;
            return true;
        }

        public IEnumerable<Catalogo> CatalogosProduto(long idProduto)
        {
            var lista= contexto.ProdutoCatalogo.Where<ProdutoCatalogo>(pc => pc.ProdutoId == idProduto).Include(pc => pc.Catalogo);
            var listaRetorno = new List<Catalogo>();
            foreach(var pcAtual in lista)
            {
                listaRetorno.Add(pcAtual.Catalogo);
            }
            return listaRetorno;
        }

        public bool VerificarCatalogo(Catalogo catalogo)
        {
            IEnumerable<ProdutoCatalogo> produtos = contexto.ProdutoCatalogo.Include(pm => pm.Produto).ThenInclude(p => p.ProdutoCatalogos).Where(m => m.CatalogoId == catalogo.CatalogoId);
            if (produtos == null) return true;
            foreach (ProdutoCatalogo pm in produtos)
            {
                if (pm.Produto.ProdutoCatalogos.Count== 1) return false;
            }
            return true;
        }
    }
}
