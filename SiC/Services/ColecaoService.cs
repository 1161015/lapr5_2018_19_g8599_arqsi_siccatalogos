﻿using Microsoft.EntityFrameworkCore;
using SiC.Models;
using System.Collections.Generic;
using System.Linq;

namespace SiC.Services
{
    public class ColecaoService
    {
        private readonly SicContext contexto;

        public ColecaoService(SicContext contexto)
        {
            this.contexto = contexto;
        }

        public IEnumerable<Colecao> ColecoesPreenchidas()
        {
            List<Colecao> lista = new List<Colecao>();
            foreach (var colecao in contexto.Colecoes.Include(c => c.Catalogo.Nome).Include(c=> c.Catalogo.ProdutoCatalogos))
            {
                lista.Add(colecao);
            }
            return lista;
        }

        public IEnumerable<Colecao> ColecoesDeCatalogo(long idCatalogo)
        {
            List<Colecao> lista = new List<Colecao>();
            foreach (var colecao in contexto.Colecoes.Include(c => c.Catalogo.Nome).Include(c => c.Catalogo.ProdutoCatalogos).Where(col => col.Catalogo.CatalogoId==idCatalogo))
            {
                lista.Add(colecao);
            }
            return lista;
        }



        public Colecao ColecaoPreenchida(long idColecao)
        {
            return contexto.Colecoes.Where(c => c.Id == idColecao).Include(c => c.Catalogo).FirstOrDefault();
        }

        public bool PreencherColecao(Colecao colecao)
        {
            return (colecao.Catalogo = contexto.Catalogos.Where(catal => catal.CatalogoId == colecao.Catalogo.CatalogoId).FirstOrDefault()) != null;
        }

        public void ApagarColecao(Colecao colecao)
        {
            var listaProdutosColecao = contexto.Produtos.Where(p => p.Colecao.Id == colecao.Id).Include(p => p.Colecao);
            foreach (var produtoAtual in listaProdutosColecao)
            {
                produtoAtual.Colecao = null; // apagar referência
                contexto.Produtos.Update(produtoAtual);
            }

        }

        public bool AtualizarColecao(long idColecao, Colecao colecao)
        {
            var colecaoBD = contexto.Colecoes.Where(c => c.Id == idColecao).Include(c => c.Catalogo).FirstOrDefault();
            if (colecaoBD == null) return false;
            if (colecao.Nome != null)
            {
                colecaoBD.Nome.Conteudo = colecao.Nome.Conteudo;
            }
       
            if (colecao.Catalogo != null)
            {
                var novoCatalogo = contexto.Catalogos.Where(c => c.CatalogoId == colecao.Catalogo.CatalogoId).FirstOrDefault();
                if (novoCatalogo != null)
                {
                    colecaoBD.Catalogo = novoCatalogo;
                }
               
            }
            contexto.Entry(colecaoBD).State = EntityState.Modified;
            return true;
        }

        public Colecao ColecaoPorId(long id)
        {
            Colecao c = contexto.Colecoes.Where<Colecao>(colecao => colecao.Id == id).First();
            PreencherColecao(c);
            return c;
        }
    }
}
