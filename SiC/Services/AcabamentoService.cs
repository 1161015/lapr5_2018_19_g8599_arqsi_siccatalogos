﻿using Microsoft.EntityFrameworkCore;
using SiC.Models;
using SiC.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Services
{
    public class AcabamentoService
    {
        private readonly SicContext contexto;

        private AcabamentoService()
        {

        }

        public AcabamentoService(SicContext contexto)
        {
            this.contexto = contexto;
        }

        public Acabamento ByName(string name)
        {
            foreach(Acabamento a in contexto.Acabamentos)
            {
                if (a.Nome.Equals(name))
                {
                    return a;
                }
            } 
            {

            }
            return null;
        }

        public IEnumerable<Acabamento> AcabamentosMaterial(long idMaterial)
        {
            var lista = contexto.MaterialAcabamento.Where(ma => ma.IdMaterial == idMaterial).Include(ma => ma.Acabamento);
            var retorno = new List<Acabamento>();
            foreach(var maAtual in lista)
            {
                retorno.Add(maAtual.Acabamento);
            }
            return retorno;
        }

        public List<Acabamento> AcabamentosPreenchidosAsync()
        {
            List<Acabamento> resultado = new List<Acabamento>();
           foreach (Acabamento a in contexto.Acabamentos)
            {
                PreencherAcabamento(a);
                resultado.Add(a);
            }

            return resultado;
        }

        public void PreencherAcabamento(Acabamento a)
        {
            foreach (MaterialAcabamento ma in contexto.MaterialAcabamento)
            {
                if (a.Id == ma.IdAcabamento)
                {
                    a.Materiais.Add(ma);

                }
            }
        }


        public void ApagarAcabamento(Acabamento acabamento)
        {
            var temp = contexto.MaterialAcabamento.Where<MaterialAcabamento>(a => a.IdAcabamento == acabamento.Id);
            foreach(MaterialAcabamento ma in temp)
            {
                contexto.MaterialAcabamento.Remove(ma);
            }
            List<HistoricoPreco> precos = contexto.HistoricoPrecos.Where(hp => hp.Id_Acabamento == acabamento.Id).ToList();
            foreach (HistoricoPreco hp in precos)
            {
                contexto.HistoricoPrecos.Remove(hp);
            }
            contexto.Entry(acabamento).State = EntityState.Deleted;
            contexto.Acabamentos.Remove(acabamento);

        }
        public void AtualizarAcabamento (long id, Acabamento acabamento)
        {
            var temp = contexto.Acabamentos.Where<Acabamento>(a => a.Id == id);
            Acabamento aux = temp.First<Acabamento>();
            aux.Materiais = acabamento.Materiais;
            aux.Nome = acabamento.Nome;
            aux.Brilho = acabamento.Brilho;
            aux.Emissividade = acabamento.Emissividade;
            aux.Fosco = acabamento.Fosco;
            aux.Cor = acabamento.Cor;
            aux.Opacidade = acabamento.Opacidade;
            contexto.Entry(aux).State = EntityState.Modified;
            contexto.Acabamentos.Update(aux);

        }

        public bool VerificarAcabamento(Acabamento acabamento)
        {
            IEnumerable<MaterialAcabamento> materiais = contexto.MaterialAcabamento.Include(pm => pm.Material).ThenInclude(p => p.Acabamentos).Where(m => m.IdAcabamento == acabamento.Id);
            if (materiais == null) return true;
            foreach (MaterialAcabamento pm in materiais)
            {
                if (pm.Material.Acabamentos.Count == 1) return false;
            }
            return true;
        }
    }
}
