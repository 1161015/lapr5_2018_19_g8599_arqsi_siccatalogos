﻿using Microsoft.EntityFrameworkCore;
using SiC.Models;
using System;
using System.Linq;

namespace SiC.Services
{
    public class CategoriaService
    {

        private readonly SicContext contexto;

        private CategoriaService()
        {

        }

        public CategoriaService(SicContext contexto)
        {
            this.contexto = contexto;
        }

        public Categoria CategoriaPorNome(string name)
        {
            foreach (Categoria a in contexto.Categorias)
            {
                if (a.Nome.Equals(name))
                {
                    return a;
                }
            }
            return null;
        }

        public void preencherCategoria(Categoria categoria)
        {
            Categoria categoriaAux = contexto.Categorias.Where<Categoria>(c => c.Id == categoria.Id).Include(c => c.CategoriaPai).FirstOrDefault();
            categoria = categoriaAux;
        }


        public bool PodeTerFilhos(long idCategoria)
        {
            Categoria categoriaAux = contexto.Categorias.Where<Categoria>(c => c.Id == idCategoria).Include(c => c.CategoriaPai).FirstOrDefault();

            if (categoriaAux == null) return false;
            Preencher(categoriaAux);
            var resultado = PodeTerFilhos(categoriaAux);
            return resultado;
        }

        private void Preencher(Categoria categoria)
        {
            if (categoria.CategoriaPai != null)
            {
                var paiAtual = categoria.CategoriaPai;
                var categoriaAtual = contexto.Categorias.Where<Categoria>(c => c.Id == paiAtual.Id).Include(c => c.CategoriaPai).FirstOrDefault();
                Preencher(categoriaAtual);
            }
            return;
        }


        private bool PodeTerFilhos(Categoria categoria)
        {
            if (categoria.Nome.Conteudo.ToLower().Equals("armário", StringComparison.InvariantCultureIgnoreCase)
                || categoria.Nome.Conteudo.ToLower().Equals("armario", StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }
            else if (categoria.CategoriaPai != null)
            {
                return PodeTerFilhos(categoria.CategoriaPai);
            }
            else
            {
                return false;
            }

        }

        public void preencherCategoriaPost(Categoria categoria)
        {
            Categoria categoriaPai = null;
            if (categoria.CategoriaPai != null)
            {
                categoriaPai = contexto.Categorias.Where<Categoria>(c => c.Id == categoria.CategoriaPai.Id).FirstOrDefault();
            }
            categoria.CategoriaPai = categoriaPai;
        }

        public void AtualizarCategoria(long id, Categoria categoria)
        {
            var temp = contexto.Categorias.Where<Categoria>(a => a.Id == id);
            Categoria aux = temp.First<Categoria>();
            aux.Nome = categoria.Nome;
            preencherCategoriaPost(categoria);
            if (categoria.CategoriaPai != null)
            {
                aux.CategoriaPai = categoria.CategoriaPai;
            }
            else
            {
                contexto.Entry(aux).Property("CategoriaPaiId").CurrentValue = null;
                contexto.Entry(aux).Property("CategoriaPaiId").IsModified = true;

            }


            contexto.Entry(aux).State = EntityState.Modified;
            contexto.Categorias.Update(aux);

        }

        internal bool VerificarCategoria(Categoria categoria)
        {
            var res = contexto.Produtos.Where<Produto>(p => p.Categoria.Id == categoria.Id).FirstOrDefault();
            if (res == null) return true;
            return false;
        }

        private void EliminarReferenciasProduto(long? idCategoria)
        {
            var listaProdutos = contexto.Produtos.Include(p => p.Categoria).Where(p => p.Categoria.Id == idCategoria);
            foreach (Produto produtoAtual in listaProdutos)
            {
                produtoAtual.Categoria = null;
                contexto.Entry(produtoAtual).State = EntityState.Modified;
                contexto.Produtos.Update(produtoAtual);
            }

        }

        public void ApagarCategoria(Categoria categoria)
        {
            EliminarReferenciasProduto(categoria.Id);
            var temp = contexto.Categorias.Where<Categoria>(a => a.CategoriaPai.Id == categoria.Id);
            foreach (Categoria c in temp)
            {
                c.CategoriaPai = null;
            }
            contexto.Entry(categoria).State = EntityState.Deleted;
            contexto.Categorias.Remove(categoria);
        }

    }
}
