﻿using Microsoft.EntityFrameworkCore;
using SiC.Models;
using SiC.Persistence;
using SiC.Utils;
using SiC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Services
{
    public class RestricaoService
    {
        private readonly SicContext contexto;

        private RestricaoService()
        {

        }

        public RestricaoService(SicContext contexto)
        {
            this.contexto = contexto;
        }


        //GET de lista de Restricoes de uma agregacao
        public IEnumerable<Restricao> RestricaoAgregacao(long idPai, long idFilho)
        {
            var lista = contexto.Restricao.Include(r=> r.Pai).Include(r=> r.Filho).Where(r => r.Filho.ProdutoId == idFilho && r.Pai.ProdutoId == idPai);
            return lista;
        }

        public bool DuplicadoTamanho (Restricao restricao)
        {
            var listaRestricoes = contexto.Restricao.Where(r => r.Filho.ProdutoId == restricao.Filho.ProdutoId && r.Pai.ProdutoId == restricao.Pai.ProdutoId);
            foreach (var restricaoAtual in listaRestricoes)
            {
                if (restricaoAtual is RestricaoTamanho) return true;
            }
            return false;
        }

        public bool DuplicadoObrigatoriedade(Restricao restricao)
        {
            var listaRestricoes = contexto.Restricao.Where(r => r.Filho.ProdutoId == restricao.Filho.ProdutoId && r.Pai.ProdutoId == restricao.Pai.ProdutoId);
            foreach (var restricaoAtual in listaRestricoes)
            {
                if (restricaoAtual is RestricaoObrigatoriedade) return true;
            }
            return false;
        }

        public bool DuplicadoMaterial(Restricao restricao)
        {
            var restricaoCast = (RestricaoMaterial)restricao;
            var listaRestricoes = contexto.Restricao.Where(r => r.Filho.ProdutoId == restricao.Filho.ProdutoId && r.Pai.ProdutoId == restricao.Pai.ProdutoId);
            foreach (var restricaoAtual in listaRestricoes)
            {
                if (restricaoAtual is RestricaoMaterial)
                {
                    var rm = (RestricaoMaterial) restricaoAtual;
                    if (rm.IdMaterialFilho == restricaoCast.IdMaterialFilho && rm.IdMaterialPai == restricaoCast.IdMaterialPai) return true;
                }
            }
            return false;
        }

        public bool AtualizarRestricao(long idRestricao, Restricao restricao, long idPai, long idFilho)
        {
            var restricaoDB = contexto.Restricao.Where(r => r.Id == idRestricao).Include( r=> r.Pai).Include(r => r.Filho).FirstOrDefault();
            if(restricaoDB is RestricaoObrigatoriedade)
            {
                var rObrigatoriedade = (RestricaoObrigatoriedade)restricao;
                bool obrigatoriedade = rObrigatoriedade.Obrigatoriedade;
                var rGuardar = (RestricaoObrigatoriedade)restricaoDB;
                rGuardar.Obrigatoriedade = obrigatoriedade;
                restricaoDB = rGuardar;
            } else if (restricaoDB is RestricaoMaterial)
            {

                var rMaterial = (RestricaoMaterial)restricao;
                if (!VerificarRestricaoMaterialDuplicada(idRestricao, rMaterial, idPai, idFilho)) return false;
                long idMaterialPai = rMaterial.IdMaterialPai;
                long idMaterialFilho = rMaterial.IdMaterialFilho;
                var rGuardar = (RestricaoMaterial)restricaoDB;
                rGuardar.IdMaterialFilho = idMaterialFilho;
                rGuardar.IdMaterialPai = idMaterialPai;
                restricaoDB = rGuardar;
            } else if (restricaoDB is RestricaoTamanho)
            {
                var rTamanho = (RestricaoTamanho)restricao;
                var alturaMin = rTamanho.percentagemAlturaMinima;
                var alturaMax = rTamanho.percentagemAlturaMaxima;
                var larguraMin = rTamanho.percentagemLarguraMinima;
                var larguraMax = rTamanho.percentagemLarguraMaxima;
                var profundidadeMin = rTamanho.percentagemProfundidadeMinima;
                var profundidadeMax = rTamanho.percentagemProfundidadeMaxima;
                var rGuardar = (RestricaoTamanho)restricaoDB;
                rGuardar.percentagemAlturaMinima = alturaMin;
                rGuardar.percentagemAlturaMaxima = alturaMax;
                rGuardar.percentagemLarguraMinima = larguraMin;
                rGuardar.percentagemLarguraMaxima = larguraMax;
                rGuardar.percentagemProfundidadeMinima = profundidadeMin;
                rGuardar.percentagemProfundidadeMaxima = profundidadeMax;
                restricaoDB = rGuardar;
            }
            contexto.Entry(restricaoDB).State = EntityState.Modified;
            return true;
        }

        private bool VerificarRestricaoMaterialDuplicada(long idRestricao, RestricaoMaterial restricao, long idPai, long idFilho)
        {
            var restricoesPaiFilho = contexto.Restricao.Include( r => r.Filho).Include( r => r.Pai).Where(r => r.Filho.ProdutoId == idFilho && r.Pai.ProdutoId == idPai);
            foreach(var restricaoAtual in restricoesPaiFilho)
            {
                if(restricaoAtual is RestricaoMaterial)
                {
                    RestricaoMaterial restricaoCast = (RestricaoMaterial)restricaoAtual;
                    if (restricaoCast.IdMaterialFilho == restricao.IdMaterialFilho && restricaoCast.IdMaterialPai == restricao.IdMaterialPai) return false;
                }
            }
            return true;
        }

        public bool VerificaAgregacao(Restricao restricao)
        {
           ProdutoParte parte = contexto.ProdutoPartes.Where<ProdutoParte>(pp => pp.IdFilho == restricao.Filho.ProdutoId && pp.IdPai == restricao.Pai.ProdutoId).FirstOrDefault();
            if (parte != null) return true;
            return false;
        }
    }
}
