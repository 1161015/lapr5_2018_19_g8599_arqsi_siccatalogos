﻿using Microsoft.EntityFrameworkCore;
using SiC.Models;
using SiC.Utils;
using SiC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SiC.Services
{
    public class ProdutoService
    {
        private readonly SicContext contexto;
        private ProdutoService()
        {

        }

        public ProdutoService(SicContext contexto)
        {
            this.contexto = contexto;
        }

        public IEnumerable<Produto> ProdutoPorNome(string name)
        {
            var lista = contexto.Produtos.Include(p => p.Categoria).Include(p => p.Materiais).Include(p => p.ProdutosAgregados).Where(p => p.Nome.Conteudo.Equals(name, StringComparison.InvariantCultureIgnoreCase));
            return lista;
        }

        public IEnumerable<Produto> ProdutosObrigatorios(long idPai)
        {
            var restricoesProduto = contexto.Restricao.Where(r => r.Pai.ProdutoId == idPai).Include(r => r.Filho);
            var listaProdutosObrigatorios = new List<Produto>();
            foreach (var restricaoAtual in restricoesProduto)
            {
                if (restricaoAtual is RestricaoObrigatoriedade)
                {
                    RestricaoObrigatoriedade restricaoCast = (RestricaoObrigatoriedade)restricaoAtual;
                    if (restricaoCast.Obrigatoriedade)
                    {
                        PreencherProduto(restricaoCast.Filho);
                        listaProdutosObrigatorios.Add(restricaoCast.Filho);
                    }
                }
            }
            return listaProdutosObrigatorios;
        }


        public IEnumerable<Produto> ProdutosFilho(int id)
        {
            var listaProdutoPartes = contexto.ProdutoPartes.Where(pp => pp.IdPai == id);
            List<Produto> retorno = new List<Produto>();
            foreach (ProdutoParte pp in listaProdutoPartes)
            {
                Produto p1 = contexto.Produtos.Include(p => p.Categoria).Include(p => p.Materiais).Include(p => p.ProdutosAgregados).Where(p2 => p2.ProdutoId == pp.IdFilho).First();

                retorno.Add(p1);
            }
            return retorno;
        }



        public IEnumerable<Produto> ProdutosPai(int id)
        {
            var listaProdutoPartes = contexto.ProdutoPartes.Where(pp => pp.IdFilho == id);
            List<Produto> retorno = new List<Produto>();
            foreach (ProdutoParte pp in listaProdutoPartes)
            {
                Produto p1 = contexto.Produtos.Include(p => p.Categoria).Include(p => p.Materiais).Include(p => p.ProdutosAgregados).Where(p2 => p2.ProdutoId == pp.IdPai).First();
                retorno.Add(p1);
            }
            return retorno;
        }


        public List<Produto> ProdutosDaColecao(int idColecao)
        {
            List<Produto> produtosColecao = new List<Produto>();
            foreach (Produto p in contexto.Produtos.Include(p => p.Colecao))
            {
                if (p.Colecao != null)
                {
                    if (p.Colecao.Id == idColecao)
                    {
                        PreencherProduto(p);
                        produtosColecao.Add(p);

                    }
                }
            }
            return produtosColecao;
        }

        public List<Produto> ProdutosDoCatalogo(int idCatalogo)
        {
            List<Produto> produtosCatalogo = new List<Produto>();
            foreach (ProdutoCatalogo pc in contexto.ProdutoCatalogo.Include(pc => pc.Produto).Include(pc => pc.Catalogo))
            {
                if (pc.CatalogoId == idCatalogo)
                {
                    PreencherProduto(pc.Produto);
                    produtosCatalogo.Add(pc.Produto);
                }
            }
            return produtosCatalogo;
        }




        public void PreencherProduto(Produto p)
        {
            p = contexto.Produtos.Include(pr => pr.Categoria).Include(pr => pr.ProdutosAgregados).Include(pr => pr.Colecao).ThenInclude(c => c.Catalogo).Include(pr => pr.ProdutoCatalogos).
                Include(pr => pr.Dimensoes).ThenInclude(pr => pr.Altura).
                Include(pr => pr.Dimensoes).ThenInclude(pr => pr.Largura).
                Include(pr => pr.Dimensoes).ThenInclude(pr => pr.Profundidade).
                Include(pr => pr.Materiais).Where(pr => pr.ProdutoId == p.ProdutoId).First();

        }

        public bool AtualizarProduto(Produto p)
        {
            if (p.Dimensoes == null) return false;
            Categoria cat = contexto.Categorias.Where<Categoria>(c => c.Id == p.Categoria.Id).FirstOrDefault();
            p.Categoria = contexto.Categorias.Where<Categoria>(c => c.Id == p.Categoria.Id).FirstOrDefault();

            if (p.Colecao != null)
            {
                p.Colecao = contexto.Colecoes.Include(c => c.Catalogo).Where<Colecao>(c => c.Id == p.Colecao.Id).FirstOrDefault();

            }
            else
            {
                contexto.Entry(p).Property("ColecaoId").CurrentValue = null;
            }
            if (p.ProdutosAgregados == null)
            {
                p.ProdutosAgregados = new List<ProdutoParte>();
            }
            //if (p.produtocatalogos != null)
            //{
            //    foreach (var pp in p.produtocatalogos)
            //    {
            //        var produtocatalogodb = contexto.produtocatalogo.where(pc => pc.catalogoid == pp.catalogoid && pc.produtoid==p.produtoid).firstordefault();
            //        p.produtocatalogos.add(produtocatalogodb);

            //    }
            //}
            return true;
        }

        public void AtualizarProduto(Produto produtoBD, Produto produtoPM)
        {

            if (produtoPM.Nome != null)
            {
                produtoBD.Nome = produtoPM.Nome;
            }
            if (produtoPM.Categoria != null)
            {
                produtoBD.Categoria = contexto.Categorias.Where<Categoria>(c => c.Id == produtoPM.Categoria.Id).FirstOrDefault();
            }
            if (produtoPM.ProdutoCatalogos != null)
            {
                foreach (ProdutoCatalogo pc in produtoBD.ProdutoCatalogos)
                {
                    contexto.ProdutoCatalogo.Remove(pc);
                }
                produtoBD.ProdutoCatalogos = produtoPM.ProdutoCatalogos;
            }
            if (produtoPM.Colecao != null)
            {
                produtoBD.Colecao = contexto.Colecoes.Where<Colecao>(c => c.Id == produtoPM.Colecao.Id).FirstOrDefault();
            }

            else
            {
                contexto.Entry(produtoBD).Property("ColecaoId").CurrentValue = null;
                contexto.Entry(produtoBD).Property("ColecaoId").IsModified = true;

            }

            // Dimensoes
            produtoBD.Dimensoes = produtoPM.Dimensoes;
            foreach (ProdutoMaterial pc in produtoBD.Materiais)
            {
                contexto.ProdutoMaterial.Remove(pc);
            }
            List<ProdutoMaterial> listaPM = new List<ProdutoMaterial>();
            if (produtoPM.Materiais != null)
            {
                foreach (ProdutoMaterial pm in produtoPM.Materiais)
                {
                    pm.IdProduto = produtoBD.ProdutoId;
                    listaPM.Add(pm);
                }
            }


            produtoBD.Materiais = listaPM;
            List<ProdutoParte> listaPP = new List<ProdutoParte>();
            foreach (ProdutoParte pc in produtoBD.ProdutosAgregados)
            {
                contexto.ProdutoPartes.Remove(pc);
            }
            if (produtoPM.ProdutosAgregados != null)
            {
                foreach (ProdutoParte pp in produtoPM.ProdutosAgregados)
                {
                    pp.IdPai = produtoBD.ProdutoId;
                    listaPP.Add(pp);
                }
            }
            produtoBD.ProdutosAgregados = listaPP;

        }

        public bool verificarDimensoes(List<Triplo<double, double, double>> podeEditar, Produto produto)
        {
            List<double> alturaItem = new List<double>();
            List<double> larguraItem = new List<double>();
            List<double> profundidadeItem = new List<double>();

            foreach (Triplo<double, double, double> t in podeEditar)
            {
                alturaItem.Add(t.Altura);
                larguraItem.Add(t.Largura);
                profundidadeItem.Add(t.Profundidade);
            }
            bool altura = verificarAltura(alturaItem, produto.Dimensoes.Altura);
            bool largura = verificarLargura(larguraItem, produto.Dimensoes.Largura);
            bool profundidade = verificarProfundidade(profundidadeItem, produto.Dimensoes.Profundidade);

            if (altura && largura && profundidade) return true;
            return false;
        }

        private bool verificarProfundidade(List<double> profundidadeItem, ICollection<Dimensao> profundidade)
        {
            bool state = false;
            if (profundidade.ElementAt(0) is DimensaoDiscreta)
            {

                foreach (double alt in profundidadeItem)
                {
                    state = false;
                    foreach (DimensaoDiscreta dim in profundidade)
                    {
                        if (dim.dimensao == alt)
                        {
                            state = true;
                            break;
                        }
                        if (!state)
                        {
                            return state;
                        }
                    }
                }

            }
            else
            {
                foreach (double alt in profundidadeItem)
                {
                    state = false;
                    foreach (DimensaoContinua dim in profundidade)
                    {
                        if (dim.dimensaoMin <= alt && dim.dimensaoMax >= alt)
                        {
                            state = true;
                            break;
                        }
                        if (!state)
                        {
                            return state;
                        }
                    }
                }
            }
            return true;
        }
    

        private bool verificarLargura(List<double> larguraItem, ICollection<Dimensao> largura)
        {
            bool state = false;
            if (largura.ElementAt(0) is DimensaoDiscreta)
            {

                foreach (double alt in larguraItem)
                {
                    state = false;
                    foreach (DimensaoDiscreta dim in largura)
                    {
                        if (dim.dimensao == alt)
                        {
                            state = true;
                            break;
                        }
                        if (!state)
                        {
                            return state;
                        }
                    }
                }

            }
            else
            {
                foreach (double alt in larguraItem)
                {
                    state = false;
                    foreach (DimensaoContinua dim in largura)
                    {
                        if (dim.dimensaoMin <= alt && dim.dimensaoMax >= alt)
                        {
                            state = true;
                            break;
                        }
                        if (!state)
                        {
                            return state;
                        }
                    }
                }
            }
            return true;
        }

        private bool verificarAltura(List<double> alturaItem, ICollection<Dimensao> altura)
        {
            bool state = false;
            if (altura.ElementAt(0) is DimensaoDiscreta)
            {
                
                foreach (double alt in alturaItem)
                {
                    state = false;
                    foreach (DimensaoDiscreta dim in altura)
                    {
                        if (dim.dimensao == alt)
                        {
                            state = true;
                            break;
                        }
                        if (!state)
                        {
                            return state;
                        }
                    }
                }

            }
            else
            {
                foreach(double alt in alturaItem)
                {
                    state = false;
                    foreach (DimensaoContinua dim in altura)
                    {
                        if (dim.dimensaoMin <= alt && dim.dimensaoMax >= alt)
                        {
                            state = true;
                            break;
                        }
                        if (!state)
                        {
                            return state;
                        }
                    }
                }
            }
            return true;
        }

        public bool VerificaComoFilho(Produto produto)
        {
            List<ProdutoParte> listaPais = contexto.ProdutoPartes.Include(p => p.ProdutoPai).ThenInclude(pp => pp.Dimensoes).ThenInclude(d => d.Altura)
                .Include(p => p.ProdutoPai).ThenInclude(pp => pp.Dimensoes).ThenInclude(d => d.Largura)
                .Include(p => p.ProdutoPai).ThenInclude(pp => pp.Dimensoes).ThenInclude(d => d.Profundidade)
                .Where(pp => pp.IdFilho == produto.ProdutoId).ToList();

            double alturaMinFilho = produto.BuscarLimiteDimensao(produto.Dimensoes,0,0);
            double larguraMinFilho = produto.BuscarLimiteDimensao(produto.Dimensoes, 1,0);
            double profundidadeMinFilho = produto.BuscarLimiteDimensao(produto.Dimensoes, 2, 0);
            foreach (ProdutoParte pp in listaPais)
            {
                
                double alturaMaxPai = pp.ProdutoPai.BuscarLimiteDimensao(pp.ProdutoPai.Dimensoes, 0, 1);
                double larguraMaxPai = pp.ProdutoPai.BuscarLimiteDimensao(pp.ProdutoPai.Dimensoes, 1, 1);
                double profundidadeMaxPai = pp.ProdutoPai.BuscarLimiteDimensao(pp.ProdutoPai.Dimensoes, 2, 1);

                if (alturaMinFilho >= alturaMaxPai || larguraMinFilho >= larguraMaxPai || profundidadeMinFilho >= profundidadeMaxPai) return false;

            }


            return true;
        }

        public List<Produto> ProdutosPreenchidos()
        {
            List<Produto> resultado = new List<Produto>();
            foreach (Produto p in contexto.Produtos)
            {
                PreencherProduto(p);
                resultado.Add(p);
            }
            return resultado;
        }

        /*
         * Verifica se o volume total dos filhos de um produto p respeita as restrições volumicas impostas
         */

        // ATUALIZAR METODO PARA NOVA ESTRUTURA RESTRICAO
        //public bool verificaTaxaOcupacao(Produto p, double pOcupacao)
        //{
        //    if (p == null) return false; // se o produtoPai recebido não for válido

        //    if (p.Restricao.PercentagemMinima == null) return false;
        //    if (p.Restricao.PercentagemMaxima == null) return false;

        //    return pOcupacao >= p.Restricao.PercentagemMinima && pOcupacao <= p.Restricao.PercentagemMaxima;
        //}

        public Produto ProdutoPorId(long id)
        {
            Produto p = contexto.Produtos.Where<Produto>(pr => pr.ProdutoId == id).First();
            PreencherProduto(p);
            return p;
        }

        public bool VerificarMaterialAcabamentoItem(Produto p, int idMaterial, int idAcabamento)
        {
            List<long> materialProduto = new List<long>();
            List<Tuple<long, long>> materialAcabamentoProduto = new List<Tuple<long, long>>();
            foreach (ProdutoMaterial pm in p.Materiais)
            {
                materialProduto.Add(pm.IdMaterial);
            }

            foreach (long m in materialProduto)
            {
                List<MaterialAcabamento> lista = contexto.MaterialAcabamento.Where<MaterialAcabamento>(ma => ma.IdMaterial == m).ToList();
                foreach (MaterialAcabamento ma in lista)
                {
                    Tuple<long, long> idMaterialAcabamento = new Tuple<long, long>(ma.IdMaterial, ma.IdAcabamento);
                    materialAcabamentoProduto.Add(idMaterialAcabamento);
                }
            }

            foreach (Tuple<long, long> id in materialAcabamentoProduto)
            {
                if (id.Item1 == idMaterial && id.Item2 == idAcabamento)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        ///
        /// Verifica se o item pai tem todos os itens obrigatórios nele inseridos.
        /// </summary>
        /// <param name="itemDTO">item pai e respetivos filhos que se pretendem inserir.</param>
        /// <returns>true se todos os itens obrigatórios estiverem nele inseridos</returns>
        public bool VerificarObrigatoriedade(ItemDTO itemDTO)
        {
            Produto produtoPai = contexto.Produtos.Where(p => p.ProdutoId == itemDTO.ProdutoPaiId).Include(p => p.ProdutosAgregados).FirstOrDefault();
            if (produtoPai == null) return false;
            var restricoesPai = contexto.Restricao.Where(r => r.Pai.ProdutoId == itemDTO.ProdutoPaiId).Include(r => r.Filho);
            foreach (var restricaoAtual in restricoesPai)
            {
                if (restricaoAtual is RestricaoObrigatoriedade)
                {
                    var restricaoCast = (RestricaoObrigatoriedade)restricaoAtual;
                    if (restricaoCast.Obrigatoriedade)
                    {
                        if (!itemDTO.ProdutosFilhosIds.Contains(restricaoCast.Filho.ProdutoId)) return false;
                    }
                }
            }
            return true;
        }

        public bool VerificarMaterialAcabamentoObrigatoriedade(ItemDTO itemDTO)
        {
            Produto produtoPai = contexto.Produtos.Where(p => p.ProdutoId == itemDTO.ProdutoPaiId).Include(p => p.ProdutosAgregados).FirstOrDefault();
            if (produtoPai == null) return false;
            int i = 0;
            foreach (long produtoFilhoid in itemDTO.ProdutosFilhosIds)
            {
                bool flag = true;
                var restricoes = contexto.Restricao.Where(p => p.Pai.ProdutoId == itemDTO.ProdutoPaiId && p.Filho.ProdutoId == produtoFilhoid);
                foreach (var restricaoAtual in restricoes)
                {
                    if (restricaoAtual is RestricaoMaterial)
                    {
                        RestricaoMaterial restricaoCast = (RestricaoMaterial)restricaoAtual;
                        if (restricaoCast.IdMaterialPai == itemDTO.MaterialAcabamentoPai.Chave) // se houver uma restrição cujo material do pai é o material preetendido para o pai
                        {
                            if (restricaoCast.IdMaterialFilho == itemDTO.MaterialIdFilhos[i])
                            {
                                flag = true;
                                break;
                            }
                            else
                            {
                                flag = false;
                            }
                        }

                    }
                }
                if (!flag) return false;
                i++;
            }
            return true;
        }

        public bool VerificarTaxaPercentual(ItemDTO itemDTO)
        {
            Produto produtoPai = contexto.Produtos.Where(p => p.ProdutoId == itemDTO.ProdutoPaiId).Include(p => p.ProdutosAgregados).FirstOrDefault();
            if (produtoPai == null) return false;
            int i = 0;
            foreach (long produtoFilhoid in itemDTO.ProdutosFilhosIds)
            {
                var restricoes = contexto.Restricao.Where(p => p.Pai.ProdutoId == itemDTO.ProdutoPaiId && p.Filho.ProdutoId == produtoFilhoid);
                foreach (var restricaoAtual in restricoes)
                {
                    if (restricaoAtual is RestricaoTamanho)
                    {
                        RestricaoTamanho restricaoCast = (RestricaoTamanho)restricaoAtual;
                        if (restricaoCast.percentagemAlturaMinima != -1) // se houver de altura
                        {
                            if (itemDTO.PercentagensFilhos[i].PercentagemAltura < restricaoCast.percentagemAlturaMinima || itemDTO.PercentagensFilhos[i].PercentagemAltura > restricaoCast.percentagemAlturaMaxima) return false;
                        }
                        if (restricaoCast.percentagemLarguraMinima != -1) // se houver de largura
                        {
                            if (itemDTO.PercentagensFilhos[i].PercentagemLargura < restricaoCast.percentagemLarguraMinima || itemDTO.PercentagensFilhos[i].PercentagemLargura > restricaoCast.percentagemLarguraMaxima) return false;
                        }
                        if (restricaoCast.percentagemProfundidadeMinima != -1)// se houver de profundidade
                        {
                            if (itemDTO.PercentagensFilhos[i].PercentagemProfundidade < restricaoCast.percentagemProfundidadeMinima || itemDTO.PercentagensFilhos[i].PercentagemProfundidade > restricaoCast.percentagemProfundidadeMaxima) return false;
                        }
                    }
                }
                i++;
            }
            return true;

        }

        public bool VerificarProdutosFilhosExistem(ItemDTO itemDTO)
        {
            Produto produtoPai = contexto.Produtos.Where(p => p.ProdutoId == itemDTO.ProdutoPaiId).Include(p => p.ProdutosAgregados).FirstOrDefault();
            if (produtoPai == null) return false;
            var produtosFilhos = produtoPai.IdsProdutosFilhos();
            if (produtosFilhos.Count == 0 && itemDTO.ProdutosFilhosIds.Count > 0) return false; // não podem existir filhos
            foreach (long idProdutoFilhos in itemDTO.ProdutosFilhosIds)
            {
                if (!produtosFilhos.Contains(idProdutoFilhos)) return false; // basta não ter um produto filho
            }
            return true; // todos os filhos estão contidos no pai
        }

        public bool VerificarDimensoesItem(Produto p, double altura, double largura, double profundidade)
        {
            return p.Dimensoes.Altura.Any(d => d.ValorValido(altura)) && p.Dimensoes.Largura.Any(d => d.ValorValido(largura)) &&
                p.Dimensoes.Profundidade.Any(d => d.ValorValido(profundidade));
        }

        public void RemoverReferencias(Produto p)
        {
            var produtosPartes = contexto.ProdutoPartes.Where(p1 => p1.IdFilho == p.ProdutoId || p1.IdPai == p.ProdutoId);
            foreach (var pp in produtosPartes)
            {
                contexto.ProdutoPartes.Remove(pp);
            }
            foreach (var altura in p.Dimensoes.Altura)
            {
                contexto.Dimensao.Remove(altura);
            }
            foreach (var largura in p.Dimensoes.Largura)
            {
                contexto.Dimensao.Remove(largura);
            }
            foreach (var profundidade in p.Dimensoes.Profundidade)
            {
                contexto.Dimensao.Remove(profundidade);
            }
            contexto.Dimensoes.Remove(p.Dimensoes);
            List<Restricao> restricoes = contexto.Restricao.Where<Restricao>(r => r.Filho.ProdutoId == p.ProdutoId || r.Pai.ProdutoId == p.ProdutoId).ToList();
            foreach (Restricao r in restricoes)
            {
                contexto.Restricao.Remove(r);
            }
        }

        public double alturaMaximaProduto(Produto p)
        {
            double alturaMax = 0;
            if (p.Dimensoes.Altura.ElementAt(0) is DimensaoContinua)
            {
                foreach (DimensaoContinua d in p.Dimensoes.Altura)
                {
                    if (d.dimensaoMax > alturaMax)
                    {
                        alturaMax = d.dimensaoMax;
                    }
                }
            }
            else
            {
                foreach (DimensaoDiscreta d in p.Dimensoes.Altura)
                {
                    if (d.dimensao > alturaMax)
                    {
                        alturaMax = d.dimensao;
                    }
                }
            }
            return alturaMax;
        }

        public double alturaMinimaProduto(Produto p)
        {
            double alturaMin = 999;
            if (p.Dimensoes.Altura.ElementAt(0) is DimensaoContinua)
            {
                foreach (DimensaoContinua d in p.Dimensoes.Altura)
                {
                    if (d.dimensaoMax < alturaMin)
                    {
                        alturaMin = d.dimensaoMax;
                    }
                }
            }
            else
            {
                foreach (DimensaoDiscreta d in p.Dimensoes.Altura)
                {
                    if (d.dimensao < alturaMin)
                    {
                        alturaMin = d.dimensao;
                    }
                }
            }
            return alturaMin;
        }

        public double larguraMinimaProduto(Produto p)
        {
            double larguraMin = 999;
            if (p.Dimensoes.Largura.ElementAt(0) is DimensaoContinua)
            {
                foreach (DimensaoContinua d in p.Dimensoes.Largura)
                {
                    if (d.dimensaoMax < larguraMin)
                    {
                        larguraMin = d.dimensaoMax;
                    }
                }
            }
            else
            {
                foreach (DimensaoDiscreta d in p.Dimensoes.Largura)
                {
                    if (d.dimensao < larguraMin)
                    {
                        larguraMin = d.dimensao;
                    }
                }
            }
            return larguraMin;
        }

        public double profundidadeMinimaProduto(Produto p)
        {
            double profundidadeMin = 999;
            if (p.Dimensoes.Profundidade.ElementAt(0) is DimensaoContinua)
            {
                foreach (DimensaoContinua d in p.Dimensoes.Profundidade)
                {
                    if (d.dimensaoMax < profundidadeMin)
                    {
                        profundidadeMin = d.dimensaoMax;
                    }
                }
            }
            else
            {
                foreach (DimensaoDiscreta d in p.Dimensoes.Profundidade)
                {
                    if (d.dimensao < profundidadeMin)
                    {
                        profundidadeMin = d.dimensao;
                    }
                }
            }
            return profundidadeMin;
        }

        public double larguraMaximaProduto(Produto p)
        {
            double larguraMax = 0;
            if (p.Dimensoes.Largura.ElementAt(0) is DimensaoContinua)
            {
                foreach (DimensaoContinua d in p.Dimensoes.Largura)
                {
                    if (d.dimensaoMax > larguraMax)
                    {
                        larguraMax = d.dimensaoMax;
                    }
                }
            }
            else
            {
                foreach (DimensaoDiscreta d in p.Dimensoes.Largura)
                {
                    if (d.dimensao > larguraMax)
                    {
                        larguraMax = d.dimensao;
                    }
                }
            }
            return larguraMax;
        }

        public double profundidadeMaximaProduto(Produto p)
        {
            double profundidadeMax = 0;
            if (p.Dimensoes.Profundidade.ElementAt(0) is DimensaoContinua)
            {
                foreach (DimensaoContinua d in p.Dimensoes.Profundidade)
                {
                    if (d.dimensaoMax > profundidadeMax)
                    {
                        profundidadeMax = d.dimensaoMax;
                    }
                }
            }
            else
            {
                foreach (DimensaoDiscreta d in p.Dimensoes.Profundidade)
                {
                    if (d.dimensao > profundidadeMax)
                    {
                        profundidadeMax = d.dimensao;
                    }
                }
            }
            return profundidadeMax;
        }
        public bool VerificarTamanho(Produto p)
        {
            double alturaMinima = p.BuscarLimiteDimensao(p.Dimensoes, 0, 0);
            double alturaMaxima = p.BuscarLimiteDimensao(p.Dimensoes, 0, 1);
            double larguraMinima = p.BuscarLimiteDimensao(p.Dimensoes, 1, 0);
            double larguraMaxima = p.BuscarLimiteDimensao(p.Dimensoes, 1, 1);
            double profundidadeMinima = p.BuscarLimiteDimensao(p.Dimensoes, 2, 0);
            double profundidadeMaxima = p.BuscarLimiteDimensao(p.Dimensoes, 2, 1);
            CategoriaService cs = new CategoriaService(contexto);

            bool armario = cs.PodeTerFilhos((long)p.Categoria.Id);
            if (!Dimensao.ValidarAltura(alturaMinima, armario) || !Dimensao.ValidarAltura(alturaMaxima, armario)
                || !Dimensao.ValidarLargura(larguraMinima, armario) || !Dimensao.ValidarLargura(larguraMaxima, armario)
                || !Dimensao.ValidarProfundidade(profundidadeMinima, armario) || !Dimensao.ValidarProfundidade(profundidadeMaxima, armario)) return false;
            if (p.ProdutosAgregados != null)
            {
                if (p.ProdutosAgregados.Count > 0)
                {
                    double alturaMax = alturaMaximaProduto(p);
                    double larguraMax = larguraMaximaProduto(p);
                    double profundidadeMax = profundidadeMaximaProduto(p);

                    foreach (ProdutoParte pa in p.ProdutosAgregados)
                    {
                        if (pa.IdPai == p.ProdutoId)
                        {
                            Produto filho = contexto.Produtos.Where<Produto>(pf => pf.ProdutoId == pa.IdFilho).FirstOrDefault();
                            PreencherProduto(filho);
                            //altura
                            if (filho.Dimensoes.Altura.ElementAt(0) is DimensaoContinua)
                            {
                                foreach (DimensaoContinua d in filho.Dimensoes.Altura)
                                {
                                    if (d.dimensaoMin > alturaMax)
                                    {
                                        return false;
                                    }
                                }
                            }
                            else
                            {
                                foreach (DimensaoDiscreta d in filho.Dimensoes.Altura)
                                {
                                    if (d.dimensao > alturaMax)
                                    {
                                        return false;
                                    }
                                }
                            }
                            //largura
                            if (filho.Dimensoes.Largura.ElementAt(0) is DimensaoContinua)
                            {
                                foreach (DimensaoContinua d in filho.Dimensoes.Largura)
                                {
                                    if (d.dimensaoMin > larguraMax)
                                    {
                                        return false;
                                    }
                                }
                            }
                            else
                            {
                                foreach (DimensaoDiscreta d in filho.Dimensoes.Largura)
                                {
                                    if (d.dimensao > larguraMax)
                                    {
                                        return false;
                                    }
                                }
                            }
                            //profundidade
                            if (filho.Dimensoes.Profundidade.ElementAt(0) is DimensaoContinua)
                            {
                                foreach (DimensaoContinua d in filho.Dimensoes.Profundidade)
                                {
                                    if (d.dimensaoMin > profundidadeMax)
                                    {
                                        return false;
                                    }
                                }
                            }
                            else
                            {
                                foreach (DimensaoDiscreta d in filho.Dimensoes.Profundidade)
                                {
                                    if (d.dimensao > profundidadeMax)
                                    {
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return true;
        }
        // verifica se dois items têm pelo menos um material em comum
        private bool TemMaterialEmComum(Produto filho, Produto pai)
        {
            bool contem = false;
            foreach (ProdutoMaterial pm in pai.Materiais)
            {
                if (filho.Materiais.Contains(pm)) contem = true;
            }
            return contem;
        }

        public bool AdicionarFilhos(Produto pai)
        {
            List<Tuplo<Produto, RestricaoTamanho>> listaProdutos = new List<Tuplo<Produto, RestricaoTamanho>>();
            RestricaoTamanho rt;
            Tuplo<Produto, RestricaoTamanho> tuplo = new Tuplo<Produto, RestricaoTamanho>();
            foreach (Produto filho in ProdutosFilhoObrigatorios(pai))
            {
                PreencherProduto(filho);
                rt = RestricoesTamanhoProdutoPai(pai, filho);
                if (rt == null) continue;
                tuplo.Chave = filho;
                tuplo.Valor = rt;
                listaProdutos.Add(tuplo);
            }
            return pai.AdicionarProdutos(listaProdutos);
        }

        public bool CumpreTaxaOcupacao(Produto pai)
        {
            if (pai.ProdutosAgregados == null) return true;
            ProdutoService produtoService = new ProdutoService(contexto);
            foreach (ProdutoParte pp in pai.ProdutosAgregados) // para cada filho, vai buscar a restrição tamanho respetiva
            {
                Produto pPai = produtoService.ProdutoPorId(pp.IdPai);
                Produto pFilho = produtoService.ProdutoPorId(pp.IdFilho);

                if (!pai.CumpreTaxaOcupacao(RestricoesTamanhoProdutoPai(pPai, pFilho), pFilho)) return false;
            }
            return true;
        }

        private List<Produto> ProdutosFilhoObrigatorios(Produto pai)
        {
            List<Produto> produtosFilhoObrigatorios = new List<Produto>();
            RestricaoService servicoRestricao = new RestricaoService(contexto);
            RestricaoObrigatoriedade ro;
            List<Restricao> rests = new List<Restricao>();
            if (pai.ProdutosAgregados == null) return produtosFilhoObrigatorios;
            foreach (ProdutoParte pp in pai.ProdutosAgregados) // vai buscar todos os filhos do produto recebido que são obrigatórios
            {
                rests = servicoRestricao.RestricaoAgregacao(pp.IdPai, pp.IdFilho).ToList();
                foreach (Restricao rest in rests)
                {
                    if (rest is RestricaoObrigatoriedade)
                    {
                        ro = (RestricaoObrigatoriedade)rest;
                        if (ro.Obrigatoriedade)
                        {
                            produtosFilhoObrigatorios.Add(ro.Filho);
                        }
                    }
                }
            }
            return produtosFilhoObrigatorios;
        }

        private RestricaoTamanho RestricoesTamanhoProdutoPai(Produto pai, Produto filho)
        {
            RestricaoService servicoRestricao = new RestricaoService(contexto);
            List<Restricao> rests = new List<Restricao>();

            rests = servicoRestricao.RestricaoAgregacao(pai.ProdutoId, filho.ProdutoId).ToList();
            foreach (Restricao rest in rests)
            {
                if (rest is RestricaoTamanho)
                {
                    return (RestricaoTamanho)rest;
                }
            }

            return null;
        }
    }
}
