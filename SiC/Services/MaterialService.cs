﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using SiC.Models;
using SiC.Persistence;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Services
{
    public class MaterialService
    {
        private readonly SicContext contexto;

        public MaterialService(SicContext contexto)
        {
            this.contexto = contexto;
        }

        private MaterialService()
        {

        }

        public Material ByName(string name)
        {
            /* com erro, falta implementar o get da fabrica de repositorios */

            foreach (Material a in contexto.Materiais)
            {
                if (a.Nome.Equals(name))
                {
                    return a;
                }
            }
            {

            }
            return null;
        }


        public List<Material> MateriaisPreenchidos()
        {
            List<Material> resultado = new List<Material>();
            foreach (Material m in contexto.Materiais)
            {
                PreencherMaterial(m);
                resultado.Add(m);
            }
            return resultado;
        }

        public void PreencherMaterial(Material m)
        {
            foreach (MaterialAcabamento ma in contexto.MaterialAcabamento)
            {
                if (m.Id == ma.IdMaterial)
                {
                    m.Acabamentos.Add(ma);
                }
            }
            foreach (ProdutoMaterial pm in contexto.ProdutoMaterial)
            {
                if (m.Id == pm.IdMaterial)
                {
                    m.Produtos.Add(pm);
                }
            }
        }

        public IEnumerable<Material> MateriaisProduto(int idProduto)
        {
            var retorno = new List<Material>();
            var listaProdutoMaterial = contexto.ProdutoMaterial.Where(pm => pm.IdProduto == idProduto).Include(pm => pm.Material);
            foreach (var materialAtual in listaProdutoMaterial)
            {
                retorno.Add(materialAtual.Material);
            }
            return retorno;
        }


        public void ApagarMaterial (Material material)
        {
            var temp = contexto.MaterialAcabamento.Where<MaterialAcabamento>(a => a.IdMaterial == material.Id);
            foreach (MaterialAcabamento ma in temp)
            {
                contexto.MaterialAcabamento.Remove(ma);
            }
            var temp2 = contexto.ProdutoMaterial.Where<ProdutoMaterial>(a => a.IdMaterial == material.Id);
            foreach (ProdutoMaterial pm in temp2)
            {
                contexto.ProdutoMaterial.Remove(pm);
            }
            List<HistoricoPreco> precos = contexto.HistoricoPrecos.Where(hp => hp.Id_Material == material.Id).ToList();
            foreach(HistoricoPreco hp in precos)
            {
                contexto.HistoricoPrecos.Remove(hp);
            }
            foreach(var r in contexto.Restricao)
            {
                if(r is RestricaoMaterial)
                {
                    RestricaoMaterial rm = (RestricaoMaterial)r;
                    if(rm.IdMaterialFilho == material.Id || rm.IdMaterialPai == material.Id)
                    {
                        contexto.Restricao.Remove(r);
                    }
                }
            }
            contexto.Entry(material).State = EntityState.Deleted;
            contexto.Materiais.Remove(material);
        }
        public void AtualizarMaterial(long id, Material material)
        {

            var temp = contexto.Materiais.Where<Material>(a => a.Id == id);
            Material aux = temp.First<Material>();
            aux.Nome = material.Nome;
            aux.TexturaImagem = material.TexturaImagem;
            foreach(MaterialAcabamento ma in contexto.MaterialAcabamento.Where<MaterialAcabamento>(ma => ma.IdMaterial == id)){
                contexto.MaterialAcabamento.Remove(ma);
            }
            if (material.Acabamentos != null) { 
                foreach(MaterialAcabamento ma in material.Acabamentos)
                {
                    ma.IdMaterial = id;
                    contexto.MaterialAcabamento.Add(ma);
                }
            }
            aux.Acabamentos = material.Acabamentos;
            contexto.Entry(aux).State = EntityState.Modified;
            contexto.Materiais.Update(aux);

        }

        public bool VerificarMaterial(Material material)
        {
            IEnumerable<ProdutoMaterial> produtos = contexto.ProdutoMaterial.Include(pm => pm.Produto).ThenInclude(p => p.Materiais).Where(m => m.IdMaterial == material.Id);
            if (produtos == null) return true;
            foreach (ProdutoMaterial pm in produtos)
            {
                if (pm.Produto.Materiais.Count == 1) return false;
            }
            return true;
        }
    }
}

