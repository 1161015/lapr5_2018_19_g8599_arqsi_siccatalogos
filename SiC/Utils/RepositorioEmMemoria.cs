﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Utils
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T">A entidade</typeparam>
    /// <typeparam name="K">A chave primária da entidade</typeparam>
    public abstract class RepositorioEmMemoria<T, K> : IRepositorioDados<T, K>
    {
        private static readonly Dictionary<K, T> Dados = new Dictionary<K, T>();

        public bool Delete(T entity)
        {
            foreach (var entidade in Dados)
            {
                if (entidade.Value.Equals(entity))
                {
                    Dados.Remove(entidade.Key);
                    return true;
                }
            }
            return false;
        }

        public bool Delete(K primaryKey)
        {
            return Dados.Remove(primaryKey);
        }

        public IEnumerable<T> FindAll()
        {
            return Dados.Values;
        }

        public T FindOne(K id)
        {
            return Dados[id];
        }
        /// <summary>
        /// Caso a entidade não existe na base de dados ela é adicionada, caso já exista simplemente
        /// é atualiazada.
        /// </summary>
        /// <param name="entity">A entidade a ser adicionada.</param>
        /// <returns>A mesma entidade.</returns>
        public T Save(T entity)
        {
            Dados[GenerateKey(entity)] = entity;
            return entity;
        }

        public abstract K GenerateKey(T entity);
    }
}
