﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Utils
{
    public class Triplo<A, L, P>
    {
        public A Altura { get; set; }
        public L Largura { get; set; }
        public P Profundidade { get; set; }

    }
}
