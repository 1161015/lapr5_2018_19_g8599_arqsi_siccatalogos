﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace SiC.Utils
{
    public interface IRepositorioDados<T, K>
    {
        IEnumerable<T> FindAll();
        T FindOne(K id);
        T Save(T entity);
        bool Delete(T entity);
        bool Delete(K primaryKey);

    }
}
