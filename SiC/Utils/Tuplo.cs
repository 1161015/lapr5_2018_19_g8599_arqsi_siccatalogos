﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Utils
{
    public class Tuplo<K,V>
    {
        public K Chave { get; set; }
        public V Valor { get; set; }
    }
}
