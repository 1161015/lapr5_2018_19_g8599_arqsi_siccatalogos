﻿using Microsoft.EntityFrameworkCore;
using SiC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace SiC.Utils
{
    public abstract class RepositorioDados<T, K> : IRepositorioDados<T,K>
    {
        public SicContext Contexto { get; set; }

        public RepositorioDados()
        {

        }
        public RepositorioDados(SicContext contexto)
        {
            this.Contexto = contexto;
        }

        public abstract bool Delete(T entity);
        public abstract bool Delete(K primaryKey);
        public abstract IEnumerable<T> FindAll();
        public abstract T FindOne(K id);
        public abstract T Save(T entity);
    }
}
