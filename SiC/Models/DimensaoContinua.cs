﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Models
{
    public class DimensaoContinua : Dimensao
    {
        public double dimensaoMin { get; set; }
        public double dimensaoMax { get; set; }

        public override bool ValorValido(double valorInserido)
        {
            return valorInserido >= dimensaoMin && valorInserido <= dimensaoMax;
        }
    }
}
