using Microsoft.EntityFrameworkCore;
using SiC.ViewModels;
using System.Collections.Generic;

namespace SiC.Models
{
    
    public class Dimensoes
    {
        public long Id { get; set; }
        public ICollection<Dimensao> Altura { get; set; }
        public ICollection<Dimensao> Largura { get; set; }
        public ICollection<Dimensao> Profundidade { get; set; }
        
        public Dimensoes()
        {

        }


        public override bool Equals(object obj)
        {
            var item = obj as Dimensoes;

            if (item == null)
            {
                return false;
            }

            if (!this.Altura.Equals(item.Altura))
            {
                return false;
            }
            if (!this.Largura.Equals(item.Largura))
            {
                return false;
            }
            if (!this.Profundidade.Equals(item.Profundidade))
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            
            return Altura.GetHashCode() + Largura.GetHashCode() + Profundidade.GetHashCode();
        }

    }
}