﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Models
{
    public class ProdutoCatalogo
    {
        public long ProdutoId { get; set; }
        [JsonIgnore]
        public Produto Produto { get; set; }
        public long CatalogoId { get; set; }
        [JsonIgnore]
        public Catalogo Catalogo { get; set; }

        public ProdutoCatalogo (){

        }

    }
}
