using Microsoft.EntityFrameworkCore;

namespace SiC.Models
{
    public class SicContext : DbContext
    {
        public SicContext(DbContextOptions<SicContext> options) : base(options)
        {
        }


        protected override void OnModelCreating(ModelBuilder builder)
        {

            builder.Entity<ProdutoMaterial>().HasKey(pm => new { pm.IdProduto, pm.IdMaterial });
            builder.Entity<ProdutoMaterial>().HasOne(pm => pm.Produto)
                .WithMany(p => p.Materiais)
                .HasForeignKey(pm => pm.IdProduto);
            builder.Entity<ProdutoMaterial>().HasOne(pm => pm.Material)
                .WithMany(p => p.Produtos)
                .HasForeignKey(pm => pm.IdMaterial);

            builder.Entity<MaterialAcabamento>().HasKey(pm => new { pm.IdMaterial, pm.IdAcabamento });
            builder.Entity<MaterialAcabamento>().HasOne(pm => pm.Material)
                .WithMany(p => p.Acabamentos)
                .HasForeignKey(pm => pm.IdMaterial);
            builder.Entity<MaterialAcabamento>().HasOne(pm => pm.Acabamento)
                .WithMany(p => p.Materiais)
                .HasForeignKey(pm => pm.IdAcabamento);

            builder.Entity<ProdutoParte>().HasKey(p => new { p.IdPai, p.IdFilho });
            builder.Entity<ProdutoParte>().HasOne(pt => pt.ProdutoFilho).
                WithMany().
                HasForeignKey(pt => pt.IdFilho).OnDelete(DeleteBehavior.Restrict);

            builder.Entity<ProdutoParte>().HasOne(pt => pt.ProdutoPai).
                WithMany(p => p.ProdutosAgregados).
                HasForeignKey(pt => pt.IdPai).OnDelete(DeleteBehavior.Restrict);

            builder.Entity<RestricaoMaterial>().HasBaseType<Restricao>();
            builder.Entity<RestricaoTamanho>().HasBaseType<Restricao>();
            builder.Entity<RestricaoObrigatoriedade>().HasBaseType<Restricao>();

            builder.Entity<DimensaoContinua>().HasBaseType<Dimensao>();
            builder.Entity<DimensaoDiscreta>().HasBaseType<Dimensao>();
            builder.Entity<ProdutoCatalogo>().HasKey(pc => new { pc.CatalogoId, pc.ProdutoId });




        }
        public DbSet<Produto> Produtos { get; set; }

        public DbSet<Categoria> Categorias { get; set; }

        public DbSet<Material> Materiais { get; set; }

        public DbSet<Acabamento> Acabamentos { get; set; }
        public DbSet<ProdutoMaterial> ProdutoMaterial { get; set; }
        public DbSet<MaterialAcabamento> MaterialAcabamento { get; set; }
        public DbSet<Dimensao> Dimensao { get; set; }
        public DbSet<Catalogo> Catalogos { get; set; }
        public DbSet<Colecao> Colecoes { get; set; }
        public DbSet<HistoricoPreco> HistoricoPrecos { get; set; }
        public DbSet<Restricao> Restricao { get; set; }
        public DbSet<Dimensoes> Dimensoes { get; set; }
        public DbSet<ProdutoParte> ProdutoPartes { get; set; }
        public DbSet<ProdutoCatalogo> ProdutoCatalogo { get; set; }
    }
}