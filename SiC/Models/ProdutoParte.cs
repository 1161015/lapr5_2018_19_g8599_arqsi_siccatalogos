﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Models
{
    public class ProdutoParte
    {
        public long IdPai { get; set; }
        [JsonIgnore]
        public Produto ProdutoPai { get; set; }
        public long IdFilho { get; set; }
        [JsonIgnore]
        public Produto ProdutoFilho { get; set; }
    }
}
