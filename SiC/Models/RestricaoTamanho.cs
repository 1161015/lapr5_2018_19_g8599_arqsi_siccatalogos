﻿using SiC.ViewModels;
using System;

namespace SiC.Models
{
    public class RestricaoTamanho : Restricao
    {
        public Double percentagemAlturaMinima { get; set; }
        public Double percentagemAlturaMaxima { get; set; }
        public Double percentagemLarguraMinima { get; set; }
        public Double percentagemLarguraMaxima { get; set; }
        public Double percentagemProfundidadeMinima { get; set; }
        public Double percentagemProfundidadeMaxima { get; set; }

        public override RestricaoDTO ToDTO()
        {
            return new RestricaoDTO()
            {
                Id = Id,
                IdPai = Pai.ProdutoId,
                IdFilho = Filho.ProdutoId,
                percentagemAlturaMinima = percentagemAlturaMinima,
                percentagemAlturaMaxima = percentagemAlturaMaxima,
                percentagemLarguraMinima = percentagemLarguraMinima,
                percentagemLarguraMaxima = percentagemLarguraMaxima,
                percentagemProfundidadeMinima = percentagemProfundidadeMinima,
                percentagemProfundidadeMaxima = percentagemProfundidadeMaxima
            };
        }
    }
}
