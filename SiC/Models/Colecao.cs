﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Models
{
    public class Colecao
    {
        public long Id { get; set; }
        public Nome Nome { get; set; }
        public Catalogo Catalogo { get; set; }
        private Colecao()
        {

        }
        public Colecao(Nome nome)
        {
            this.Nome = nome;
        }

        public override bool Equals(object obj)
        {
            var item = obj as Colecao;

            if (item == null)
            {
                return false;
            }

            if (!this.Id.Equals(item.Id))
            {
                return false;
            }
            if (!this.Nome.Equals(item.Nome))
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            
            return Nome.GetHashCode() + Catalogo.GetHashCode();
        }
    }
}
