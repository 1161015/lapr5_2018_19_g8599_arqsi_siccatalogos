﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Models
{
    [Owned]
    public class Preco
    {
        public Double valor { get; set; }
        public DateTime DataInicio { get; set; }
        private Preco()
        {

        }


    }
}
