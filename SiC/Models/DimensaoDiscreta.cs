﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Models
{
    public class DimensaoDiscreta : Dimensao
    {
        public double dimensao { get; set; }

        public override bool ValorValido(double valorInserido)
        {
            return (Math.Abs(dimensao - valorInserido) < 0.0001); //margem de erro , desvio
        }
    }
}
