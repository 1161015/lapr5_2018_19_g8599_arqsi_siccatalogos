using SiC.ViewModels;
using System.Collections.Generic;

namespace SiC.Models
{
    public class Material
    {

        public long Id { get; set; }
        public Nome Nome { get; set; }
        public virtual ICollection<MaterialAcabamento> Acabamentos { get; set; }
        public virtual ICollection<ProdutoMaterial> Produtos { get; set; }
        public int TexturaImagem { get; set; }

        public Material()
        {

        }

        public Material(Nome nome)
        {
            Nome = nome;
            Acabamentos = new List<MaterialAcabamento>();
        }

        public Material(Nome nome, List<MaterialAcabamento> acabamentos, int texturaImagem)
        {
            Nome = nome;
            Acabamentos = acabamentos;
            TexturaImagem = texturaImagem;
        }

        public Material(Nome nome, List<MaterialAcabamento> acabamentos)
        {
            Nome = nome;
            Acabamentos = acabamentos;
        }

        // toDTO method that converts and returns a DTO representation of the Material being used.
        public MaterialDTO toDTO()
        {
            List<MaterialAcabamentoDTO> acabamentosDTO = new List<MaterialAcabamentoDTO>();

            MaterialDTO materialDTO = new MaterialDTO(Nome.toDTO(), acabamentosDTO);
            materialDTO.Id = Id;
            return materialDTO;
        }

        // fromDTO method that converts and returns an instance of Material from it's DTO representation.
        public static Material fromDTO(MaterialDTO materialDTO)
        {
            List<MaterialAcabamento> acabamentos = new List<MaterialAcabamento>();
            Material material = new Material(Nome.fromDTO(materialDTO.Nome), acabamentos);
            material.Id = materialDTO.Id;
            return material;
        }

        public override bool Equals(object obj)
        {
            var item = obj as Material;

            if (item == null)
            {
                return false;
            }

            if (!Id.Equals(item.Id))
            {
                return false;
            }
            if (!Nome.Equals(item.Nome))
            {
                return false;
            }
            /*if (!this.Acabamentos.All(item.Acabamentos.Contains))
            {
                return false;
            }*/
            return true;
        }

        public override int GetHashCode()
        {
            
            return Nome.GetHashCode() + Acabamentos.GetHashCode() + Produtos.GetHashCode() + TexturaImagem.GetHashCode();
        }
    }
}