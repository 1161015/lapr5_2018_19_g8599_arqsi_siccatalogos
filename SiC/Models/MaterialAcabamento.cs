﻿using Newtonsoft.Json;
using SiC.ViewModels;

namespace SiC.Models
{
    public class MaterialAcabamento
    {
        public long IdMaterial { get; set; }
        public long IdAcabamento { get; set; }
        [JsonIgnore]
        public Material Material { get; set; }
        [JsonIgnore]
        public Acabamento Acabamento { get; set; }
        /// <summary>
        /// Construtor vazio para fins ORM
        /// </summary>
        public MaterialAcabamento()
        {

        }

        public MaterialAcabamento(Material material, Acabamento acabamento)
        {
            Material = material;
            Acabamento = acabamento;
        }

        public MaterialAcabamentoDTO ToDTO()
        {
            MaterialAcabamentoDTO dto = new MaterialAcabamentoDTO(Material.toDTO(), Acabamento.toDTO());
            return dto;
        }

        public static MaterialAcabamento FromDTO(MaterialAcabamentoDTO dto)
        {
            MaterialAcabamento materialAcabamento = new MaterialAcabamento(Material.fromDTO(dto.MaterialDTO), Acabamento.fromDTO(dto.AcabamentoDTO));
            materialAcabamento.IdAcabamento = dto.AcabamentoDTO.Id;
            materialAcabamento.IdMaterial = dto.MaterialDTO.Id;
            return materialAcabamento;
        }
    }
}
