﻿using SiC.ViewModels;
using System;


namespace SiC.Models
{

    public abstract class Restricao
    {
        public long Id { get; set; }
        public Produto Pai { get; set; }
        public Produto Filho { get; set; }


        protected Restricao()
        {

        }

        public abstract RestricaoDTO ToDTO();


        public static Restricao FromDTO(RestricaoDTO dto)
        {
            if (dto.Obrigatoriedade != null)
            {
                return new RestricaoObrigatoriedade { Pai = null, Filho = null, Obrigatoriedade = (bool)dto.Obrigatoriedade };
            }
            else if (dto.percentagemAlturaMaxima != null || dto.percentagemLarguraMaxima != null || dto.percentagemProfundidadeMaxima != null)
            {
                return new RestricaoTamanho
                {
                    Pai = null,
                    Filho = null,
                    percentagemAlturaMaxima = (Double)dto.percentagemAlturaMaxima,
                    percentagemAlturaMinima = (Double)dto.percentagemAlturaMinima,
                    percentagemLarguraMaxima = (Double)dto.percentagemLarguraMaxima,
                    percentagemLarguraMinima = (Double)dto.percentagemLarguraMinima,
                    percentagemProfundidadeMaxima = (Double)dto.percentagemProfundidadeMaxima,
                    percentagemProfundidadeMinima = (Double)dto.percentagemProfundidadeMinima
                };
            }
            else
            {
                return new RestricaoMaterial
                {
                    IdMaterialPai = (long)dto.idMaterialPai,
                    IdMaterialFilho = (long)dto.idMaterialFilho,
                    Pai = null,
                    Filho = null
                };
            }
        }
    }
}
