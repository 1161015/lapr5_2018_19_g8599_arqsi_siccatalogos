using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using SiC.ViewModels;

namespace SiC.Models
{

    [Owned]
    public class Nome
    {
        public string Conteudo { get; set; }

        public Nome(string Conteudo)
        {
            this.Conteudo = Conteudo;
        }

        public Nome()
        {

        }

        // toDTO method that converts and returns a DTO representation of the Nome being used.
        public NomeDTO toDTO()
        {
            return new NomeDTO(this.Conteudo);
        }

        // fromDTO method that converts and returns an instance of Nome from it's DTO representation.
        public static Nome fromDTO(NomeDTO nomeDTO)
        {
            String nome = nomeDTO.Conteudo;
            return new Nome(nome);
        }

        public override bool Equals(object obj)
        {
            var item = obj as Nome;

            if (item == null)
            {
                return false;
            }

            if (!this.Conteudo.Equals(item.Conteudo))
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            
            return Conteudo.GetHashCode();
        }
    }
}