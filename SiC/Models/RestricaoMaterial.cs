﻿using SiC.ViewModels;

namespace SiC.Models
{
    public class RestricaoMaterial : Restricao
    {
        public long IdMaterialPai { get; set; }
        public long IdMaterialFilho { get; set; }

        public override RestricaoDTO ToDTO()
        {
            return new RestricaoDTO()
            {
                Id = Id,
                IdPai = Pai.ProdutoId,
                IdFilho = Filho.ProdutoId,
                idMaterialFilho = IdMaterialFilho,
                idMaterialPai = IdMaterialPai
            };
        }
    }
}
