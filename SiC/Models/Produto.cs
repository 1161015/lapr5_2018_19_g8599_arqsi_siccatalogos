using SiC.Utils;
using SiC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SiC.Models
{
    public class Produto
    {
        public long ProdutoId { get; set; }
        public Nome Nome { get; set; }
        public virtual ICollection<ProdutoMaterial> Materiais { get; set; }
        public Categoria Categoria { get; set; }
        public Dimensoes Dimensoes { get; set; }
        public ICollection<ProdutoParte> ProdutosAgregados { get; set; }
        public Colecao Colecao { get; set; }
        public ICollection<ProdutoCatalogo> ProdutoCatalogos { get; set; }

        public Produto()
        {

        }
        // Constructor(s)
        public Produto(Nome nome, List<ProdutoMaterial> materiais, Categoria categoria, List<ProdutoParte> produtosAgregados, Dimensoes dimensoes, Colecao colecao)
        {
            Nome = nome;
            Materiais = materiais;
            Categoria = categoria;
            ProdutosAgregados = produtosAgregados;
            Dimensoes = dimensoes;
            Colecao = colecao;
        }
        public Produto(Nome nome, List<ProdutoMaterial> materiais, Categoria categoria, List<ProdutoParte> produtosAgregados)
        {
            Nome = nome;
            Materiais = materiais;
            Categoria = categoria;
            ProdutosAgregados = produtosAgregados;
        }
        public Produto(Nome nome, List<ProdutoMaterial> materiais, Categoria categoria, List<ProdutoParte> produtosAgregados, Dimensoes dimensoes, Restricao restricao, Colecao colecao)
        {
            Nome = nome;
            Materiais = materiais;
            Categoria = categoria;
            ProdutosAgregados = produtosAgregados;
            Dimensoes = dimensoes;
            Colecao = colecao;
        }

        public static Produto FromDTO(ProdutoDTO dto)
        {
            var Produto = new Produto();
            Produto.Nome = dto.Nome;
            Produto.Materiais = dto.Materiais;
            Produto.Categoria = dto.Categoria;
            Produto.ProdutosAgregados = dto.ProdutosAgregados;
            Produto.Colecao = dto.Colecao;
            Produto.ProdutoCatalogos = dto.ProdutoCatalogos;

            List<Dimensao> altura = new List<Dimensao>();
            var i = 0;
            for (i = 0; i < dto.Dimensoes.Altura.Count(); i++)
            {
                altura.Add(Dimensao.FromDTO(dto.Dimensoes.Altura.ElementAt(i)));
            }

            List<Dimensao> largura = new List<Dimensao>();
            for (i = 0; i < dto.Dimensoes.Largura.Count(); i++)
            {
                largura.Add(Dimensao.FromDTO(dto.Dimensoes.Largura.ElementAt(i)));
            }

            List<Dimensao> profundidade = new List<Dimensao>();
            for (i = 0; i < dto.Dimensoes.Profundidade.Count(); i++)
            {
                profundidade.Add(Dimensao.FromDTO(dto.Dimensoes.Profundidade.ElementAt(i)));
            }

            Dimensoes dim = new Dimensoes
            {
                Altura = altura,
                Largura = largura,
                Profundidade = profundidade
            };
            Produto.Dimensoes = dim;

            return Produto;
        }


        /*
         * Restri��o Caber, tem em conta a dimens�o m�xima do Produto agregador e as dimens�es m�nimas dos agregados, para m�xima efic�cia
         * O m�todo retorna true se todos os Produtos agregados couberem e false se pelo menos um n�o couber
         */
        public bool AdicionarProdutos(List<Tuplo<Produto, RestricaoTamanho>> listaProdutos)
        {
            // se a lista de produtos filho vier vazia
            if (listaProdutos.Count == 0) return true;

            // ----- dimens�es m�ximas do produto pai -----
            double alturaPai = BuscarLimiteDimensao(Dimensoes, 0, 1);
            double larguraPai = BuscarLimiteDimensao(Dimensoes, 1, 1);
            double profundidadePai = BuscarLimiteDimensao(Dimensoes, 2, 1);
            // ----------


            // ----- verifica se h� algum produto que n�o caiba em profundidade -----
            double profFilho, altFilho, largFilho;

            foreach (Tuplo<Produto, RestricaoTamanho> p in listaProdutos)
            {
                profFilho = ConverteTamanhoMinimo(p.Chave, 2, p.Valor);
                if (profFilho >= profundidadePai) return false;
            }
            // ----------

            // ----- representa��o do item agregador na forma de uma matriz booleana -----
            bool[,] matriz = new bool[(int)alturaPai, (int)larguraPai];
            for (int i = 0; i < alturaPai; i++)
                for (int j = 0; j < larguraPai; j++)
                    matriz[i, j] = false;
            // ----------


            Tuplo<Produto, RestricaoTamanho> produto = null;
            int areaFilho = 0;
            double mAltura = 0, mLargura = 0;

            // ----- percorre todos os itens da lista, tentando encaix�-los no item agregador, caso n�o consiga, retorna false -----
            while (listaProdutos.Count > 0)
            {
                // procura o filho de maior �rea na lista de produtos, para ser tratado e removido de seguida
                foreach (Tuplo<Produto, RestricaoTamanho> p in listaProdutos)
                {
                    altFilho = ConverteTamanhoMinimo(p.Chave, 0, p.Valor);
                    largFilho = ConverteTamanhoMinimo(p.Chave, 1, p.Valor);

                    if (altFilho * largFilho >= areaFilho)
                    {
                        mLargura = largFilho;
                        mAltura = altFilho;
                        produto = p;
                    }
                }
                listaProdutos.Remove(produto);

                if (!OcuparEspaco(matriz, (int)mAltura, (int)mLargura)) return false;
            }
            // ----------


            return true;
        }

        /*
         * Fun��o de suporte � AdicionarProdutos
         * Tipo: 0 - Altura, 1 - Largura, 2 - Profundidade
         * MinMax: 0 - Min, 1 - Max
         */
        public double BuscarLimiteDimensao(Dimensoes dimensoes, int tipo, int minMax)
        {
            ICollection<Dimensao> listaDimensoes;

            switch (tipo)
            {
                case 0:
                    listaDimensoes = dimensoes.Altura;
                    break;
                case 1:
                    listaDimensoes = dimensoes.Largura;
                    break;
                default:
                    listaDimensoes = dimensoes.Profundidade;
                    break;
            }

            double medida;
            DimensaoContinua dc;
            DimensaoDiscreta dd;

            if (minMax == 0) medida = Double.MaxValue;
            else medida = 0;

            foreach (Dimensao d in listaDimensoes)
            {
                if (d is DimensaoContinua)
                {
                    dc = (DimensaoContinua)d;
                    if (minMax == 0)
                    {
                        if (dc.dimensaoMin < medida) medida = dc.dimensaoMin;
                    }
                    else
                    {
                        if (dc.dimensaoMax >= medida) medida = dc.dimensaoMax;
                    }
                }
                else
                {
                    dd = (DimensaoDiscreta)d;
                    if (minMax == 0)
                    {
                        if (dd.dimensao < medida) medida = dd.dimensao;
                    }
                    else
                    {
                        if (dd.dimensao >= medida) medida = dd.dimensao;
                    }
                }
            }

            return medida;
        }

        /**
         * Se o tamanho m�nimo do produto filho for mais pequeno que a taxa minima de ocupa��o que este pode ocupar no pai, retorna a segunda op��o
         **/
        public Double ConverteTamanhoMinimo(Produto filho, int tipo, RestricaoTamanho rt)
        {
            Double tamanhoPai, tamanhoFilho;
            switch (tipo)
            {
                case 0:
                    tamanhoPai = BuscarLimiteDimensao(Dimensoes, 0, 1);
                    tamanhoFilho = BuscarLimiteDimensao(filho.Dimensoes, 0, 0);
                    if (tamanhoPai * (rt.percentagemAlturaMinima / 100) > tamanhoFilho) tamanhoFilho = tamanhoPai * (rt.percentagemAlturaMinima / 100);
                    break;
                case 1:
                    tamanhoPai = BuscarLimiteDimensao(Dimensoes, 1, 1);
                    tamanhoFilho = BuscarLimiteDimensao(filho.Dimensoes, 1, 0);
                    if (tamanhoPai * (rt.percentagemLarguraMinima / 100) > tamanhoFilho) tamanhoFilho = tamanhoPai * (rt.percentagemLarguraMinima / 100);
                    break;
                default:
                    tamanhoPai = BuscarLimiteDimensao(Dimensoes, 2, 1);
                    tamanhoFilho = BuscarLimiteDimensao(filho.Dimensoes, 2, 0);
                    if (tamanhoPai * (rt.percentagemProfundidadeMinima / 100) > tamanhoFilho) tamanhoFilho = tamanhoPai * (rt.percentagemProfundidadeMinima / 100);
                    break;
            }
            return tamanhoFilho;
        }

        public List<long> IdsProdutosFilhos()
        {
            List<long> retorno = new List<long>();
            foreach (ProdutoParte pp in ProdutosAgregados)
            {
                retorno.Add(pp.IdFilho);
            }
            return retorno;
        }

        private bool OcuparEspaco(bool[,] matriz, int altura, int largura)
        {
            int i, j;

            for (i = 0; i < matriz.Length; i++)
            {
                for (j = 0; j < matriz.GetLength(0); j++)
                {
                    if (!matriz[i, j]) // se o lugar estiver desocupado
                    {
                        if (i + altura <= matriz.Length && j + largura <= matriz.GetLength(0)) // se ainda couber no espa�o restante
                        {
                            if (EspacoLivre(matriz, i, j, altura, largura))
                            {
                                OcuparEspacoAux(matriz, i, j, altura, largura);
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        private bool EspacoLivre(bool[,] matriz, int i, int j, int altura, int largura)
        {
            for (int x = i; x < i + altura; x++)
            {
                for (int y = j; y < j + largura; y++)
                {
                    if (matriz[x, y]) return false;
                }
            }
            return true;
        }

        private void OcuparEspacoAux(bool[,] matriz, int i, int j, int altura, int largura)
        {
            for (int x = i; x < i + altura; x++)
            {
                for (int y = j; y < j + largura; y++)
                {
                    matriz[x, y] = true;
                }
            }
        }

        public bool CumpreTaxaOcupacao(RestricaoTamanho rt, Produto filho)
        {
            double medidaPMax, medidaPMin, medidaFMax, medidaFMin;
            DimensaoDiscreta dd;
            Boolean encontrado;

            if (rt.percentagemAlturaMinima != -1)
            {
                medidaPMax = (rt.percentagemAlturaMaxima / 100) * BuscarLimiteDimensao(Dimensoes, 0, 1);
                medidaPMin = (rt.percentagemAlturaMinima / 100) * BuscarLimiteDimensao(Dimensoes, 0, 1);
                medidaFMax = BuscarLimiteDimensao(filho.Dimensoes, 0, 1);
                medidaFMin = BuscarLimiteDimensao(filho.Dimensoes, 0, 0);


                if (medidaFMax < medidaPMin || medidaFMin > medidaPMax) return false;

                if (filho.Dimensoes.Altura.First() is DimensaoDiscreta)
                {
                    encontrado = false;
                    foreach (Dimensao d in filho.Dimensoes.Altura)
                    {
                        dd = (DimensaoDiscreta)d;
                        if (dd.dimensao <= medidaPMax && dd.dimensao >= medidaPMin) encontrado = true;
                    }
                    if (!encontrado) return false;
                }
            }

            if (rt.percentagemLarguraMinima != -1)
            {
                medidaPMax = (rt.percentagemLarguraMaxima / 100) * BuscarLimiteDimensao(Dimensoes, 1, 1);
                medidaPMin = (rt.percentagemLarguraMinima / 100) * BuscarLimiteDimensao(Dimensoes, 1, 1);
                medidaFMax = BuscarLimiteDimensao(filho.Dimensoes, 1, 1);
                medidaFMin = BuscarLimiteDimensao(filho.Dimensoes, 1, 0);

                if (medidaFMax < medidaPMin || medidaFMin > medidaPMax) return false;

                if (filho.Dimensoes.Largura.First() is DimensaoDiscreta)
                {
                    encontrado = false;
                    foreach (Dimensao d in filho.Dimensoes.Largura)
                    {
                        dd = (DimensaoDiscreta)d;
                        if (dd.dimensao <= medidaPMax && dd.dimensao >= medidaPMin) encontrado = true;
                    }
                    if (!encontrado) return false;
                }
            }


            if (rt.percentagemProfundidadeMinima != -1)
            {
                medidaPMax = (rt.percentagemProfundidadeMaxima / 100) * BuscarLimiteDimensao(Dimensoes, 2, 1);
                medidaPMin = (rt.percentagemProfundidadeMinima / 100) * BuscarLimiteDimensao(Dimensoes, 2, 1);
                medidaFMax = BuscarLimiteDimensao(filho.Dimensoes, 2, 1);
                medidaFMin = BuscarLimiteDimensao(filho.Dimensoes, 2, 0);

                if (medidaFMax < medidaPMin || medidaFMin > medidaPMax) return false;

                if (filho.Dimensoes.Largura.First() is DimensaoDiscreta)
                {
                    encontrado = false;
                    foreach (Dimensao d in filho.Dimensoes.Largura)
                    {
                        dd = (DimensaoDiscreta)d;
                        if (dd.dimensao <= medidaPMax && dd.dimensao >= medidaPMin) encontrado = true;
                    }
                    if (!encontrado) return false;
                }
            }

            return true;
        }

        public bool PodeTerFilhos()
        {
            return PodeTerFilhos(Categoria);
        }

        private bool PodeTerFilhos(Categoria categoria)
        {
            if (categoria.Nome.Conteudo.ToLower().Equals("arm�rio", StringComparison.InvariantCultureIgnoreCase)
                || categoria.Nome.Conteudo.ToLower().Equals("armario", StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }
            else if (categoria.CategoriaPai != null)
            {
                return PodeTerFilhos(categoria.CategoriaPai);
            }
            else
            {
                return false;
            }

        }



        public override bool Equals(object obj)
        {
            var item = obj as Produto;

            if (item == null)
            {
                return false;
            }

            if (!ProdutoId.Equals(item.ProdutoId))
            {
                return false;
            }
            if (!Nome.Equals(item.Nome))
            {
                return false;
            }
            if (!Materiais.Equals(item.Materiais))
            {
                return false;
            }
            if (Categoria != null) // adicionado por causa de problemas a remover produtos de uma lista, quando estes nao tinham categorias atribuidas
                if (!Categoria.Equals(item.Categoria))
                {
                    return false;
                }
            if (!ProdutosAgregados.All(item.ProdutosAgregados.Contains))
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            
            return Nome.GetHashCode();
        }
    }
}