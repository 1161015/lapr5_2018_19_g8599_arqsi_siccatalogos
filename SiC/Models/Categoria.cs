using System;
using System.ComponentModel.DataAnnotations.Schema;
using SiC.ViewModels;

namespace SiC.Models
{
    public class Categoria
    {
        public long? Id { get; set; }
        public  Nome Nome { get; set; }
        public Categoria CategoriaPai { get; set; }

        public Categoria()
        {

        }

        public Categoria(Nome nome, Categoria categoriaPai)
        {
            this.Nome = nome;
            this.CategoriaPai = categoriaPai;
        }

        // toDTO method that converts and returns a DTO representation of the Categoria being used.
        public CategoriaDTO toDTO()
        {
            CategoriaDTO categoriaDTO;
            if (this.CategoriaPai != null)
            {
                categoriaDTO = new CategoriaDTO(this.Nome.toDTO(), this.CategoriaPai.toDTO());
            }
            else
            {
                categoriaDTO = new CategoriaDTO(this.Nome.toDTO(), null); // TODO: Check this later on
            }
          //  categoriaDTO.Id = this.Id;
            return categoriaDTO;
        }


        // fromDTO method that converts and returns an instance of Categoria from it's DTO representation.
        public static Categoria fromDTO(CategoriaDTO categoriaDTO)
        {
            if (categoriaDTO == null) return null;
            Categoria categoria = new Categoria(Nome.fromDTO(categoriaDTO.Nome), Categoria.fromDTO(categoriaDTO.CategoriaPai));
            categoria.Id = categoriaDTO.Id;
            return categoria;
        }

        public override bool Equals(object obj)
        {
            var item = obj as Categoria;

            if (item == null)
            {
                return false;
            }

            if (!this.Id.Equals(item.Id))
            {
                return false;
            }
            if (!this.Nome.Equals(item.Nome))
            {
                return false;
            }
            if(item.CategoriaPai!=null && this.CategoriaPai != null)
            {
                if (!this.CategoriaPai.Equals(item.CategoriaPai))
                {
                    return false;
                }
            }
            if (item.CategoriaPai == null && this.CategoriaPai != null)
            {
                return false;
            }

            if (item.CategoriaPai != null && this.CategoriaPai == null)
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            
            return Nome.GetHashCode() + CategoriaPai.GetHashCode();
        }
    }
}