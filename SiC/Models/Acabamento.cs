using SiC.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SiC.Models
{
    public class Acabamento
    {
        public long Id { get; set; }
        public Nome Nome { get; set; }

        public string Cor { get; set; }

        public Double Opacidade { get; set; } // 0 = n�o opaco 1 = opaco

        public bool Fosco { get; set; }

        public Double Brilho { get; set; } // 0 a 1

        public Double Emissividade { get; set; } // 0 a 1

        public virtual ICollection<MaterialAcabamento> Materiais { get; set; }

        private Acabamento()
        {

        }

        public Acabamento(Nome nome)
        {
            Nome = nome;
            Materiais = new List<MaterialAcabamento>();
        }

        public Acabamento(Nome nome, List<MaterialAcabamento> materiais)
        {
            Nome = nome;
            Materiais = materiais;
        }

        // toDTO method that converts and returns a DTO representation of the Acabamento being used.
        public AcabamentoDTO toDTO()
        {
            List<MaterialAcabamentoDTO> lista = new List<MaterialAcabamentoDTO>();

            AcabamentoDTO acabamentoDTO = new AcabamentoDTO(Nome.toDTO(), lista);
            acabamentoDTO.Id = Id;
            return acabamentoDTO;
        }

        // fromDTO method that converts and returns an instance of Acabamento from it's DTO representation.
        public static Acabamento fromDTO(AcabamentoDTO acabamentoDTO)
        {
            List<MaterialAcabamento> lista = new List<MaterialAcabamento>();
            Nome nome = Nome.fromDTO(acabamentoDTO.Nome);

            Debug.WriteLine("\n\n\n");
            Debug.WriteLine(nome.Conteudo);
            Debug.WriteLine("\n\n\n");

            Acabamento acabamento = new Acabamento(nome, lista);
            acabamento.Id = acabamentoDTO.Id;
            return acabamento;
        }

        public override bool Equals(object obj)
        {
            var item = obj as Acabamento;

            if (item == null)
            {
                return false;
            }

            if (!Id.Equals(item.Id))
            {
                return false;
            }
            if (!Nome.Equals(item.Nome))
            {
                return false;
            }
            return true;
        }

         public override int GetHashCode()
        {
            
            return Id.GetHashCode() + Nome.GetHashCode() + Cor.GetHashCode()+Opacidade.GetHashCode()+Fosco.GetHashCode()+
                Brilho.GetHashCode()+Emissividade.GetHashCode() + Materiais.GetHashCode();
        }
    }
}