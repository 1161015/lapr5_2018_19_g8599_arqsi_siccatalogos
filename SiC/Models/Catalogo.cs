﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Models
{
    public class Catalogo
    {
        public long CatalogoId { get; set; }
        public Nome Nome { get; set; }
        public virtual ICollection<ProdutoCatalogo> ProdutoCatalogos { get; set; }

        private Catalogo()
        {

        }

    }
}
