﻿using SiC.ViewModels;
using System;

namespace SiC.Models
{
    public class RestricaoObrigatoriedade : Restricao
    {
        public Boolean Obrigatoriedade { get; set; }

        public override RestricaoDTO ToDTO()
        {
            return new RestricaoDTO()
            {
                Id = Id,
                IdPai = Pai.ProdutoId,
                IdFilho = Filho.ProdutoId,
                Obrigatoriedade = Obrigatoriedade
            };
        }
    }
}
