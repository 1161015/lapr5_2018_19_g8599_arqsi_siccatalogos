using SiC.ViewModels;
using System;

namespace SiC.Models
{

    public abstract class Dimensao
    {
        public long Id { get; set; }

        public static readonly double ALTURA_MAXIMA = 220;
        public static readonly double PROFUNDIDADE_MAXIMA = 200;
        public static readonly double LARGURA_MAXIMA = 300;
        public static readonly double ALTURA_MINIMA_ARMARIO = 60;
        public static readonly double PROFUNDIDADE_MINIMA_ARMARIO = 60;
        public static readonly double LARGURA_MINIMA_ARMARIO = 60;
        public static readonly double ALTURA_MINIMA = 50;
        public static readonly double PROFUNDIDADE_MINIMA = 50;
        public static readonly double LARGURA_MINIMA = 50;
        protected Dimensao()
        {

        }

        public abstract bool ValorValido(double valorInserido);

        public static Dimensao FromDTO(DimensaoDTO dto)
        {
            if (dto.dimensao != null)
            {
                return new DimensaoDiscreta
                {
                    dimensao = (Double)dto.dimensao
                };
            }
            else
            {
                return new DimensaoContinua
                {
                    dimensaoMin = (Double)dto.dimensaoMin,
                    dimensaoMax = (Double)dto.dimensaoMax
                };
            }
        }
        //220 altura , 300 largura, 200 profundidade m�ximo de tudo
        public static bool ValidarAltura(double valor, bool armario)
        {
            if (valor > ALTURA_MAXIMA) return false;
            if (armario)
            {
                if (valor < ALTURA_MINIMA_ARMARIO) return false;
            }
            else
            {
                if (valor < ALTURA_MINIMA) return false;
            }
            return true;
        }

        public static bool ValidarLargura(double valor, bool armario)
        {
            if (valor > LARGURA_MAXIMA) return false;
            if (armario)
            {
                if (valor < LARGURA_MINIMA_ARMARIO) return false;
            }
            else
            {
                if (valor < LARGURA_MINIMA) return false;
            }
            return true;
        }

        public static bool ValidarProfundidade(double valor, bool armario)
        {
            if (valor > PROFUNDIDADE_MAXIMA) return false;
            if (armario)
            {
                if (valor < PROFUNDIDADE_MINIMA_ARMARIO) return false;
            }
            else
            {
                if (valor < PROFUNDIDADE_MINIMA) return false;
            }
            return true;
        }

    }
}