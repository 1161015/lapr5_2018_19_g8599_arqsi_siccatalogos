﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Models
{
    public class HistoricoPreco
    {
        public long Id { get; set; }
        public long Id_Material { get; set; }
        public long? Id_Acabamento { get; set; }
        public Preco Preco { get; set; }



        private HistoricoPreco ()
        {

        }
    }
}
