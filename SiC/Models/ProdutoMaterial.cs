﻿using Newtonsoft.Json;
using SiC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Models
{
    public class ProdutoMaterial
    {
        public long IdProduto { get; set; }
        public long IdMaterial { get; set; }
        [JsonIgnore]
        public Produto Produto { get; set; }
        [JsonIgnore]
        public Material Material { get; set; }
        private ProdutoMaterial()
        {

        }

        public ProdutoMaterial(Produto produto, Material material)
        {
            Produto = produto;
            Material = material;
        }


    }
}
