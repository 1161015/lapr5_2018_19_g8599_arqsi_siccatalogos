﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.ViewModels
{
    public class MaterialAcabamentoDTO
    {
        public MaterialDTO MaterialDTO { get; set; }
        public AcabamentoDTO AcabamentoDTO { get; set; }

        public MaterialAcabamentoDTO(MaterialDTO materialDTO, AcabamentoDTO acabamentoDTO)
        {
            MaterialDTO = materialDTO;
            AcabamentoDTO = acabamentoDTO;
        }


    }
}
