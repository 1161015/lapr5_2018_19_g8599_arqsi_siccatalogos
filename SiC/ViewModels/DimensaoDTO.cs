﻿using System;
using System.Collections.Generic;

namespace SiC.ViewModels
{
    public class DimensaoDTO
    {
        public long Id { get; set; }

        public Double? dimensao { get; set; }
        public Double? dimensaoMin { get; set; }
        public Double? dimensaoMax { get; set; }
    }
}
