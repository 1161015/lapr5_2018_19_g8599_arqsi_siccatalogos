﻿using System;
using System.Collections.Generic;

namespace SiC.ViewModels
{
    public class DimensoesDTO
    {

        public ICollection<DimensaoDTO> Altura { get; set; }
        public ICollection<DimensaoDTO> Largura { get; set; }
        public ICollection<DimensaoDTO> Profundidade { get; set; }

    }
}
