﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SiC.ViewModels
{
    public class MaterialDTO
    {

        public long Id { get; set; }
        public NomeDTO Nome { get; set; }
        public List<MaterialAcabamentoDTO> Acabamentos { get; set; }

        public MaterialDTO(NomeDTO nome)
        {
            this.Nome = nome;
            Acabamentos = new List<MaterialAcabamentoDTO>();
        }

        public MaterialDTO(NomeDTO nome, List<MaterialAcabamentoDTO> acabamentos)
        {
            this.Nome = nome;
            this.Acabamentos = acabamentos;
        }

        public override bool Equals(object obj)
        {
            var item = obj as MaterialDTO;

            if (item == null)
            {
                return false;
            }

            if (!this.Id.Equals(item.Id))
            {
                return false;
            }
            if (!this.Nome.Equals(item.Nome))
            {
                return false;
            }
            if (!this.Acabamentos.All(item.Acabamentos.Contains))
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            
            return Nome.GetHashCode() + Acabamentos.GetHashCode();
        }
    }
}
