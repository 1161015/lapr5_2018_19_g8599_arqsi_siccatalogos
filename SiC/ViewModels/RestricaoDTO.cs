﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.ViewModels
{
    public class RestricaoDTO
    {
        public long Id { get; set; }
        public long IdPai { get; set; }
        public long IdFilho { get; set; }
        public Double? percentagemAlturaMinima { get; set; }
        public Double? percentagemAlturaMaxima { get; set; }
        public Double? percentagemLarguraMinima { get; set; }
        public Double? percentagemLarguraMaxima { get; set; }
        public Double? percentagemProfundidadeMinima { get; set; }
        public Double? percentagemProfundidadeMaxima { get; set; }
        public long? idMaterialPai { get; set; }
        public long? idMaterialFilho { get; set; }
        public bool? Obrigatoriedade { get; set; }

    }
}
