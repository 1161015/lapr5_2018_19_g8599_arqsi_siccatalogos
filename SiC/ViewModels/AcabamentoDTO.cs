﻿using System;
using System.Collections.Generic;

namespace SiC.ViewModels
{
    public class AcabamentoDTO
    {

        public long Id { get; set; }
        public NomeDTO Nome { get; set; }

        public List<MaterialAcabamentoDTO> Materiais { get; set; }

        public AcabamentoDTO(NomeDTO nome)
        {
            this.Nome = nome;
            this.Materiais = new List<MaterialAcabamentoDTO>();
        }

        public AcabamentoDTO(NomeDTO nome,List<MaterialAcabamentoDTO> materiais)
        {
            this.Nome = nome;
            this.Materiais = materiais;
        }

        public override bool Equals(object obj)
        {
            var item = obj as AcabamentoDTO;

            if (item == null)
            {
                return false;
            }

            if (!this.Id.Equals(item.Id))
            {
                return false;
            }
            if (!this.Nome.Equals(item.Nome))
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            
            return Id.GetHashCode() + Nome.GetHashCode() + Materiais.GetHashCode();
        }
    }
}
