﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.ViewModels
{
    public class ProdutoMaterialDTO
    {
        public ProdutoDTO ProdutoDTO { get; set; }
        public MaterialDTO MaterialDTO { get; set; }

        public ProdutoMaterialDTO(ProdutoDTO produtoDTO, MaterialDTO materialDTO)
        {
            ProdutoDTO = produtoDTO;
            MaterialDTO = materialDTO;
        }
    }
}
