﻿using System;

namespace SiC.ViewModels
{
    public class NomeDTO
    {
        public string Conteudo { get; set; }

        public NomeDTO(string Conteudo)
        {
            this.Conteudo = Conteudo;
        }


        public override bool Equals(object obj)
        {
            var item = obj as NomeDTO;

            if (item == null)
            {
                return false;
            }

            if (!this.Conteudo.Equals(item.Conteudo))
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            
            return Conteudo.GetHashCode();
        }

    }
}
