﻿using System;

namespace SiC.ViewModels
{
    public class ValorDTO
    {
        public double Conteudo { get; set; }

        public ValorDTO(double conteudo)
        {
            this.Conteudo = conteudo;
        }

        public override bool Equals(object obj)
        {
            var item = obj as ValorDTO;

            if (item == null)
            {
                return false;
            }

            if (!this.Conteudo.Equals(item.Conteudo))
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            
            return Conteudo.GetHashCode();
        }
    }
}
