using System;

namespace SiC.ViewModels
{
    public class CategoriaDTO
    {

        public long Id { get; set; }
        public NomeDTO Nome { get; set; }
        public CategoriaDTO CategoriaPai { get; set; }

        // Construtor
        public CategoriaDTO(NomeDTO nome, CategoriaDTO categoriaPai)
        {
            this.Nome = nome;
            this.CategoriaPai = categoriaPai;
        }

        public override bool Equals(object obj)
        {
            var item = obj as CategoriaDTO;

            if (item == null)
            {
                return false;
            }

            if (!this.Id.Equals(item.Id))
            {
                return false;
            }
            if (!this.Nome.Equals(item.Nome))
            {
                return false;
            }
            if (item.CategoriaPai != null && this.CategoriaPai != null)
            {
                if (!this.CategoriaPai.Equals(item.CategoriaPai))
                {
                    return false;
                }
            }
            if (item.CategoriaPai == null && this.CategoriaPai != null)
            {
                return false;
            }

            if (item.CategoriaPai != null && this.CategoriaPai == null)
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            
            return Nome.GetHashCode() + CategoriaPai.GetHashCode();
        }
    }
}