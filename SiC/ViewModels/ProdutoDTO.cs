using System.Collections.Generic;
using System.Linq;
using SiC.Models;

namespace SiC.ViewModels
{
    public class ProdutoDTO
    {
        public long Id { get; set; }
        public Nome Nome { get; set; }
        public ICollection<ProdutoMaterial> Materiais { get; set; }
        public Categoria Categoria { get; set; }
        public ICollection<ProdutoParte> ProdutosAgregados { get; set; }
        public Colecao Colecao { get; set; }
        public ICollection<ProdutoCatalogo> ProdutoCatalogos { get; set; }
        public DimensoesDTO Dimensoes { get; set; }

        public ProdutoDTO(Nome nome, List<ProdutoMaterial> materiais, Categoria categoria, List<ProdutoParte> produtosAgregados)
        {
            Nome = nome;
            Materiais = materiais;
            Categoria = categoria;
            ProdutosAgregados = produtosAgregados;

        }

        public override bool Equals(object obj)
        {
            var item = obj as ProdutoDTO;

            if (item == null)
            {
                return false;
            }

            if (!Id.Equals(item.Id))
            {
                return false;
            }
            return true;
        }

    }
}