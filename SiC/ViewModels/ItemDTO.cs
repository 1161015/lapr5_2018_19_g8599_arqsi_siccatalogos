﻿using SiC.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.ViewModels
{
    public class ItemDTO
    {
        public long ProdutoPaiId { get; set; }
        public Tuplo<long, long> MaterialAcabamentoPai { get; set; }
        public List<long> ProdutosFilhosIds { get; set; }
        public List<long> MaterialIdFilhos { get; set; }
        public List<Percentagem> PercentagensFilhos { get; set; }



    }
    public class Percentagem
    {
        public double PercentagemAltura { get; set; }
        public double PercentagemLargura { get; set; }
        public double PercentagemProfundidade { get; set; }
    }


}
