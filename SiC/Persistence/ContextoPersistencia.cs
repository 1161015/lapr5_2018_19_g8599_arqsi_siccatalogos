﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Persistence
{
    public class ContextoPersistencia
    {
        private static volatile IFabricaRepositorio Instancia;

        private ContextoPersistencia()
        {

        }

        public static IFabricaRepositorio Repositorios()
        {
            if (Instancia == null)
            {
                string tipoRepositorio = Program.TipoRepositorio();
                Instancia = (IFabricaRepositorio) System.Reflection.Assembly.GetExecutingAssembly().CreateInstance(tipoRepositorio);
            }
            return Instancia;
        }
    }
}
