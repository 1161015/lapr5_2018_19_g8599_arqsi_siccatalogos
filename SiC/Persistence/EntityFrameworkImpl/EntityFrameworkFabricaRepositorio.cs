﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Persistence.EntityFrameworkImpl
{
    public class EntityFrameworkFabricaRepositorio : IFabricaRepositorio
    {
        public IAcabamentoRepositorio Acabamentos()
        {
            return null;
            //return new EntityFrameworkAcabamentoRepositorio();
        }

        public ICategoriaRepositorio Categorias()
        {
            return null;
            //return new EntityFrameworkCategoriaRepositorio();
        }

        public IMaterialRepositorio Materiais()
        {
            return null;
           // return new EntityFrameworkMaterialRepositorio();
        }

        public IProdutoRepositorio Produtos()
        {
            return new EntityFrameworkProdutoRepositorio();
        }

    }
}
