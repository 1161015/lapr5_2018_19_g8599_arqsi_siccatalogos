﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SiC.Models;
using SiC.Utils;

namespace SiC.Persistence.EntityFrameworkImpl
{
    public class EntityFrameworkProdutoRepositorio : RepositorioDados<Produto, long>, IProdutoRepositorio
    {
        public override bool Delete(Produto entity)
        {
            throw new NotImplementedException();
        }

        public override bool Delete(long primaryKey)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<Produto> FindAll()
        {
            throw new NotImplementedException();
        }

        public override Produto FindOne(long id)
        {
            throw new NotImplementedException();
        }

        public override Produto Save(Produto entity)
        {
            throw new NotImplementedException();
        }
    }
}
