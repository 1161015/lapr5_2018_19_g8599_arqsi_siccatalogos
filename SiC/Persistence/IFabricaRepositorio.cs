﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Persistence
{
    public interface IFabricaRepositorio
    {
        IAcabamentoRepositorio Acabamentos();
        IMaterialRepositorio Materiais();
        IProdutoRepositorio Produtos();
        ICategoriaRepositorio Categorias();

    }
}
