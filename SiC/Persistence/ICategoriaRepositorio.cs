﻿using SiC.Models;
using SiC.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Persistence
{
    public interface ICategoriaRepositorio : IRepositorioDados<Categoria,long>
    {
    }
}
