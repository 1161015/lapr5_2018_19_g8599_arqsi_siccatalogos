﻿using SiC.Models;
using SiC.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Persistence.InMemoryImpl
{
    public class EmMemoriaAcabamentoRepositorio : RepositorioEmMemoria<Acabamento, long>, IAcabamentoRepositorio
    {
        public override long GenerateKey(Acabamento entity)
        {
            return entity.Id;
        }
    }
}
