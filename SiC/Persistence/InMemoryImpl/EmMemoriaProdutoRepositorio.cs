﻿using SiC.Models;
using SiC.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Persistence.InMemoryImpl
{
    public class EmMemoriaProdutoRepositorio : RepositorioEmMemoria<Produto, long>, IProdutoRepositorio
    {
        public override long GenerateKey(Produto entity)
        {
            return entity.ProdutoId;
        }
    }
}
