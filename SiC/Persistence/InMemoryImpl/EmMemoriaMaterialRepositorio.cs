﻿using SiC.Models;
using SiC.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Persistence.InMemoryImpl
{
    public class EmMemoriaMaterialRepositorio : RepositorioEmMemoria<Material, long>, IMaterialRepositorio
    {
        public override long GenerateKey(Material entity)
        {
            return entity.Id;
        }
    }
}
