﻿using SiC.Models;
using SiC.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Persistence.InMemoryImpl
{
    public class EmMemoriaCategoriaRepositorio : RepositorioEmMemoria<Categoria, long>, ICategoriaRepositorio
    {
        public override long GenerateKey(Categoria entity)
        {
            return 1;
        }
    }
}
