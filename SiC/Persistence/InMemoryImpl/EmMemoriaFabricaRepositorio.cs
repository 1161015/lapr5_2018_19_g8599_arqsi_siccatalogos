﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiC.Persistence.InMemoryImpl
{
    public class EmMemoriaFabricaRepositorio : IFabricaRepositorio
    {
        public IAcabamentoRepositorio Acabamentos()
        {
            return new EmMemoriaAcabamentoRepositorio();
        }

        public ICategoriaRepositorio Categorias()
        {
            return new EmMemoriaCategoriaRepositorio();
        }

        public IMaterialRepositorio Materiais()
        {
            return new EmMemoriaMaterialRepositorio();
        }

        public IProdutoRepositorio Produtos()
        {
            return new EmMemoriaProdutoRepositorio();
        }
    }
}
