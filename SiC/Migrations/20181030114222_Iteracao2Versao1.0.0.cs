﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class Iteracao2Versao100 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Restricao_MaterialAcabamento",
                table: "Produtos",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<double>(
                name: "Restricao_PercentagemMaxima",
                table: "Produtos",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Restricao_PercentagemMinima",
                table: "Produtos",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Restricao_MaterialAcabamento",
                table: "Produtos");

            migrationBuilder.DropColumn(
                name: "Restricao_PercentagemMaxima",
                table: "Produtos");

            migrationBuilder.DropColumn(
                name: "Restricao_PercentagemMinima",
                table: "Produtos");
        }
    }
}
