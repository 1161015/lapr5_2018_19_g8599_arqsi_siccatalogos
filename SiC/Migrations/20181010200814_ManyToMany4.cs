﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class ManyToMany4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MaterialAcabamento_Acabamentos_AcabamentoId",
                table: "MaterialAcabamento");

            migrationBuilder.DropForeignKey(
                name: "FK_ProdutoMaterial_Materiais_MaterialId",
                table: "ProdutoMaterial");

            migrationBuilder.DropIndex(
                name: "IX_ProdutoMaterial_MaterialId",
                table: "ProdutoMaterial");

            migrationBuilder.DropIndex(
                name: "IX_MaterialAcabamento_AcabamentoId",
                table: "MaterialAcabamento");

            migrationBuilder.DropColumn(
                name: "MaterialId",
                table: "ProdutoMaterial");

            migrationBuilder.DropColumn(
                name: "AcabamentoId",
                table: "MaterialAcabamento");

            migrationBuilder.AddColumn<long>(
                name: "Id",
                table: "ProdutoMaterial",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "Id",
                table: "MaterialAcabamento",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_ProdutoMaterial_Id",
                table: "ProdutoMaterial",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_MaterialAcabamento_Id",
                table: "MaterialAcabamento",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MaterialAcabamento_Acabamentos_Id",
                table: "MaterialAcabamento",
                column: "Id",
                principalTable: "Acabamentos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProdutoMaterial_Materiais_Id",
                table: "ProdutoMaterial",
                column: "Id",
                principalTable: "Materiais",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MaterialAcabamento_Acabamentos_Id",
                table: "MaterialAcabamento");

            migrationBuilder.DropForeignKey(
                name: "FK_ProdutoMaterial_Materiais_Id",
                table: "ProdutoMaterial");

            migrationBuilder.DropIndex(
                name: "IX_ProdutoMaterial_Id",
                table: "ProdutoMaterial");

            migrationBuilder.DropIndex(
                name: "IX_MaterialAcabamento_Id",
                table: "MaterialAcabamento");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "ProdutoMaterial");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "MaterialAcabamento");

            migrationBuilder.AddColumn<long>(
                name: "MaterialId",
                table: "ProdutoMaterial",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "AcabamentoId",
                table: "MaterialAcabamento",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProdutoMaterial_MaterialId",
                table: "ProdutoMaterial",
                column: "MaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_MaterialAcabamento_AcabamentoId",
                table: "MaterialAcabamento",
                column: "AcabamentoId");

            migrationBuilder.AddForeignKey(
                name: "FK_MaterialAcabamento_Acabamentos_AcabamentoId",
                table: "MaterialAcabamento",
                column: "AcabamentoId",
                principalTable: "Acabamentos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProdutoMaterial_Materiais_MaterialId",
                table: "ProdutoMaterial",
                column: "MaterialId",
                principalTable: "Materiais",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
