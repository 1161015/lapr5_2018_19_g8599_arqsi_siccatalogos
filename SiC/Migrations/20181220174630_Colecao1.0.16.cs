﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class Colecao1016 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Brilho",
                table: "Acabamentos",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "Cor",
                table: "Acabamentos",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Emissividade",
                table: "Acabamentos",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<bool>(
                name: "Fosco",
                table: "Acabamentos",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<double>(
                name: "Opacidade",
                table: "Acabamentos",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Brilho",
                table: "Acabamentos");

            migrationBuilder.DropColumn(
                name: "Cor",
                table: "Acabamentos");

            migrationBuilder.DropColumn(
                name: "Emissividade",
                table: "Acabamentos");

            migrationBuilder.DropColumn(
                name: "Fosco",
                table: "Acabamentos");

            migrationBuilder.DropColumn(
                name: "Opacidade",
                table: "Acabamentos");
        }
    }
}
