﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class Colecao109 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HistoricoPrecos_Acabamentos_AcabamentoId",
                table: "HistoricoPrecos");

            migrationBuilder.DropForeignKey(
                name: "FK_HistoricoPrecos_Materiais_MaterialId",
                table: "HistoricoPrecos");

            migrationBuilder.DropIndex(
                name: "IX_HistoricoPrecos_AcabamentoId",
                table: "HistoricoPrecos");

            migrationBuilder.DropIndex(
                name: "IX_HistoricoPrecos_MaterialId",
                table: "HistoricoPrecos");

            migrationBuilder.DropColumn(
                name: "AcabamentoId",
                table: "HistoricoPrecos");

            migrationBuilder.DropColumn(
                name: "MaterialId",
                table: "HistoricoPrecos");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "AcabamentoId",
                table: "HistoricoPrecos",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "MaterialId",
                table: "HistoricoPrecos",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_HistoricoPrecos_AcabamentoId",
                table: "HistoricoPrecos",
                column: "AcabamentoId");

            migrationBuilder.CreateIndex(
                name: "IX_HistoricoPrecos_MaterialId",
                table: "HistoricoPrecos",
                column: "MaterialId");

            migrationBuilder.AddForeignKey(
                name: "FK_HistoricoPrecos_Acabamentos_AcabamentoId",
                table: "HistoricoPrecos",
                column: "AcabamentoId",
                principalTable: "Acabamentos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_HistoricoPrecos_Materiais_MaterialId",
                table: "HistoricoPrecos",
                column: "MaterialId",
                principalTable: "Materiais",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
