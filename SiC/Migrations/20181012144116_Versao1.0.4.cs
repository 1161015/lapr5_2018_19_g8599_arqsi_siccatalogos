﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class Versao104 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "DimensoesId",
                table: "Produtos",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Valor",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Conteudo = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Valor", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Dimensao",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ValorId = table.Column<long>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false),
                    MinId = table.Column<long>(nullable: true),
                    MaxId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dimensao", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Dimensao_Valor_ValorId",
                        column: x => x.ValorId,
                        principalTable: "Valor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Dimensao_Valor_MaxId",
                        column: x => x.MaxId,
                        principalTable: "Valor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Dimensao_Valor_MinId",
                        column: x => x.MinId,
                        principalTable: "Valor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Dimensoes",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AlturaId = table.Column<long>(nullable: true),
                    LarguraId = table.Column<long>(nullable: true),
                    ProfundidadeId = table.Column<long>(nullable: true),
                    UnidadeId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dimensoes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Dimensoes_Dimensao_AlturaId",
                        column: x => x.AlturaId,
                        principalTable: "Dimensao",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Dimensoes_Dimensao_LarguraId",
                        column: x => x.LarguraId,
                        principalTable: "Dimensao",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Dimensoes_Dimensao_ProfundidadeId",
                        column: x => x.ProfundidadeId,
                        principalTable: "Dimensao",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Dimensoes_TipoUnidades_UnidadeId",
                        column: x => x.UnidadeId,
                        principalTable: "TipoUnidades",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Produtos_DimensoesId",
                table: "Produtos",
                column: "DimensoesId");

            migrationBuilder.CreateIndex(
                name: "IX_Dimensao_ValorId",
                table: "Dimensao",
                column: "ValorId");

            migrationBuilder.CreateIndex(
                name: "IX_Dimensao_MaxId",
                table: "Dimensao",
                column: "MaxId");

            migrationBuilder.CreateIndex(
                name: "IX_Dimensao_MinId",
                table: "Dimensao",
                column: "MinId");

            migrationBuilder.CreateIndex(
                name: "IX_Dimensoes_AlturaId",
                table: "Dimensoes",
                column: "AlturaId");

            migrationBuilder.CreateIndex(
                name: "IX_Dimensoes_LarguraId",
                table: "Dimensoes",
                column: "LarguraId");

            migrationBuilder.CreateIndex(
                name: "IX_Dimensoes_ProfundidadeId",
                table: "Dimensoes",
                column: "ProfundidadeId");

            migrationBuilder.CreateIndex(
                name: "IX_Dimensoes_UnidadeId",
                table: "Dimensoes",
                column: "UnidadeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Produtos_Dimensoes_DimensoesId",
                table: "Produtos",
                column: "DimensoesId",
                principalTable: "Dimensoes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Produtos_Dimensoes_DimensoesId",
                table: "Produtos");

            migrationBuilder.DropTable(
                name: "Dimensoes");

            migrationBuilder.DropTable(
                name: "Dimensao");

            migrationBuilder.DropTable(
                name: "Valor");

            migrationBuilder.DropIndex(
                name: "IX_Produtos_DimensoesId",
                table: "Produtos");

            migrationBuilder.DropColumn(
                name: "DimensoesId",
                table: "Produtos");
        }
    }
}
