﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class AtualizacaoCrucial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Valor_Dimensao_IdDimensao",
                table: "Valor");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Valor",
                table: "Valor");

            migrationBuilder.RenameTable(
                name: "Valor",
                newName: "Valores");

            migrationBuilder.RenameIndex(
                name: "IX_Valor_IdDimensao",
                table: "Valores",
                newName: "IX_Valores_IdDimensao");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Valores",
                table: "Valores",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Valores_Dimensao_IdDimensao",
                table: "Valores",
                column: "IdDimensao",
                principalTable: "Dimensao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Valores_Dimensao_IdDimensao",
                table: "Valores");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Valores",
                table: "Valores");

            migrationBuilder.RenameTable(
                name: "Valores",
                newName: "Valor");

            migrationBuilder.RenameIndex(
                name: "IX_Valores_IdDimensao",
                table: "Valor",
                newName: "IX_Valor_IdDimensao");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Valor",
                table: "Valor",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Valor_Dimensao_IdDimensao",
                table: "Valor",
                column: "IdDimensao",
                principalTable: "Dimensao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
