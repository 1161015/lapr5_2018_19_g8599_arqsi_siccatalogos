﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class Colecao1012 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dimensoes_Dimensao_AlturaId",
                table: "Dimensoes");

            migrationBuilder.DropForeignKey(
                name: "FK_Dimensoes_Dimensao_LarguraId",
                table: "Dimensoes");

            migrationBuilder.DropForeignKey(
                name: "FK_Dimensoes_Dimensao_ProfundidadeId",
                table: "Dimensoes");

            migrationBuilder.DropIndex(
                name: "IX_Dimensoes_AlturaId",
                table: "Dimensoes");

            migrationBuilder.DropIndex(
                name: "IX_Dimensoes_LarguraId",
                table: "Dimensoes");

            migrationBuilder.DropIndex(
                name: "IX_Dimensoes_ProfundidadeId",
                table: "Dimensoes");

            migrationBuilder.DropColumn(
                name: "AlturaId",
                table: "Dimensoes");

            migrationBuilder.DropColumn(
                name: "LarguraId",
                table: "Dimensoes");

            migrationBuilder.DropColumn(
                name: "ProfundidadeId",
                table: "Dimensoes");

            migrationBuilder.AddColumn<long>(
                name: "DimensoesId",
                table: "Dimensao",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DimensoesId1",
                table: "Dimensao",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DimensoesId2",
                table: "Dimensao",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Dimensao_DimensoesId",
                table: "Dimensao",
                column: "DimensoesId");

            migrationBuilder.CreateIndex(
                name: "IX_Dimensao_DimensoesId1",
                table: "Dimensao",
                column: "DimensoesId1");

            migrationBuilder.CreateIndex(
                name: "IX_Dimensao_DimensoesId2",
                table: "Dimensao",
                column: "DimensoesId2");

            migrationBuilder.AddForeignKey(
                name: "FK_Dimensao_Dimensoes_DimensoesId",
                table: "Dimensao",
                column: "DimensoesId",
                principalTable: "Dimensoes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Dimensao_Dimensoes_DimensoesId1",
                table: "Dimensao",
                column: "DimensoesId1",
                principalTable: "Dimensoes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Dimensao_Dimensoes_DimensoesId2",
                table: "Dimensao",
                column: "DimensoesId2",
                principalTable: "Dimensoes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dimensao_Dimensoes_DimensoesId",
                table: "Dimensao");

            migrationBuilder.DropForeignKey(
                name: "FK_Dimensao_Dimensoes_DimensoesId1",
                table: "Dimensao");

            migrationBuilder.DropForeignKey(
                name: "FK_Dimensao_Dimensoes_DimensoesId2",
                table: "Dimensao");

            migrationBuilder.DropIndex(
                name: "IX_Dimensao_DimensoesId",
                table: "Dimensao");

            migrationBuilder.DropIndex(
                name: "IX_Dimensao_DimensoesId1",
                table: "Dimensao");

            migrationBuilder.DropIndex(
                name: "IX_Dimensao_DimensoesId2",
                table: "Dimensao");

            migrationBuilder.DropColumn(
                name: "DimensoesId",
                table: "Dimensao");

            migrationBuilder.DropColumn(
                name: "DimensoesId1",
                table: "Dimensao");

            migrationBuilder.DropColumn(
                name: "DimensoesId2",
                table: "Dimensao");

            migrationBuilder.AddColumn<long>(
                name: "AlturaId",
                table: "Dimensoes",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LarguraId",
                table: "Dimensoes",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ProfundidadeId",
                table: "Dimensoes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Dimensoes_AlturaId",
                table: "Dimensoes",
                column: "AlturaId");

            migrationBuilder.CreateIndex(
                name: "IX_Dimensoes_LarguraId",
                table: "Dimensoes",
                column: "LarguraId");

            migrationBuilder.CreateIndex(
                name: "IX_Dimensoes_ProfundidadeId",
                table: "Dimensoes",
                column: "ProfundidadeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Dimensoes_Dimensao_AlturaId",
                table: "Dimensoes",
                column: "AlturaId",
                principalTable: "Dimensao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Dimensoes_Dimensao_LarguraId",
                table: "Dimensoes",
                column: "LarguraId",
                principalTable: "Dimensao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Dimensoes_Dimensao_ProfundidadeId",
                table: "Dimensoes",
                column: "ProfundidadeId",
                principalTable: "Dimensao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
