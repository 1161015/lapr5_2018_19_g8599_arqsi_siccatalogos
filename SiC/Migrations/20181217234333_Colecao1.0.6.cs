﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class Colecao106 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProdutoCatalogo_Catalogos_CatalogoId",
                table: "ProdutoCatalogo");

            migrationBuilder.DropForeignKey(
                name: "FK_ProdutoCatalogo_Produtos_Id_Catalogo",
                table: "ProdutoCatalogo");

            migrationBuilder.DropForeignKey(
                name: "FK_ProdutoPartes_Produtos_ProdutoFilhoId",
                table: "ProdutoPartes");

            migrationBuilder.DropForeignKey(
                name: "FK_Restricao_Produtos_ProdutoFilhoId",
                table: "Restricao");

            migrationBuilder.DropForeignKey(
                name: "FK_Restricao_Produtos_ProdutoPaiId",
                table: "Restricao");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProdutoCatalogo",
                table: "ProdutoCatalogo");

            migrationBuilder.DropIndex(
                name: "IX_ProdutoCatalogo_CatalogoId",
                table: "ProdutoCatalogo");

            migrationBuilder.DropColumn(
                name: "Id_Catalogo",
                table: "ProdutoCatalogo");

            migrationBuilder.RenameColumn(
                name: "ProdutoPaiId",
                table: "Restricao",
                newName: "ProdutoPaiProdutoId");

            migrationBuilder.RenameColumn(
                name: "ProdutoFilhoId",
                table: "Restricao",
                newName: "ProdutoFilhoProdutoId");

            migrationBuilder.RenameIndex(
                name: "IX_Restricao_ProdutoPaiId",
                table: "Restricao",
                newName: "IX_Restricao_ProdutoPaiProdutoId");

            migrationBuilder.RenameIndex(
                name: "IX_Restricao_ProdutoFilhoId",
                table: "Restricao",
                newName: "IX_Restricao_ProdutoFilhoProdutoId");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Produtos",
                newName: "ProdutoId");

            migrationBuilder.RenameColumn(
                name: "ProdutoFilhoId",
                table: "ProdutoPartes",
                newName: "ProdutoFilhoProdutoId");

            migrationBuilder.RenameIndex(
                name: "IX_ProdutoPartes_ProdutoFilhoId",
                table: "ProdutoPartes",
                newName: "IX_ProdutoPartes_ProdutoFilhoProdutoId");

            migrationBuilder.RenameColumn(
                name: "Id_Produto",
                table: "ProdutoCatalogo",
                newName: "ProdutoId");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Catalogos",
                newName: "CatalogoId");

            migrationBuilder.AlterColumn<long>(
                name: "CatalogoId",
                table: "ProdutoCatalogo",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProdutoCatalogo",
                table: "ProdutoCatalogo",
                columns: new[] { "CatalogoId", "ProdutoId" });

            migrationBuilder.CreateIndex(
                name: "IX_ProdutoCatalogo_ProdutoId",
                table: "ProdutoCatalogo",
                column: "ProdutoId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProdutoCatalogo_Catalogos_CatalogoId",
                table: "ProdutoCatalogo",
                column: "CatalogoId",
                principalTable: "Catalogos",
                principalColumn: "CatalogoId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProdutoCatalogo_Produtos_ProdutoId",
                table: "ProdutoCatalogo",
                column: "ProdutoId",
                principalTable: "Produtos",
                principalColumn: "ProdutoId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProdutoPartes_Produtos_ProdutoFilhoProdutoId",
                table: "ProdutoPartes",
                column: "ProdutoFilhoProdutoId",
                principalTable: "Produtos",
                principalColumn: "ProdutoId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Restricao_Produtos_ProdutoFilhoProdutoId",
                table: "Restricao",
                column: "ProdutoFilhoProdutoId",
                principalTable: "Produtos",
                principalColumn: "ProdutoId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Restricao_Produtos_ProdutoPaiProdutoId",
                table: "Restricao",
                column: "ProdutoPaiProdutoId",
                principalTable: "Produtos",
                principalColumn: "ProdutoId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProdutoCatalogo_Catalogos_CatalogoId",
                table: "ProdutoCatalogo");

            migrationBuilder.DropForeignKey(
                name: "FK_ProdutoCatalogo_Produtos_ProdutoId",
                table: "ProdutoCatalogo");

            migrationBuilder.DropForeignKey(
                name: "FK_ProdutoPartes_Produtos_ProdutoFilhoProdutoId",
                table: "ProdutoPartes");

            migrationBuilder.DropForeignKey(
                name: "FK_Restricao_Produtos_ProdutoFilhoProdutoId",
                table: "Restricao");

            migrationBuilder.DropForeignKey(
                name: "FK_Restricao_Produtos_ProdutoPaiProdutoId",
                table: "Restricao");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProdutoCatalogo",
                table: "ProdutoCatalogo");

            migrationBuilder.DropIndex(
                name: "IX_ProdutoCatalogo_ProdutoId",
                table: "ProdutoCatalogo");

            migrationBuilder.RenameColumn(
                name: "ProdutoPaiProdutoId",
                table: "Restricao",
                newName: "ProdutoPaiId");

            migrationBuilder.RenameColumn(
                name: "ProdutoFilhoProdutoId",
                table: "Restricao",
                newName: "ProdutoFilhoId");

            migrationBuilder.RenameIndex(
                name: "IX_Restricao_ProdutoPaiProdutoId",
                table: "Restricao",
                newName: "IX_Restricao_ProdutoPaiId");

            migrationBuilder.RenameIndex(
                name: "IX_Restricao_ProdutoFilhoProdutoId",
                table: "Restricao",
                newName: "IX_Restricao_ProdutoFilhoId");

            migrationBuilder.RenameColumn(
                name: "ProdutoId",
                table: "Produtos",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "ProdutoFilhoProdutoId",
                table: "ProdutoPartes",
                newName: "ProdutoFilhoId");

            migrationBuilder.RenameIndex(
                name: "IX_ProdutoPartes_ProdutoFilhoProdutoId",
                table: "ProdutoPartes",
                newName: "IX_ProdutoPartes_ProdutoFilhoId");

            migrationBuilder.RenameColumn(
                name: "ProdutoId",
                table: "ProdutoCatalogo",
                newName: "Id_Produto");

            migrationBuilder.RenameColumn(
                name: "CatalogoId",
                table: "Catalogos",
                newName: "Id");

            migrationBuilder.AlterColumn<long>(
                name: "CatalogoId",
                table: "ProdutoCatalogo",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<long>(
                name: "Id_Catalogo",
                table: "ProdutoCatalogo",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProdutoCatalogo",
                table: "ProdutoCatalogo",
                columns: new[] { "Id_Catalogo", "Id_Produto" });

            migrationBuilder.CreateIndex(
                name: "IX_ProdutoCatalogo_CatalogoId",
                table: "ProdutoCatalogo",
                column: "CatalogoId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProdutoCatalogo_Catalogos_CatalogoId",
                table: "ProdutoCatalogo",
                column: "CatalogoId",
                principalTable: "Catalogos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProdutoCatalogo_Produtos_Id_Catalogo",
                table: "ProdutoCatalogo",
                column: "Id_Catalogo",
                principalTable: "Produtos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProdutoPartes_Produtos_ProdutoFilhoId",
                table: "ProdutoPartes",
                column: "ProdutoFilhoId",
                principalTable: "Produtos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Restricao_Produtos_ProdutoFilhoId",
                table: "Restricao",
                column: "ProdutoFilhoId",
                principalTable: "Produtos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Restricao_Produtos_ProdutoPaiId",
                table: "Restricao",
                column: "ProdutoPaiId",
                principalTable: "Produtos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
