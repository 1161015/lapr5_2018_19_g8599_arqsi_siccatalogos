﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class SeraAgora : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MaterialAcabamento_Acabamentos_AcabamentoId",
                table: "MaterialAcabamento");

            migrationBuilder.DropForeignKey(
                name: "FK_ProdutoMaterial_Materiais_MaterialId",
                table: "ProdutoMaterial");

            migrationBuilder.DropIndex(
                name: "IX_ProdutoMaterial_MaterialId",
                table: "ProdutoMaterial");

            migrationBuilder.DropIndex(
                name: "IX_MaterialAcabamento_AcabamentoId",
                table: "MaterialAcabamento");

            migrationBuilder.DropColumn(
                name: "MaterialId",
                table: "ProdutoMaterial");

            migrationBuilder.DropColumn(
                name: "AcabamentoId",
                table: "MaterialAcabamento");

            migrationBuilder.CreateIndex(
                name: "IX_ProdutoMaterial_IdMaterial",
                table: "ProdutoMaterial",
                column: "IdMaterial");

            migrationBuilder.CreateIndex(
                name: "IX_MaterialAcabamento_IdAcabamento",
                table: "MaterialAcabamento",
                column: "IdAcabamento");

            migrationBuilder.AddForeignKey(
                name: "FK_MaterialAcabamento_Acabamentos_IdAcabamento",
                table: "MaterialAcabamento",
                column: "IdAcabamento",
                principalTable: "Acabamentos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProdutoMaterial_Materiais_IdMaterial",
                table: "ProdutoMaterial",
                column: "IdMaterial",
                principalTable: "Materiais",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MaterialAcabamento_Acabamentos_IdAcabamento",
                table: "MaterialAcabamento");

            migrationBuilder.DropForeignKey(
                name: "FK_ProdutoMaterial_Materiais_IdMaterial",
                table: "ProdutoMaterial");

            migrationBuilder.DropIndex(
                name: "IX_ProdutoMaterial_IdMaterial",
                table: "ProdutoMaterial");

            migrationBuilder.DropIndex(
                name: "IX_MaterialAcabamento_IdAcabamento",
                table: "MaterialAcabamento");

            migrationBuilder.AddColumn<long>(
                name: "MaterialId",
                table: "ProdutoMaterial",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "AcabamentoId",
                table: "MaterialAcabamento",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProdutoMaterial_MaterialId",
                table: "ProdutoMaterial",
                column: "MaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_MaterialAcabamento_AcabamentoId",
                table: "MaterialAcabamento",
                column: "AcabamentoId");

            migrationBuilder.AddForeignKey(
                name: "FK_MaterialAcabamento_Acabamentos_AcabamentoId",
                table: "MaterialAcabamento",
                column: "AcabamentoId",
                principalTable: "Acabamentos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProdutoMaterial_Materiais_MaterialId",
                table: "ProdutoMaterial",
                column: "MaterialId",
                principalTable: "Materiais",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
