﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class Colecao1011 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdFilho",
                table: "Restricao");

            migrationBuilder.DropColumn(
                name: "IdPai",
                table: "Restricao");

            migrationBuilder.AddColumn<long>(
                name: "FilhoProdutoId",
                table: "Restricao",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "PaiProdutoId",
                table: "Restricao",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Restricao_FilhoProdutoId",
                table: "Restricao",
                column: "FilhoProdutoId");

            migrationBuilder.CreateIndex(
                name: "IX_Restricao_PaiProdutoId",
                table: "Restricao",
                column: "PaiProdutoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Restricao_Produtos_FilhoProdutoId",
                table: "Restricao",
                column: "FilhoProdutoId",
                principalTable: "Produtos",
                principalColumn: "ProdutoId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Restricao_Produtos_PaiProdutoId",
                table: "Restricao",
                column: "PaiProdutoId",
                principalTable: "Produtos",
                principalColumn: "ProdutoId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Restricao_Produtos_FilhoProdutoId",
                table: "Restricao");

            migrationBuilder.DropForeignKey(
                name: "FK_Restricao_Produtos_PaiProdutoId",
                table: "Restricao");

            migrationBuilder.DropIndex(
                name: "IX_Restricao_FilhoProdutoId",
                table: "Restricao");

            migrationBuilder.DropIndex(
                name: "IX_Restricao_PaiProdutoId",
                table: "Restricao");

            migrationBuilder.DropColumn(
                name: "FilhoProdutoId",
                table: "Restricao");

            migrationBuilder.DropColumn(
                name: "PaiProdutoId",
                table: "Restricao");

            migrationBuilder.AddColumn<long>(
                name: "IdFilho",
                table: "Restricao",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "IdPai",
                table: "Restricao",
                nullable: false,
                defaultValue: 0L);
        }
    }
}
