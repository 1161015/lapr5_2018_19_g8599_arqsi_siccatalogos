﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class Colecao1014 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Restricao_Produtos_ProdutoFilhoProdutoId",
                table: "Restricao");

            migrationBuilder.DropForeignKey(
                name: "FK_Restricao_Produtos_ProdutoPaiProdutoId",
                table: "Restricao");

            migrationBuilder.DropIndex(
                name: "IX_Restricao_ProdutoFilhoProdutoId",
                table: "Restricao");

            migrationBuilder.DropIndex(
                name: "IX_Restricao_ProdutoPaiProdutoId",
                table: "Restricao");

            migrationBuilder.DropColumn(
                name: "ProdutoFilhoProdutoId",
                table: "Restricao");

            migrationBuilder.DropColumn(
                name: "ProdutoPaiProdutoId",
                table: "Restricao");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ProdutoFilhoProdutoId",
                table: "Restricao",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ProdutoPaiProdutoId",
                table: "Restricao",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Restricao_ProdutoFilhoProdutoId",
                table: "Restricao",
                column: "ProdutoFilhoProdutoId");

            migrationBuilder.CreateIndex(
                name: "IX_Restricao_ProdutoPaiProdutoId",
                table: "Restricao",
                column: "ProdutoPaiProdutoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Restricao_Produtos_ProdutoFilhoProdutoId",
                table: "Restricao",
                column: "ProdutoFilhoProdutoId",
                principalTable: "Produtos",
                principalColumn: "ProdutoId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Restricao_Produtos_ProdutoPaiProdutoId",
                table: "Restricao",
                column: "ProdutoPaiProdutoId",
                principalTable: "Produtos",
                principalColumn: "ProdutoId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
