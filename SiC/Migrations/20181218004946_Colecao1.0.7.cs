﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class Colecao107 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProdutoPartes_Produtos_IdPai",
                table: "ProdutoPartes");

            migrationBuilder.DropForeignKey(
                name: "FK_ProdutoPartes_Produtos_ProdutoFilhoProdutoId",
                table: "ProdutoPartes");

            migrationBuilder.DropIndex(
                name: "IX_ProdutoPartes_ProdutoFilhoProdutoId",
                table: "ProdutoPartes");

            migrationBuilder.DropColumn(
                name: "Obrigatoriedade",
                table: "ProdutoPartes");

            migrationBuilder.DropColumn(
                name: "ProdutoFilhoProdutoId",
                table: "ProdutoPartes");

            migrationBuilder.CreateIndex(
                name: "IX_ProdutoPartes_IdFilho",
                table: "ProdutoPartes",
                column: "IdFilho");

            migrationBuilder.AddForeignKey(
                name: "FK_ProdutoPartes_Produtos_IdFilho",
                table: "ProdutoPartes",
                column: "IdFilho",
                principalTable: "Produtos",
                principalColumn: "ProdutoId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProdutoPartes_Produtos_IdPai",
                table: "ProdutoPartes",
                column: "IdPai",
                principalTable: "Produtos",
                principalColumn: "ProdutoId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProdutoPartes_Produtos_IdFilho",
                table: "ProdutoPartes");

            migrationBuilder.DropForeignKey(
                name: "FK_ProdutoPartes_Produtos_IdPai",
                table: "ProdutoPartes");

            migrationBuilder.DropIndex(
                name: "IX_ProdutoPartes_IdFilho",
                table: "ProdutoPartes");

            migrationBuilder.AddColumn<bool>(
                name: "Obrigatoriedade",
                table: "ProdutoPartes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<long>(
                name: "ProdutoFilhoProdutoId",
                table: "ProdutoPartes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProdutoPartes_ProdutoFilhoProdutoId",
                table: "ProdutoPartes",
                column: "ProdutoFilhoProdutoId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProdutoPartes_Produtos_IdPai",
                table: "ProdutoPartes",
                column: "IdPai",
                principalTable: "Produtos",
                principalColumn: "ProdutoId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProdutoPartes_Produtos_ProdutoFilhoProdutoId",
                table: "ProdutoPartes",
                column: "ProdutoFilhoProdutoId",
                principalTable: "Produtos",
                principalColumn: "ProdutoId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
