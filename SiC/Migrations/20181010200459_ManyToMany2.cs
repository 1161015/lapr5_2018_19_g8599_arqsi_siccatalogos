﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class ManyToMany2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Acabamentos_Materiais_MaterialId",
                table: "Acabamentos");

            migrationBuilder.DropForeignKey(
                name: "FK_Materiais_Produtos_ProdutoId",
                table: "Materiais");

            migrationBuilder.DropIndex(
                name: "IX_Materiais_ProdutoId",
                table: "Materiais");

            migrationBuilder.DropIndex(
                name: "IX_Acabamentos_MaterialId",
                table: "Acabamentos");

            migrationBuilder.DropColumn(
                name: "ProdutoId",
                table: "Materiais");

            migrationBuilder.DropColumn(
                name: "MaterialId",
                table: "Acabamentos");

            migrationBuilder.DropColumn(
                name: "Nome_AcabamentoId",
                table: "Acabamentos");

            migrationBuilder.CreateTable(
                name: "MaterialAcabamento",
                columns: table => new
                {
                    IdMaterial = table.Column<long>(nullable: false),
                    IdAcabamento = table.Column<long>(nullable: false),
                    AcabamentoId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaterialAcabamento", x => new { x.IdMaterial, x.IdAcabamento });
                    table.ForeignKey(
                        name: "FK_MaterialAcabamento_Acabamentos_AcabamentoId",
                        column: x => x.AcabamentoId,
                        principalTable: "Acabamentos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MaterialAcabamento_Materiais_IdMaterial",
                        column: x => x.IdMaterial,
                        principalTable: "Materiais",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProdutoMaterial",
                columns: table => new
                {
                    IdProduto = table.Column<long>(nullable: false),
                    IdMaterial = table.Column<long>(nullable: false),
                    MaterialId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProdutoMaterial", x => new { x.IdProduto, x.IdMaterial });
                    table.ForeignKey(
                        name: "FK_ProdutoMaterial_Produtos_IdProduto",
                        column: x => x.IdProduto,
                        principalTable: "Produtos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProdutoMaterial_Materiais_MaterialId",
                        column: x => x.MaterialId,
                        principalTable: "Materiais",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MaterialAcabamento_AcabamentoId",
                table: "MaterialAcabamento",
                column: "AcabamentoId");

            migrationBuilder.CreateIndex(
                name: "IX_ProdutoMaterial_MaterialId",
                table: "ProdutoMaterial",
                column: "MaterialId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MaterialAcabamento");

            migrationBuilder.DropTable(
                name: "ProdutoMaterial");

            migrationBuilder.AddColumn<long>(
                name: "ProdutoId",
                table: "Materiais",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "MaterialId",
                table: "Acabamentos",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "Nome_AcabamentoId",
                table: "Acabamentos",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_Materiais_ProdutoId",
                table: "Materiais",
                column: "ProdutoId");

            migrationBuilder.CreateIndex(
                name: "IX_Acabamentos_MaterialId",
                table: "Acabamentos",
                column: "MaterialId");

            migrationBuilder.AddForeignKey(
                name: "FK_Acabamentos_Materiais_MaterialId",
                table: "Acabamentos",
                column: "MaterialId",
                principalTable: "Materiais",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Materiais_Produtos_ProdutoId",
                table: "Materiais",
                column: "ProdutoId",
                principalTable: "Produtos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
