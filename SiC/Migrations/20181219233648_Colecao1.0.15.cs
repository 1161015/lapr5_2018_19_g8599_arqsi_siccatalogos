﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class Colecao1015 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Restricao_Materiais_MaterialFilhoId",
                table: "Restricao");

            migrationBuilder.DropForeignKey(
                name: "FK_Restricao_Materiais_MaterialPaiId",
                table: "Restricao");

            migrationBuilder.DropIndex(
                name: "IX_Restricao_MaterialFilhoId",
                table: "Restricao");

            migrationBuilder.DropIndex(
                name: "IX_Restricao_MaterialPaiId",
                table: "Restricao");

            migrationBuilder.DropColumn(
                name: "MaterialFilhoId",
                table: "Restricao");

            migrationBuilder.DropColumn(
                name: "MaterialPaiId",
                table: "Restricao");

            migrationBuilder.AddColumn<long>(
                name: "IdMaterialFilho",
                table: "Restricao",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "IdMaterialPai",
                table: "Restricao",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdMaterialFilho",
                table: "Restricao");

            migrationBuilder.DropColumn(
                name: "IdMaterialPai",
                table: "Restricao");

            migrationBuilder.AddColumn<long>(
                name: "MaterialFilhoId",
                table: "Restricao",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "MaterialPaiId",
                table: "Restricao",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Restricao_MaterialFilhoId",
                table: "Restricao",
                column: "MaterialFilhoId");

            migrationBuilder.CreateIndex(
                name: "IX_Restricao_MaterialPaiId",
                table: "Restricao",
                column: "MaterialPaiId");

            migrationBuilder.AddForeignKey(
                name: "FK_Restricao_Materiais_MaterialFilhoId",
                table: "Restricao",
                column: "MaterialFilhoId",
                principalTable: "Materiais",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Restricao_Materiais_MaterialPaiId",
                table: "Restricao",
                column: "MaterialPaiId",
                principalTable: "Materiais",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
