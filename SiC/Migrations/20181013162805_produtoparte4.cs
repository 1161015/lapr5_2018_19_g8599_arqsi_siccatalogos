﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class produtoparte4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProdutoPartes_Produtos_IdFilho",
                table: "ProdutoPartes");

            migrationBuilder.DropIndex(
                name: "IX_ProdutoPartes_IdFilho",
                table: "ProdutoPartes");

            migrationBuilder.RenameColumn(
                name: "Nome_Conteudo",
                table: "Categorias",
                newName: "Descricao");

            migrationBuilder.AddForeignKey(
                name: "FK_ProdutoPartes_Produtos_IdPai",
                table: "ProdutoPartes",
                column: "IdPai",
                principalTable: "Produtos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProdutoPartes_Produtos_IdPai",
                table: "ProdutoPartes");

            migrationBuilder.RenameColumn(
                name: "Descricao",
                table: "Categorias",
                newName: "Nome_Conteudo");

            migrationBuilder.CreateIndex(
                name: "IX_ProdutoPartes_IdFilho",
                table: "ProdutoPartes",
                column: "IdFilho");

            migrationBuilder.AddForeignKey(
                name: "FK_ProdutoPartes_Produtos_IdFilho",
                table: "ProdutoPartes",
                column: "IdFilho",
                principalTable: "Produtos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
