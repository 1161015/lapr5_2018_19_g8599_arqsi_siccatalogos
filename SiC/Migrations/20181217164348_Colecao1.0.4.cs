﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class Colecao104 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Max",
                table: "Dimensao");

            migrationBuilder.DropColumn(
                name: "Min",
                table: "Dimensao");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Dimensao",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "IdProduto",
                table: "Dimensao",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "ProdutoId",
                table: "Dimensao",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "dimensaoMax",
                table: "Dimensao",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "dimensaoMin",
                table: "Dimensao",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "dimensao",
                table: "Dimensao",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Dimensao_ProdutoId",
                table: "Dimensao",
                column: "ProdutoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Dimensao_Produtos_ProdutoId",
                table: "Dimensao",
                column: "ProdutoId",
                principalTable: "Produtos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dimensao_Produtos_ProdutoId",
                table: "Dimensao");

            migrationBuilder.DropIndex(
                name: "IX_Dimensao_ProdutoId",
                table: "Dimensao");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Dimensao");

            migrationBuilder.DropColumn(
                name: "IdProduto",
                table: "Dimensao");

            migrationBuilder.DropColumn(
                name: "ProdutoId",
                table: "Dimensao");

            migrationBuilder.DropColumn(
                name: "dimensaoMax",
                table: "Dimensao");

            migrationBuilder.DropColumn(
                name: "dimensaoMin",
                table: "Dimensao");

            migrationBuilder.DropColumn(
                name: "dimensao",
                table: "Dimensao");

            migrationBuilder.AddColumn<double>(
                name: "Max",
                table: "Dimensao",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Min",
                table: "Dimensao",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}
