﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class mcdonalds105 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Descricao",
                table: "Categorias",
                newName: "Nome_Conteudo");

            migrationBuilder.AddColumn<long>(
                name: "CategoriaPaiId",
                table: "Categorias",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Categorias_CategoriaPaiId",
                table: "Categorias",
                column: "CategoriaPaiId");

            migrationBuilder.AddForeignKey(
                name: "FK_Categorias_Categorias_CategoriaPaiId",
                table: "Categorias",
                column: "CategoriaPaiId",
                principalTable: "Categorias",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Categorias_Categorias_CategoriaPaiId",
                table: "Categorias");

            migrationBuilder.DropIndex(
                name: "IX_Categorias_CategoriaPaiId",
                table: "Categorias");

            migrationBuilder.DropColumn(
                name: "CategoriaPaiId",
                table: "Categorias");

            migrationBuilder.RenameColumn(
                name: "Nome_Conteudo",
                table: "Categorias",
                newName: "Descricao");
        }
    }
}
