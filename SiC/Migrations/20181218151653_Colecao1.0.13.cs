﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class Colecao1013 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dimensao_Produtos_ProdutoId",
                table: "Dimensao");

            migrationBuilder.DropIndex(
                name: "IX_Dimensao_ProdutoId",
                table: "Dimensao");

            migrationBuilder.DropColumn(
                name: "IdProduto",
                table: "Dimensao");

            migrationBuilder.DropColumn(
                name: "ProdutoId",
                table: "Dimensao");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "IdProduto",
                table: "Dimensao",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "ProdutoId",
                table: "Dimensao",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Dimensao_ProdutoId",
                table: "Dimensao",
                column: "ProdutoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Dimensao_Produtos_ProdutoId",
                table: "Dimensao",
                column: "ProdutoId",
                principalTable: "Produtos",
                principalColumn: "ProdutoId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
