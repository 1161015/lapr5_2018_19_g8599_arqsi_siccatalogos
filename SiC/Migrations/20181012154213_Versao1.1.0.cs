﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class Versao110 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dimensao_Valor_ValorId",
                table: "Dimensao");

            migrationBuilder.DropIndex(
                name: "IX_Dimensao_ValorId",
                table: "Dimensao");

            migrationBuilder.DropColumn(
                name: "ValorId",
                table: "Dimensao");

            migrationBuilder.AddColumn<long>(
                name: "IdDimensao",
                table: "Valor",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Valor_IdDimensao",
                table: "Valor",
                column: "IdDimensao",
                unique: true,
                filter: "[IdDimensao] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Valor_Dimensao_IdDimensao",
                table: "Valor",
                column: "IdDimensao",
                principalTable: "Dimensao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Valor_Dimensao_IdDimensao",
                table: "Valor");

            migrationBuilder.DropIndex(
                name: "IX_Valor_IdDimensao",
                table: "Valor");

            migrationBuilder.DropColumn(
                name: "IdDimensao",
                table: "Valor");

            migrationBuilder.AddColumn<long>(
                name: "ValorId",
                table: "Dimensao",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Dimensao_ValorId",
                table: "Dimensao",
                column: "ValorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Dimensao_Valor_ValorId",
                table: "Dimensao",
                column: "ValorId",
                principalTable: "Valor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
