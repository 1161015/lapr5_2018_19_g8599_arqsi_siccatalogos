﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class NovoServidorDBAzure_101 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Restricao_PercentagemMinima",
                table: "Produtos",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "Restricao_PercentagemMaxima",
                table: "Produtos",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<bool>(
                name: "Restricao_MaterialAcabamento",
                table: "Produtos",
                nullable: true,
                oldClrType: typeof(bool));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Restricao_PercentagemMinima",
                table: "Produtos",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "Restricao_PercentagemMaxima",
                table: "Produtos",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "Restricao_MaterialAcabamento",
                table: "Produtos",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);
        }
    }
}
