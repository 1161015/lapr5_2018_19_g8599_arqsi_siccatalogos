﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class NovaMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dimensao_Valor_MaxId",
                table: "Dimensao");

            migrationBuilder.DropForeignKey(
                name: "FK_Dimensao_Valor_MinId",
                table: "Dimensao");

            migrationBuilder.DropIndex(
                name: "IX_Valor_IdDimensao",
                table: "Valor");

            migrationBuilder.DropIndex(
                name: "IX_Dimensao_MaxId",
                table: "Dimensao");

            migrationBuilder.DropIndex(
                name: "IX_Dimensao_MinId",
                table: "Dimensao");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Dimensao");

            migrationBuilder.DropColumn(
                name: "MaxId",
                table: "Dimensao");

            migrationBuilder.DropColumn(
                name: "MinId",
                table: "Dimensao");

            migrationBuilder.AddColumn<double>(
                name: "Max",
                table: "Dimensao",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Min",
                table: "Dimensao",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.CreateIndex(
                name: "IX_Valor_IdDimensao",
                table: "Valor",
                column: "IdDimensao");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Valor_IdDimensao",
                table: "Valor");

            migrationBuilder.DropColumn(
                name: "Max",
                table: "Dimensao");

            migrationBuilder.DropColumn(
                name: "Min",
                table: "Dimensao");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Dimensao",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "MaxId",
                table: "Dimensao",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "MinId",
                table: "Dimensao",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Valor_IdDimensao",
                table: "Valor",
                column: "IdDimensao",
                unique: true,
                filter: "[IdDimensao] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Dimensao_MaxId",
                table: "Dimensao",
                column: "MaxId");

            migrationBuilder.CreateIndex(
                name: "IX_Dimensao_MinId",
                table: "Dimensao",
                column: "MinId");

            migrationBuilder.AddForeignKey(
                name: "FK_Dimensao_Valor_MaxId",
                table: "Dimensao",
                column: "MaxId",
                principalTable: "Valor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Dimensao_Valor_MinId",
                table: "Dimensao",
                column: "MinId",
                principalTable: "Valor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
