﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class Tent01 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Restricao_MaterialAcabamento",
                table: "Produtos");

            migrationBuilder.DropColumn(
                name: "Restricao_PercentagemMaxima",
                table: "Produtos");

            migrationBuilder.DropColumn(
                name: "Restricao_PercentagemMinima",
                table: "Produtos");

            migrationBuilder.AddColumn<long>(
                name: "ColecaoId",
                table: "Produtos",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Catalogos",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nome_Conteudo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Catalogos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HistoricoPrecos",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Id_Material = table.Column<long>(nullable: false),
                    Id_Acabamento = table.Column<long>(nullable: true),
                    Preco_valor = table.Column<double>(nullable: false),
                    Preco_DataInicio = table.Column<DateTime>(nullable: false),
                    Preco_DataFim = table.Column<DateTime>(nullable: false),
                    MaterialId = table.Column<long>(nullable: true),
                    AcabamentoId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistoricoPrecos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HistoricoPrecos_Acabamentos_AcabamentoId",
                        column: x => x.AcabamentoId,
                        principalTable: "Acabamentos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HistoricoPrecos_Materiais_MaterialId",
                        column: x => x.MaterialId,
                        principalTable: "Materiais",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Restricao",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IdPai = table.Column<long>(nullable: false),
                    IdFilho = table.Column<long>(nullable: false),
                    ProdutoPaiId = table.Column<long>(nullable: true),
                    ProdutoFilhoId = table.Column<long>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false),
                    IdMaterialPai = table.Column<long>(nullable: true),
                    IdMaterialFilho = table.Column<long>(nullable: true),
                    percentagemAlturaMinima = table.Column<double>(nullable: true),
                    percentagemAlturaMaxima = table.Column<double>(nullable: true),
                    percentagemLarguraMinima = table.Column<double>(nullable: true),
                    percentagemLarguraMaxima = table.Column<double>(nullable: true),
                    percentagemProfundidadeMinima = table.Column<double>(nullable: true),
                    percentagemProfundidadeMaxima = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Restricao", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Restricao_Produtos_ProdutoFilhoId",
                        column: x => x.ProdutoFilhoId,
                        principalTable: "Produtos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Restricao_Produtos_ProdutoPaiId",
                        column: x => x.ProdutoPaiId,
                        principalTable: "Produtos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Colecoes",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nome_Conteudo = table.Column<string>(nullable: true),
                    CatalogoId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Colecoes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Colecoes_Catalogos_CatalogoId",
                        column: x => x.CatalogoId,
                        principalTable: "Catalogos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProdutoCatalogo",
                columns: table => new
                {
                    Id_Produto = table.Column<long>(nullable: false),
                    Id_Catalogo = table.Column<long>(nullable: false),
                    CatalogoId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProdutoCatalogo", x => new { x.Id_Catalogo, x.Id_Produto });
                    table.ForeignKey(
                        name: "FK_ProdutoCatalogo_Catalogos_CatalogoId",
                        column: x => x.CatalogoId,
                        principalTable: "Catalogos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProdutoCatalogo_Produtos_Id_Catalogo",
                        column: x => x.Id_Catalogo,
                        principalTable: "Produtos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Produtos_ColecaoId",
                table: "Produtos",
                column: "ColecaoId");

            migrationBuilder.CreateIndex(
                name: "IX_Colecoes_CatalogoId",
                table: "Colecoes",
                column: "CatalogoId");

            migrationBuilder.CreateIndex(
                name: "IX_HistoricoPrecos_AcabamentoId",
                table: "HistoricoPrecos",
                column: "AcabamentoId");

            migrationBuilder.CreateIndex(
                name: "IX_HistoricoPrecos_MaterialId",
                table: "HistoricoPrecos",
                column: "MaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_ProdutoCatalogo_CatalogoId",
                table: "ProdutoCatalogo",
                column: "CatalogoId");

            migrationBuilder.CreateIndex(
                name: "IX_Restricao_ProdutoFilhoId",
                table: "Restricao",
                column: "ProdutoFilhoId");

            migrationBuilder.CreateIndex(
                name: "IX_Restricao_ProdutoPaiId",
                table: "Restricao",
                column: "ProdutoPaiId");

            migrationBuilder.AddForeignKey(
                name: "FK_Produtos_Colecoes_ColecaoId",
                table: "Produtos",
                column: "ColecaoId",
                principalTable: "Colecoes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Produtos_Colecoes_ColecaoId",
                table: "Produtos");

            migrationBuilder.DropTable(
                name: "Colecoes");

            migrationBuilder.DropTable(
                name: "HistoricoPrecos");

            migrationBuilder.DropTable(
                name: "ProdutoCatalogo");

            migrationBuilder.DropTable(
                name: "Restricao");

            migrationBuilder.DropTable(
                name: "Catalogos");

            migrationBuilder.DropIndex(
                name: "IX_Produtos_ColecaoId",
                table: "Produtos");

            migrationBuilder.DropColumn(
                name: "ColecaoId",
                table: "Produtos");

            migrationBuilder.AddColumn<bool>(
                name: "Restricao_MaterialAcabamento",
                table: "Produtos",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Restricao_PercentagemMaxima",
                table: "Produtos",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Restricao_PercentagemMinima",
                table: "Produtos",
                nullable: true);
        }
    }
}
