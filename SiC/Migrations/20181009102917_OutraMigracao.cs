﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class OutraMigracao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Materiais_Acabamentos_AcabamentoId",
                table: "Materiais");

            migrationBuilder.DropForeignKey(
                name: "FK_Produtos_Materiais_MaterialId",
                table: "Produtos");

            migrationBuilder.RenameColumn(
                name: "MaterialId",
                table: "Produtos",
                newName: "ProdutoId");

            migrationBuilder.RenameIndex(
                name: "IX_Produtos_MaterialId",
                table: "Produtos",
                newName: "IX_Produtos_ProdutoId");

            migrationBuilder.RenameColumn(
                name: "AcabamentoId",
                table: "Materiais",
                newName: "ProdutoId");

            migrationBuilder.RenameIndex(
                name: "IX_Materiais_AcabamentoId",
                table: "Materiais",
                newName: "IX_Materiais_ProdutoId");

            migrationBuilder.AddColumn<long>(
                name: "MaterialId",
                table: "Acabamentos",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Unidades",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Unidades", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Acabamentos_MaterialId",
                table: "Acabamentos",
                column: "MaterialId");

            migrationBuilder.AddForeignKey(
                name: "FK_Acabamentos_Materiais_MaterialId",
                table: "Acabamentos",
                column: "MaterialId",
                principalTable: "Materiais",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Materiais_Produtos_ProdutoId",
                table: "Materiais",
                column: "ProdutoId",
                principalTable: "Produtos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Produtos_Produtos_ProdutoId",
                table: "Produtos",
                column: "ProdutoId",
                principalTable: "Produtos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Acabamentos_Materiais_MaterialId",
                table: "Acabamentos");

            migrationBuilder.DropForeignKey(
                name: "FK_Materiais_Produtos_ProdutoId",
                table: "Materiais");

            migrationBuilder.DropForeignKey(
                name: "FK_Produtos_Produtos_ProdutoId",
                table: "Produtos");

            migrationBuilder.DropTable(
                name: "Unidades");

            migrationBuilder.DropIndex(
                name: "IX_Acabamentos_MaterialId",
                table: "Acabamentos");

            migrationBuilder.DropColumn(
                name: "MaterialId",
                table: "Acabamentos");

            migrationBuilder.RenameColumn(
                name: "ProdutoId",
                table: "Produtos",
                newName: "MaterialId");

            migrationBuilder.RenameIndex(
                name: "IX_Produtos_ProdutoId",
                table: "Produtos",
                newName: "IX_Produtos_MaterialId");

            migrationBuilder.RenameColumn(
                name: "ProdutoId",
                table: "Materiais",
                newName: "AcabamentoId");

            migrationBuilder.RenameIndex(
                name: "IX_Materiais_ProdutoId",
                table: "Materiais",
                newName: "IX_Materiais_AcabamentoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Materiais_Acabamentos_AcabamentoId",
                table: "Materiais",
                column: "AcabamentoId",
                principalTable: "Acabamentos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Produtos_Materiais_MaterialId",
                table: "Produtos",
                column: "MaterialId",
                principalTable: "Materiais",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
