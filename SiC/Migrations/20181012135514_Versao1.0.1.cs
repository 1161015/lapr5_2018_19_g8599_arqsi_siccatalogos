﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class Versao101 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Unidades",
                table: "Unidades");

            migrationBuilder.RenameTable(
                name: "Unidades",
                newName: "TipoUnidades");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TipoUnidades",
                table: "TipoUnidades",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_TipoUnidades",
                table: "TipoUnidades");

            migrationBuilder.RenameTable(
                name: "TipoUnidades",
                newName: "Unidades");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Unidades",
                table: "Unidades",
                column: "Id");
        }
    }
}
