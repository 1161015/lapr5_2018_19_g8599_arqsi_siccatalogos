﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC.Migrations
{
    public partial class RemocaoUnidade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dimensoes_TipoUnidades_UnidadeId",
                table: "Dimensoes");

            migrationBuilder.DropTable(
                name: "TipoUnidades");

            migrationBuilder.DropIndex(
                name: "IX_Dimensoes_UnidadeId",
                table: "Dimensoes");

            migrationBuilder.DropColumn(
                name: "UnidadeId",
                table: "Dimensoes");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "UnidadeId",
                table: "Dimensoes",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TipoUnidades",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Unidade = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoUnidades", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Dimensoes_UnidadeId",
                table: "Dimensoes",
                column: "UnidadeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Dimensoes_TipoUnidades_UnidadeId",
                table: "Dimensoes",
                column: "UnidadeId",
                principalTable: "TipoUnidades",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
